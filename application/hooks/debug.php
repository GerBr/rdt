<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
 * This hook enables or disables the debug mode (aka Profiler)
 * Doing this with a hook prevents messing in the core files
 * Usage is simply appending debug=on / debug=off in the query string
 */

class Debug
{

    function switch_debug()
    {
        $CI = & get_instance();
        if (isset($_GET['debug'])) {
            $CI->session->set_userdata(array('debug' => $_GET['debug']));
        }
        $sess_debug = $CI->session->userdata('debug');
        if ($sess_debug == 'on') {
            $CI->output->enable_profiler(TRUE);
        }
    }
}
