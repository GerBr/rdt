<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
  |--------------------------------------------------------------------------
  | File and Directory Modes
  |--------------------------------------------------------------------------
  |
  | These prefs are used when checking and setting modes when working
  | with the file system.  The defaults are fine on servers with proper
  | security, but you may wish (or even need) to change the values in
  | certain environments (Apache running a separate process for each
  | user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
  | always be used to set the mode correctly.
  |
 */
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
  |--------------------------------------------------------------------------
  | File Stream Modes
  |--------------------------------------------------------------------------
  |
  | These modes are used when working with fopen()/popen()
  |
 */

define('FOPEN_READ', 'rb');
define('FOPEN_READ_WRITE', 'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE', 'ab');
define('FOPEN_READ_WRITE_CREATE', 'a+b');
define('FOPEN_WRITE_CREATE_STRICT', 'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

// Generics
define('APPLICATION_NAME', 'Rond de Toren CMS');
define('MAINTENANCE_MODE', FALSE);


// Set salt for passwords
define('PASSWORD_SALT', 'o6Q7WxscwtcZ');

// Set expiration time in seconds for activation link
define('TOKEN_EXPIRATION_TIME', 86400); // 1 day

// Dirs/paths
define('SYSTEM_TEMP', './temp/');
define('UPLOAD_DIR', './uploads/');


// System wide Colors
define('COLOR_YES', '#CEF6CE');
define('COLOR_NO', '#F8E0E6');

define('COLOR_ENABLED', '#EFEFEF');
define('COLOR_DISABLED', '#CCCCCC');

// Pie chart colors
define('COLOR_PIE_0', '#1B1862');
define('COLOR_PIE_1', '#19C89D');
define('COLOR_PIE_2', '#9C0B5D');
define('COLOR_PIE_3', '#33ACE0');
define('COLOR_PIE_4', '#43AC6A');
define('COLOR_PIE_5', '#F9AF48');
define('COLOR_PIE_6', '#1073BA');
define('COLOR_PIE_7', '#40B350');
define('COLOR_PIE_8', '#33ACE0');
define('COLOR_PIE_9', '#65318F');


// Tables
define('TABLE_CALENDAR', 'calendar');
define('TABLE_EDITIONS', 'editions');
define('TABLE_GALLERIES', 'galleries');
define('TABLE_LANGSTRINGS', 'langstrings');
define('TABLE_LOG', 'log_action');
define('TABLE_NEWS', 'news');
define('TABLE_PAGES', 'pages');
define('TABLE_ROLES', 'roles');
define('TABLE_TORENTJES', 'torentjes');
define('TABLE_USERS', 'users');

// Intervals
define('INTERVAL_KOPIJ', '8 days');
define('INTERVAL_TORENTJES', '6 weeks');

// Mail
define('EMAIL_FROM', 'no-reply@ronddetoren.nl');
define('EMAIL_INFO', 'info@ronddetoren.nl');

/* End of file constants.php */
/* Location: ./application/config/constants.php */
