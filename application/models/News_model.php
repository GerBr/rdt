<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class News_model extends CI_Model
{
    /**
     *
     * News
     * @author GB
     * @copyright 2018
     *
     */

    /**
     * Get news overview method
     * @param type $parameters
     * @return boolean
     */
    public function newsGetOverview($parameters = NULL)
    {
        if (isset($parameters->countAllRows)) {
            $this->db->select('COUNT(\'id\') AS counted');
        } else {
            $this->db->select('*');
        }
        $this->db->from(TABLE_NEWS);

        if (isset($parameters->filterVar)) {
            $this->db->where(TABLE_NEWS . '.' . $parameters->filterVar, $parameters->filterVal);
        }

        if (isset($parameters->showSoftDeletes)) {
            if ($parameters->showSoftDeletes !== 'both') {
                $this->db->where(TABLE_NEWS . '.soft_delete', 1);
            }
        } else {
           $this->db->where(TABLE_NEWS . '.soft_delete', 0);
        }
        if (isset($parameters->varname)) {
            $this->db->where($parameters->varname, $parameters->value);
        }

        if (isset($parameters->sort)) {
            $this->db->order_by($parameters->sort . ' ' . $parameters->order);
        } else {
            $this->db->order_by('id');
        }

        if (isset($parameters->columnSearch)) {
            foreach ($parameters->columnSearch as $column_search) {
                $this->db->like($column_search['variable'], $this->db->escape_str($column_search['value']), 'both');
            }
        }

        if (!isset($parameters->countAllRows)) {
            if (isset($parameters->sort)) {
                $this->db->order_by($parameters->sort . ' ' . $parameters->order);
            } else {
                $this->db->order_by('updated_at DESC');
            }
            if (isset($parameters->length)) {
                $this->db->limit($parameters->length, $parameters->start);
            }
        }

        $query = $this->db->get();
        $return = $query->result_array();
        $query->free_result();

        if (empty($return)) {
            return FALSE;
        } else {
            if (isset($parameters->calcEdition) && !isset($parameters->countAllRows)) {
                $this->load->model('Edition_model');
                foreach ($return as $key => $row) {
                    $edition = $this->Edition_model->editionGet($row['edition_id']);
                    $row['edition'] = date('Y', strtotime($edition['releasedate'])) . ' - ' . $edition['number'];
                    $return[$key] = $row;
                }    
            }
            return $return;
        }
    }

    /**
     * Get torentje method
     * @param int $id
     * @return boolean
     */
    public function newsGet($id)
    {
        $query = $this->db->get_where(TABLE_NEWS, ['id' => $id]);
        $return = $query->row_array();
        $query->free_result();
        if (empty($return)) {
            return FALSE;
        } else {
            return $return;
        }
    }

    /**
     * Insert or update torentje
     * @param array $data
     * @return int|bool
     */
    public function newsStore($data) {
        if (is_null($data['id'])) {
            $this->db->insert(TABLE_NEWS, $data);
            return $this->db->insert_id();
        }
        $id = $data['id'];
        unset($data['id']);
        $this->db->where('id', $id);
        
        $this->db->update(TABLE_NEWS, $data);
        return $id;
    }

    
// Class and file ends here.
}
