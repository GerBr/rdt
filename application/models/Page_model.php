<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Page_model extends CI_Model
{
    /**
     *
     * Pages
     * @author GB
     * @copyright 2018
     *
     */

    /**
     * Prefix segment to help routing
     * @var string
     */
    public $pageUrlPrefix = 'p/';
    
    /**
     * Get page overview method
     * @param type $parameters
     * @return boolean
     */
    public function pageGetOverview($parameters = NULL)
    {
//        var_dump($parameters);die;
        if (isset($parameters->countAllRows)) {
            $this->db->select('COUNT(\'id\') AS counted');
        } else {
            $this->db->select('id, name, updated_at');
        }
        $this->db->from(TABLE_PAGES);

        if (isset($parameters->filterVar)) {
            $this->db->where(TABLE_PAGES . '.' . $parameters->filterVar, $parameters->filterVal);
        }

        if (isset($parameters->showSoftDeletes)) {
            if ($parameters->showSoftDeletes !== 'both') {
                $this->db->where(TABLE_PAGES . '.soft_delete', 1);
            }
        } else {
           $this->db->where(TABLE_PAGES . '.soft_delete', 0);
        }
        if (isset($parameters->varname)) {
            $this->db->where($parameters->varname, $parameters->value);
        }

        if (isset($parameters->sort)) {
            $this->db->order_by($parameters->sort . ' ' . $parameters->order);
        } else {
            $this->db->order_by('id');
        }

        if (isset($parameters->columnSearch)) {
            foreach ($parameters->columnSearch as $column_search) {
                $this->db->like($column_search['variable'], $this->db->escape_str($column_search['value']), 'both');
            }
        }

        if (!isset($parameters->countAllRows)) {
            if (isset($parameters->sort)) {
                $this->db->order_by($parameters->sort . ' ' . $parameters->order);
            } else {
                $this->db->order_by(TABLE_PAGES . '.id ASC');
            }
            if (isset($parameters->length)) {
                $this->db->limit($parameters->length, $parameters->start);
            }
        }
        
        $query = $this->db->get();
        $return = $query->result_array();
        $query->free_result();

        if (empty($return)) {
            return FALSE;
        } else {
            if (isset($parameters->dropdown)) {
                foreach ($return as $row) {
                    $dropdown[$row[$parameters->dropdown['key']]] = $row[$parameters->dropdown['name']];
                }    
                return $dropdown;
            }
            return $return;
        }
    }

    /**
     * Get page method
     * @param mixed $identifier
     * @return boolean
     */
    public function pageGet($identifier)
    {
        if (is_int($identifier)) {
            $this->db->where(['id' => $identifier]);
        } else {
            $this->db->where(['url' => $identifier]);
        }
        $query = $this->db->get(TABLE_PAGES);
        $return = $query->row_array();
        $query->free_result();
        if (empty($return)) {
            return FALSE;
        } else {
            return $return;
        }
    }

    /**
     * Insert or update page
     * @param array $data
     * @return int|bool
     */
    public function pageStore($data) 
    {
        if (is_null($data['id'])) {
            $data['url'] = $this->createUrl(url_title($data['name'], '-', true));
            $this->db->insert(TABLE_PAGES, $data);
            return $this->db->insert_id();
        }
        $id = $data['id'];
        unset($data['id']);
        $this->db->where('id', $id);
        
        $this->db->update(TABLE_PAGES, $data);
        return $id;
    }

    /**
     * Create unique URL
     * @param string $urlName
     * @return string
     */
    public function createUrl($urlName) 
    {
        $urlName = substr($urlName, 0, 70);
        $query = $this->db->get_where(TABLE_PAGES, ['url' => $urlName]);
        $return = $query->row_array();
        $query->free_result();    
        if (empty($return)) {
            return $urlName;
        }
        
        for ($i=2; $i<10; $i++) {
            $query = $this->db->get_where(TABLE_PAGES, ['url' => $urlName . '-' . $i]);
            $return = $query->row_array();
            $query->free_result();    
            if (empty($return)) {
                return $urlName . '-'. $i;
            }
        }
        return substr($urlName, 0, 60) . time();
    }
    
    /**
     * Get menu structure page part
     */
    public function pageMenuItemsGet($parent = 0)
    {
        $this->db->select('id, name, url, parent');
        $this->db->where('parent', $parent);
        $this->db->where('soft_delete', 0);
        $this->db->order_by('sort_order ASC');
        $query = $this->db->get(TABLE_PAGES);
        $pages = $query->result_array();
        if (!$pages) {
            return false;
        }
        foreach($pages as $page) {
            $children = $this->pageMenuItemsGet($page['id']);
            if ($children) {
                $page['children'] = $children;
            }
            $page['url'] = $this->pageUrlPrefix . $page['url'];
            $return[] = $page;
        }
        return $return;
    }
    
// Class and file ends here.
}
