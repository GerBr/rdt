<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Torentjes_model extends CI_Model
{
    /**
     *
     * Torentjes/miniads
     * @author GB
     * @copyright 2018
     *
     */

    /**
     * Get Torentjes overview method
     * @param type $parameters
     * @return boolean
     */
    public function torentjeGetOverview($parameters = NULL)
    {
        if (isset($parameters->countAllRows)) {
            $this->db->select('COUNT(\'id\') AS counted');
        } else {
            $this->db->select('*');
        }
        $this->db->from(TABLE_TORENTJES);

        if (isset($parameters->filterVar)) {
            $this->db->where(TABLE_TORENTJES . '.' . $parameters->filterVar, $parameters->filterVal);
        }

        if (isset($parameters->varname)) {
            if (is_array($parameters->value)) {
                $this->db->where_in($parameters->varname, $parameters->value);
            } else {
                $this->db->where($parameters->varname, $parameters->value);
            }
        }

        if (isset($parameters->sort)) {
            $this->db->order_by($parameters->sort . ' ' . $parameters->order);
        } else {
            $this->db->order_by('id');
        }

        if (isset($parameters->columnSearch)) {
            foreach ($parameters->columnSearch as $column_search) {
                $this->db->like($column_search['variable'], $this->db->escape_str($column_search['value']), 'both');
            }
        }

        if (!isset($parameters->countAllRows)) {
            if (isset($parameters->sort)) {
                $this->db->order_by($parameters->sort . ' ' . $parameters->order);
            } else {
                $this->db->order_by('id ASC');
            }
            if (isset($parameters->length)) {
                $this->db->limit($parameters->length, $parameters->start);
            }
        }

        $query = $this->db->get();
        $return = $query->result_array();
        $query->free_result();

        if (empty($return)) {
            return FALSE;
        } else {
            if (isset($parameters->calcEdition) && !isset($parameters->countAllRows)) {
                $this->load->model('Edition_model');
                foreach ($return as $key => $row) {
                    $edition = $this->Edition_model->editionGet($row['edition_id']);
                    $row['end_date'] = date('d-m-Y', strtotime($edition['releasedate'] . '+ ' . INTERVAL_TORENTJES));
                    $row['edition'] = date('Y', strtotime($edition['releasedate'])) . ' - ' . $edition['number'];
                    $return[$key] = $row;
                }    
            }
            return $return;
        }
    }

    /**
     * Get torentje method
     * @param int $id
     * @return boolean
     */
    public function torentjeGet($id)
    {
        $query = $this->db->get_where(TABLE_TORENTJES, ['id' => $id]);
        $return = $query->row_array();
        $query->free_result();
        if (empty($return)) {
            return FALSE;
        } else {
            return $return;
        }
    }

    /**
     * Insert or update torentje
     * @param array $data
     * @return int|bool
     */
    public function torentjeStore($data) {
        if (is_null($data['id'])) {
            $this->db->insert(TABLE_TORENTJES, $data);
            return $this->db->insert_id();
        }
        $id = $data['id'];
        unset($data['id']);
        $this->db->where('id', $id);
        
        $this->db->update(TABLE_TORENTJES, $data);
        return $id;
    }

        
    /**
     * Delete old torentjes
     * @param array $allowedEditions 
     * @return void
     */
    public function torentjeCleanup($allowedEditions)
    {
        // Keep at least 1
        $this->db->select_max('id');
        $query = $this->db->get(TABLE_TORENTJES);
        $row = $query->row_array();
        
        // Delete what's not allowed and keep latest anyway
        $this->db->where_not_in('edition_id', $allowedEditions);
        $this->db->where('id !=', $row['id']);
        $this->db->delete(TABLE_TORENTJES);
    }
    
// Class and file ends here.
}
