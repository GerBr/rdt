<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Edition_model extends CI_Model
{
    /**
     *
     * editions
     * @author GB
     * @copyright 2018
     *
     */
    
    public $firstYear = 1967;

    /**
     * Get edition overview method
     * @param type $parameters
     * @return boolean
     */
    public function editionGetOverview($parameters = NULL)
    {
        if (isset($parameters->countAllRows)) {
            $this->db->select('COUNT(\'id\') AS counted');
        } else {
            $this->db->select('*');
        }
        $this->db->from(TABLE_EDITIONS);

        if (isset($parameters->varname)) {
            $this->db->where($parameters->varname, $parameters->value);
        }

        if (isset($parameters->showSoftDeletes)) {
            if ($parameters->showSoftDeletes !== 'both') {
                $this->db->where(TABLE_EDITIONS . '.soft_delete', 1);
            }
        } else {
           $this->db->where(TABLE_EDITIONS . '.soft_delete', 0);
        }
        if (isset($parameters->releaseDates)) {
            $this->db->where_in('releasedate', $parameters->releaseDates);
        }
        if (isset($parameters->withFile)) {
            $this->db->where('file !=', '');
        }
        if (isset($parameters->year)) {
            $this->db->where('YEAR(releasedate)', $parameters->year);
            $this->db->where('releasedate <', date('Y-m-d'));
        }
        
        if (isset($parameters->columnSearch)) {
            foreach ($parameters->columnSearch as $column_search) {
                $this->db->like($column_search['variable'], $this->db->escape_str($column_search['value']), 'both');
            }
        }

        if (!isset($parameters->countAllRows)) {
            if (isset($parameters->sort)) {
                $this->db->order_by($parameters->sort . ' ' . $parameters->order);
            } else {
                $this->db->order_by('releasedate ASC');
            }
            if (isset($parameters->length)) {
                $this->db->limit($parameters->length, $parameters->start);
            }
        }

        $query = $this->db->get();
        $return = $query->result_array();
        $query->free_result();

        if (empty($return)) {
            return FALSE;
        } else {
            if (isset($parameters->dropdown)) {
                foreach ($return as $row) {
                    $dropdown[$row['id']] = date('Y', strtotime($row['releasedate'])) . ' - ' . $row['number'];
                }    
                return $dropdown;
            }
            return $return;
        }
    }

    /**
     * Get edition method
     * @param int $id
     * @return boolean
     */
    public function editionGet($id)
    {
        $query = $this->db->get_where(TABLE_EDITIONS, ['id' => $id]);
        $return = $query->row_array();
        $query->free_result();
        if (empty($return)) {
            return FALSE;
        } else {
            return $return;
        }
    }

    /**
     * Insert or update edition
     * @param array $data
     * @return int|bool
     */
    public function editionStore($data) 
    {
        if (is_null($data['id'])) {
            $this->db->insert(TABLE_EDITIONS, $data);
            return $this->db->insert_id();
        }
        $id = $data['id'];
        unset($data['id']);
        $this->db->where('id', $id);
        
        $this->db->update(TABLE_EDITIONS, $data);
        return $id;
    }

    /**
     * Calculate year numbering
     */
    public function calcYear($releasedate) {
        $year = date('Y', strtotime($releasedate));
        return $year - 1967;
    }
    
// Class and file ends here.
}
