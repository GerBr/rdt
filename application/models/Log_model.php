<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Log_model extends CI_Model
{
    /**
     *
     * Action log functions
     * @author GB
     * @copyright 2018
     *
     */
    public $userId;
    
    public function __construct() 
    {
        parent::__construct();
        $this->userId = (int) $this->session->userdata('id');    
    }
    
    /**
     * Insert new entry in log
     * @param string $action
     * @param mixed $data
     * @param $overrideUser
     */
    public function logAdd($action, $data, $overrideUser = false)
    {
        // Prep the insert array
        $insert = array(
            'user_id' => $overrideUser ?: $this->userId,
            'action' => $action,
            'json' => json_encode($data),
            'created_at' => date('Y-m-d H:i:s'),
        );

        // Insert
        $this->db->insert(TABLE_LOG, $insert);
    }

    /**
     * Log overview
     *
     * @param object $parameters
     * @return array
     */
    public function logGetOverview($parameters = NULL)
    {

        if (isset($parameters->countAllRows)) {
            $this->db->select('COUNT(\'id\') AS counted');
        } else {
            $this->db->select('name,' . TABLE_LOG . '.*');
        }
        $this->db->from(TABLE_LOG);
        $this->db->join(TABLE_USERS, 'user_id = ' . TABLE_USERS . '.id', 'left');

        if (isset($parameters->filterVar)) {
            $this->db->where(TABLE_LOG . '.' . $parameters->filterVar, $parameters->filterVal);
        }

        if (isset($parameters->variable)) {
            $this->db->where($parameters->variable, $this->db->escape_str($parameters->value));
        }

        if (isset($parameters->columnSearch)) {
            foreach ($parameters->columnSearch as $column_search) {
                $this->db->like($column_search['variable'], $this->db->escape_str($column_search['value']), 'both');
            }
        }

        if (!isset($parameters->countAllRows)) {
            if (isset($parameters->sort)) {
                switch ($parameters->sort) {
                    case 'name':
                        $this->db->order_by(TABLE_USERS . '.' . $parameters->sort . ' ' . $parameters->order);
                        break;
                    default:
                        $this->db->order_by(TABLE_LOG . '.' . $parameters->sort . ' ' . $parameters->order);
                        break;
                }
            } else {
                $this->db->order_by(TABLE_ROLES . '.id DESC');
            }
            if (isset($parameters->length)) {
                $this->db->limit($parameters->length, $parameters->start);
            }
        }

        $query = $this->db->get();
        $return = $query->result_array();
        $query->free_result();

        if (empty($return)) {
            return FALSE;
        } else {
            return $return;
        }
    }
    
    /**
     * Get single log entry with username
     * @param int $id
     * @return array
     */
    public function logSingleGet($id) 
    {
        $this->db->select('name,' . TABLE_LOG . '.*');
        $this->db->from(TABLE_LOG);
        $this->db->join(TABLE_USERS, 'user_id = ' . TABLE_USERS . '.id', 'left');
        $this->db->where(TABLE_LOG . '.id', $id);
        
        $query = $this->db->get();
        $return = $query->row_array();
        $query->free_result();

        if (empty($return)) {
            return FALSE;
        } else {
            return $return;
        }
    }
// Class and file ends here.
}
