<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Content_model extends CI_Model
{
    /**
     *
     * Content functions
     * @author GB
     * @copyright 2018
     *
     */

    /**
     * Get textstring method
     * @param type $parameters
     * @return boolean
     */
    public function textstringGetOverview($parameters = NULL)
    {
        if (isset($parameters->countAllRows)) {
            $this->db->select('COUNT(\'id\') AS counted');
        } else {
            $this->db->select('*');
        }
        $this->db->from(TABLE_LANGSTRINGS);

        if (isset($parameters->varname)) {
            $this->db->where($parameters->varname, $parameters->value);
        }

        if (isset($parameters->variable)) {
            $this->db->where($parameters->variable, $this->db->escape_str($parameters->value));
        }

        if (isset($parameters->columnSearch)) {
            foreach ($parameters->columnSearch as $column_search) {
                $this->db->like($column_search['variable'], $this->db->escape_str($column_search['value']), 'both');
            }
        }

        if (!isset($parameters->countAllRows)) {
            if (isset($parameters->sort)) {
                $this->db->order_by($parameters->sort . ' ' . $parameters->order);
            } else {
                $this->db->order_by(TABLE_STRINGS . '.key ASC');
            }
            if (isset($parameters->length)) {
                $this->db->limit($parameters->length, $parameters->start);
            }
        }

        $query = $this->db->get();
        $return = $query->result_array();
        $query->free_result();

        if (empty($return)) {
            return FALSE;
        } else {
            return $return;
        }
    }

    /**
     * Get single langstring
     * @param int $id
     * @return array
     */
    public function textstringGet($id)
    {
        $query = $this->db->get_where(TABLE_LANGSTRINGS, ['id' => $id]);
        return $query->row_array();
    }
    
    /**
     * Get value of langstring
     * @param string $key
     * @return string
     */
    public function langstring($key)
    {
        $query = $this->db->get_where(TABLE_LANGSTRINGS, ['key' => $key]);
        $result = $query->row_array();
        return isset($result['text']) ? $result['text'] : $key;
    }
    
    /**
     * Update textstring in DB
     *
     * @param int $id
     * @param string $text
     * @return int
     */
    public function textstringStore($id, $text)
    {
        $this->db->set('text', $text);
        $this->db->where('id', $id);
        if ($this->db->update(TABLE_LANGSTRINGS)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * Get gallery method
     * @param type $parameters
     * @return boolean
     */
    public function galleryGetOverview($parameters = NULL)
    {
        if (isset($parameters->countAllRows)) {
            $this->db->select('COUNT(\'id\') AS counted');
        } else {
            $this->db->select('*');
        }
        $this->db->from(TABLE_GALLERIES);

        if (isset($parameters->varname)) {
            $this->db->where($parameters->varname, $parameters->value);
        }

        if (isset($parameters->variable)) {
            $this->db->where($parameters->variable, $this->db->escape_str($parameters->value));
        }

        if (isset($parameters->columnSearch)) {
            foreach ($parameters->columnSearch as $column_search) {
                $this->db->like($column_search['variable'], $this->db->escape_str($column_search['value']), 'both');
            }
        }

        if (!isset($parameters->countAllRows)) {
            if (isset($parameters->sort)) {
                $this->db->order_by($parameters->sort . ' ' . $parameters->order);
            } else {
                $this->db->order_by(TABLE_STRINGS . '.key ASC');
            }
            if (isset($parameters->length)) {
                $this->db->limit($parameters->length, $parameters->start);
            }
        }

        $query = $this->db->get();
        $return = $query->result_array();
        $query->free_result();

        if (empty($return)) {
            return FALSE;
        } else {
            return $return;
        }
    }
    
    /**
     * Get single gallery
     * @param int $id
     * @return array
     */
    public function galleryGet($id)
    {
        $query = $this->db->get_where(TABLE_GALLERIES, ['id' => $id]);
        return $query->row_array();
    }

    /**
     * Update or add gallery in DB
     *
     * @param array $data
     * @return int
     */
    public function galleryStore($data)
    {
        if (is_null($data['id'])) {
            $this->db->insert(TABLE_GALLERIES, $data);
            return $this->db->insert_id();
        }
        $id = $data['id'];
        unset($data['id']);
        $this->db->where('id', $id);
        
        $this->db->update(TABLE_GALLERIES, $data);
        return $id;
    }
    
    /**
     * @param obj $parameters
     * @return array
     */
    public function calendarGetOverview($parameters = NULL)
    {
        if (isset($parameters->countAllRows)) {
            $this->db->select('COUNT(\'id\') AS counted');
        } else {
            $this->db->select('*');
        }
        $this->db->from(TABLE_CALENDAR);

        if (isset($parameters->year)) {
            $this->db->like('date', $parameters->year, 'after');
        }

        if (isset($parameters->varname)) {
            $this->db->where($parameters->varname, $parameters->value);
        }

        if (isset($parameters->variable)) {
            $this->db->where($parameters->variable, $this->db->escape_str($parameters->value));
        }

        if (isset($parameters->columnSearch)) {
            foreach ($parameters->columnSearch as $column_search) {
                $this->db->like($column_search['variable'], $this->db->escape_str($column_search['value']), 'both');
            }
        }

        if (!isset($parameters->countAllRows)) {
            if (isset($parameters->sort)) {
                $this->db->order_by($parameters->sort . ' ' . $parameters->order);
            } else {
                $this->db->order_by('date ASC');
            }
            if (isset($parameters->length)) {
                $this->db->limit($parameters->length, $parameters->start);
            }
        }

        $query = $this->db->get();
        $return = $query->result_array();
        $query->free_result();

        if (empty($return)) {
            return FALSE;
        } else {
            return $return;
        }
    }
    /**
     * Get items for given year
     * grouped per month
     * @param int $year
     * @return array
     */
    public function calendarGetYearContent($year)
    {
        $this->db->select('MONTH(date) as month, DAY(date) as day,  text');
        $this->db->where('YEAR(date)', (int) $year);
        $this->db->order_by('date ASC');

        $query = $this->db->get(TABLE_CALENDAR);
        $result = $query->result_array();
        $query->free_result();

        if (empty($result)) {
            return FALSE;
        } 
        foreach ($result as $row) {
            $return[$row['month']][] = ['day' => $row['day'], 'text' => auto_link($row['text'], 'both', true)];
        }
        return $return;
    }

    /**
     * Get distinct years with items in them
     * @return array
     */
    public function calendarGetYears()
    {
        $this->db->select('distinct(YEAR(date)) as year');
        $this->db->where('date > NOW()');
        $this->db->order_by('date ASC');
        
        $query = $this->db->get(TABLE_CALENDAR);
        $result = $query->result_array();
        $query->free_result();

        if (empty($result)) {
            return FALSE;
        } 
        foreach ($result as $row) {
            $return[$row['year']] = $row['year'];
        }
        return $return;
    }

    /**
     * Get single calendar item
     * @param int $id
     * @return array
     */
    public function calendarItemGet($id)
    {
        $query = $this->db->get_where(TABLE_CALENDAR, ['id' => $id]);
        return $query->row_array();
    }
    
    /**
     * Update or add calendar item
     * @param array $data
     * @return int
     */
    public function calendarItemStore($data) 
    {
        if (is_null($data['id'])) {
            $this->db->insert(TABLE_CALENDAR, $data);
            return $this->db->insert_id();
        }
        $id = $data['id'];
        unset($data['id']);
        $this->db->where('id', $id);
        
        $this->db->update(TABLE_CALENDAR, $data);
        return $id; 
    }

    
    /**
     * Delete old calendaritems
     * @param type $date 
     */
    public function calendarCleanup($date)
    {
        $beforeDate = date('Y-m-d', strtotime($date));
        $this->db->where('date <', $beforeDate);
        $this->db->delete(TABLE_CALENDAR);
    }
    
    /**
     * Get message method
     * @param type $parameters
     * @return boolean
     */
    public function messageGetOverview($parameters = NULL)
    {
        if (isset($parameters->countAllRows)) {
            $this->db->select('COUNT(\'id\') AS counted');
        } else {
            $this->db->select('*');
        }
        $this->db->from(TABLE_MESSAGES);

        if (isset($parameters->varname)) {
            $this->db->where($parameters->varname, $parameters->value);
        }

        // Build custom filter - language
        if (isset($parameters->filter_language)) {
            $this->db->where('language', $parameters->filter_language);
        }

        if (isset($parameters->columnSearch)) {
            foreach ($parameters->columnSearch as $column_search) {
                $this->db->like($column_search['variable'], $this->db->escape_str($column_search['value']), 'both');
            }
        }

        if (isset($parameters->showSoftDeletes)) {
            $this->db->where(TABLE_MESSAGES . '.soft_delete', 1);
        } else {
            $this->db->where(TABLE_MESSAGES . '.soft_delete', 0);
        }

        if (isset($parameters->sort)) {
            $this->db->order_by($parameters->sort . ' ' . $parameters->order);
        } else {
            $this->db->order_by('id, message_header');
        }

        if (!isset($parameters->countAllRows)) {
            if (isset($parameters->rows)) {
                $this->db->limit($parameters->rows, (($parameters->page - 1) * $parameters->rows));
            }
        }

        $query = $this->db->get();
        $return = $query->result_array();
        $query->free_result();

        if (empty($return)) {
            return FALSE;
        } else {
            return $return;
        }
    }

    /**
     * Store data in messages table
     *
     * @param type $data
     * @return boolean
     */
    public function messageCreate($data)
    {
        
        $this->db->insert(TABLE_MESSAGES, $data);
        return $this->db->insert_id();
    }

    /**
     * Get message method
     * @param type $parameters
     * @return boolean
     */
    public function messageGet($parameters = NULL)
    {
       
        $this->db->select('*');
        $this->db->from(TABLE_MESSAGES);

        if (isset($parameters->varname)) {
            $this->db->where($parameters->varname, $parameters->value);
        }

        if (isset($parameters->language)) {
            $this->db->where('language', $parameters->language);
        }

        $query = $this->db->get();
        $return = $query->result_array();
        $query->free_result();

        if (empty($return)) {
            return FALSE;
        } else {
            return $return;
        }
    }

    /**
     * Get messages method
     * @param type $parameters
     * @return boolean
     */
    public function messagesGet($parameters)
    {
         $this->db->select('*');
        $this->db->from(TABLE_MESSAGES);
        $sql = '((CURDATE() >= date_from AND CURDATE() <= date_till)
                    OR
                (date_from = "0000-00-00" AND date_till = "0000-00-00"))
                AND role_id <= ' . $parameters->role_id . ' AND active = "1" AND soft_delete = "0"';

        if (isset($parameters->rows)) {
            $sql .= ' ORDER BY updated_at, created_at DESC LIMIT 0, ' . $parameters->rows . ';';
        }

        $this->db->where($sql);
        $query = $this->db->get();
        $return = $query->result_array();
        $query->free_result();

        if (empty($return)) {
            return FALSE;
        } else {
            return $return;
        }
    }

    /**
     * Update message in DB
     *
     * @param array $messageData
     * @return int
     *
     */
    public function messageUpdate($messageData)
    {
        $id = $messageData['id'];
        unset($messageData['id']);
        $messageData['updated_at'] = date('Y-m-d H:i:s');
        $this->db->where('id', $id);
        $this->db->update(TABLE_MESSAGES, $messageData);
        return TRUE;
    }

    /**
     * Soft delete message from DB
     *
     * @param object $parameters
     * @return int
     *
     */
    public function messageSoftDelete($parameters)
    {
        
        $this->db->where('id', $parameters->id);
        $data = array('updated_at' => date('Y-m-d H:i:s'), 'soft_delete' => 1);
        if ($this->db->update(TABLE_MESSAGES, $data)) {
            if ($this->db->affected_rows() > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    /**
     * Soft restore message from DB
     *
     * @param object $parameters
     * @return int
     *
     */
    public function messageSoftRestore($parameters)
    {
        $this->db->where('id', $parameters->id);
        $data = array('updated_at' => date('Y-m-d H:i:s'), 'soft_delete' => 0);
        if ($this->db->update(TABLE_MESSAGES, $data)) {
            if ($this->db->affected_rows() > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }
// Class and file ends here.
}
