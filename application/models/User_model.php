<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User_model extends CI_Model
{
    /**
     * user_model.php
     * @author GB
     *
     */

    /**
     * Default preferences for users
     * @var array
     */
    public $defaultPreferencesUsers = array(
        'site_lang' => 'en',
        'table_rows' => 10
    );

    /**
     * Login method
     *
     * @param string $email
     * @param string $password
     * @return array or FALSE
     *
     */
    public function userLoginGet($email, $password)
    {
        $this->db->select('*');
        $this->db->from(TABLE_USERS);
        $this->db->where('email', $email);
        $this->db->where('password', $password);
        $query = $this->db->get();
        $return = $query->row_array();
        $query->free_result();
        if (empty($return) || $return['soft_delete'] === '1') {
            return FALSE;
        } else {
            return $return;
        }
    }

    /**
     * Get user method
     *
     * @param type $variable
     * @param type $value
     * @return boolean
     */
    public function userGet($parameters = NULL)
    {
        $this->db->select('*');
        $this->db->from(TABLE_USERS);
        if (isset($parameters->variable)) {
            $this->db->where($parameters->variable, $parameters->value);
        }
        $query = $this->db->get();
        $return = $query->row_array();
        $query->free_result();
        if (empty($return)) {
            return FALSE;
        } else {
            return $return;
        }
    }


    /**
     * cleanup expired tokens for lost password
     * called when logging in
     * single query, no input/return
     */
    public function tokensDelete()
    {
        $this->db->where('token !=', '');
        $this->db->where('token_end <', date('Y-m-d H:i:s'));
        $this->db->set('token', NULL);
        $this->db->set('token_end', NULL);
        $this->db->update(TABLE_USERS);
    }

    /**
     * Test if email is unique
     * @param string $email
     * @param int $userid
     * @return boolean  TRUE if no match is found (unique), FALSE otherwise
     */
    public function emailUnique($email, $userid)
    {
        $this->db->where('email', $email);
        if ($userid > 0) {
            $this->db->where('id !=', $userid);
        }
        $result = $this->db->get(TABLE_USERS)->row_array();
        if ($result) {
            return FALSE;
        } else {
            return TRUE;
        }
    }
    
    /**
     * Insert or update user
     * @param array $data
     * @return int|bool
     */
    public function userStore($data) 
    {
        if (is_null($data['id'])) {
            $this->db->insert(TABLE_USERS, $data);
            return $this->db->insert_id();
        }
        $id = $data['id'];
        unset($data['id']);
        $this->db->where('id', $id);
        
        $this->db->update(TABLE_USERS, $data);
        return $id;
    }
    
    /**
     * Update user in DB
     *
     * @param array $userData
     * @return int
     *
     */
    public function userUpdate($userData)
    {
        // We must know who to update
        $userId = $userData['id'];
        unset($userData['id']);
        $this->db->where('id', $userId);
        $this->db->update(TABLE_USERS, $userData);
        return $userId;
    }

    /**
     * Write captcha in DB
     *
     * @param array $data
     * @return int
     *
     */
    public function captchaCreate($data)
    {
        $query = $this->db->insert_string('captcha', $data);
        $this->db->query($query);
    }

    /**
     * Delete old captcha from DB
     *
     * @param array $expiration
     * @return int
     *
     */
    public function captchaDelete($expiration)
    {
        $query = 'DELETE FROM captcha WHERE captcha_time < ' . $expiration;
        $this->db->query($query);
    }

    /**
     * Check captcha in DB
     *
     * @param array $data
     * @return int
     *
     */
    public function captchaCheck($data)
    {
        $sql = 'SELECT COUNT(*) AS count
                FROM captcha
                WHERE word = ?
                AND ip_address = ?
                AND captcha_time > ?';

        $query = $this->db->query($sql, array($data->word, $data->ipAddress, $data->captchaTime));
        $return = $query->result_array();
        if (empty($return)) {
            return FALSE;
        } else {
            return $return;
        }
    }

    /**
     * Get userGet method
     *
     * @param type $parameters
     * @return boolean
     */
    public function userGetOverview($parameters = NULL)
    {
        if (isset($parameters->countAllRows)) {
            $this->db->select('COUNT(\'id\') AS counted');
        } else {
            $this->db->select(TABLE_USERS . '.id, '
                . TABLE_USERS . '.email,'
                . TABLE_USERS . '.name,'
                . TABLE_USERS . '.last_login,'
                . TABLE_ROLES . '.name AS role'
                );
        }
        $this->db->from(TABLE_USERS);
        $this->db->join(TABLE_ROLES, TABLE_USERS . '.role_id = ' . TABLE_ROLES . '.id', 'LEFT');

        if (isset($parameters->filterVar)) {
            $this->db->where(TABLE_USERS . '.' . $parameters->filterVar, $parameters->filterVal);
        }

        if (isset($parameters->showSoftDeletes)) {
            if ($parameters->showSoftDeletes !== 'both') {
                $this->db->where(TABLE_USERS . '.soft_delete', 1);
            }
        } else {
            $this->db->where(TABLE_USERS . '.soft_delete', 0);
        }

        if (isset($parameters->variable)) {
            $this->db->where($parameters->variable, $this->db->escape_str($parameters->value));
        }

        if (isset($parameters->columnSearch)) {
            foreach ($parameters->columnSearch as $columnSearch) {
                switch($columnSearch['variable']) {
                    case 'name':
                        $col = TABLE_USERS . '.name';
                        break;
                    case 'role':
                        $col = TABLE_ROLES . '.name';
                        break;
                    default:
                        $col = $columnSearch['variable'];
                        break;
                }
                $this->db->like($col, $this->db->escape_str($columnSearch['value']), 'both');
            }
        }

        if (!isset($parameters->countAllRows)) {
            if (isset($parameters->sort)) {
                $this->db->order_by($parameters->sort . ' ' . $parameters->order);
            } else {
                $this->db->order_by(TABLE_USERS . '.id ASC');
            }
            if (isset($parameters->length)) {
                $this->db->limit($parameters->length, $parameters->start);
            }
        }
        $query = $this->db->get();
        $return = $query->result_array();
        $query->free_result();
        if (empty($return)) {
            return FALSE;
        } else {
            return $return;
        }
    }

    /**
     * Soft delete user from DB
     *
     * @param object $parameters
     * @return int
     *
     */
    public function userSoftDelete($parameters)
    {
        $this->db->where('id', $parameters->id);
        $data_userdetails = array('updated_at' => date('Y-m-d H:i:s'));
        $this->db->update(TABLE_USERSDETAILS, $data_userdetails);
        $this->db->where('id', $parameters->id);
        $data_user = array('updated_at' => date('Y-m-d H:i:s'), 'active' => 2, 'soft_delete' => 1);
        if ($this->db->update(TABLE_USERS, $data_user)) {
            if ($this->db->affected_rows() > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    /**
     * Soft restore user from DB
     *
     * @param object $parameters
     * @return int
     *
     */
    public function userSoftRestore($parameters)
    {
        $this->db->where('id', $parameters->id);
        $data = array('updated_at' => date('Y-m-d H:i:s'), 'soft_delete' => 0);
        if ($this->db->update(TABLE_USERS, $data)) {
            if ($this->db->affected_rows() > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    /**
     * Store new user preference - borrowed from LabelChecks
     * @param int $userId
     * @param array $preferences
     * @return bool
     */
    public function preferencesCreate($userId, $preferences)
    {
        // Get current user prefs
        $parameters = new stdClass();
        $parameters->variable = 'id';
        $parameters->value = $userId;
        $user = $this->userGet($parameters);

        if ($user['preferences']) {
            $user_prefs = unserialize($user['preferences']);
            if ($user_prefs) {
                $new_prefs = array_replace($user_prefs, $preferences);
            }
        } else {
            $new_prefs = $preferences;
        }

        $edit_user_data = array(
            'id' => $userId,
            'preferences' => serialize($new_prefs)
        );
        $this->userUpdate($edit_user_data);

        // Store in session
        $this->session->set_userdata($preferences);

        return TRUE;
    }

    /**
     * Set default preferences for all users
     * @return bool
     */
    public function userDefaultPreferencesSet()
    {
        $parameters = new stdClass();
        $parameters->showSoftDeletes = 'both';
        $all_users = $this->userGetOverview($parameters);

        $parameters = new stdClass();
        $parameters->variable = 'id';
        foreach ($all_users as $row) {
            $update = FALSE;
            $parameters->value = $row['id'];
            $user = $this->userGet($parameters);
            $user_prefs = unserialize($user['preferences']);
            foreach ($this->defaultPreferencesUsers as $d_key => $d_val) {
                if (!isset($user_prefs[$d_key])) {
                    $user_prefs[$d_key] = $d_val;
                    $update = TRUE;
                }
            }
            if ($update === TRUE) {
                $store = array(
                    'id' => $user['id'],
                    'preferences' => serialize($user_prefs),
                );
                $this->userUpdate($store);
            }
        }
        return TRUE;
    }

    /**
     * Get users not linked to an account
     */
    public function usersUnlinkedGet()
    {

        $sql = 'SELECT * FROM ' . TABLE_USERS . ' u
                WHERE u.soft_delete = 0
                AND u.active = 1
                AND u.id NOT in (SELECT user_id FROM ' . TABLE_ACCOUNT_USER . ' WHERE soft_delete = 0)';
        $query = $this->db->query($sql);
        $result = $query->result_array();
        $query->free_result();
        return $result;
    }
}
