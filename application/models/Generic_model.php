<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Generic_model extends CI_Model
{
    /**
     *
     * Generic DB functions
     * @author GB
     * @copyright 2018
     *
     */

    /**
     * Get next id
     * @param string $table
     * @param int $id
     * @return int|bool
     */
    public function idNextGet($table, $id)
    {
        $this->db->select('id');
        $this->db->from($table);
        $this->db->where('id > ' . $id);
        $this->db->order_by('id', 'ASC');
        $this->db->limit(1);
        $query = $this->db->get();
        $return = $query->row_array();
        $query->free_result();
        if (empty($return)) {
            return FALSE;
        } else {
            return $return['id'];
        }
    }

    /**
     * Get prev id
     * @param string $table
     * @param int $id
     * @return int|bool
     */
    public function idPrevGet($table, $id)
    {
        $this->db->select('id');
        $this->db->from($table);
        $this->db->where('id < ' . $id);
        $this->db->order_by('id', 'DESC');
        $this->db->limit(1);
        $query = $this->db->get();
        $return = $query->row_array();
        $query->free_result();
        if (empty($return)) {
            return FALSE;
        } else {
            return $return['id'];
        }
    }
    
    /**
     * Set soft delete status for record
     * @param string $table
     * @param int $id
     * @param int $status
     * @param bool $keepDate
     * @return bool
     */
    public function setSoftDeleteStatus($table, $id, $status, $keepDate = true)
    {
        if ($keepDate) {
            if ($this->columnExists($table, 'updated_at')) {
                $this->db->select('updated_at');
                $this->db->where('id', $id);
                $timestamp = $this->db->get($table)->row_array()['updated_at'];
                if ($timestamp) {
                    $this->db->set('updated_at', $timestamp);
                }
            }
        }
        
        $this->db->where('id', $id);
        $this->db->set('soft_delete', $status);
        return $this->db->update($table);
    }

    /**
     * Hard delete a record
     * @param string $table
     * @param int $id
     * @return bool
     */
    public function hardDelete($table, $id)
    {
        $this->db->where('id', $id);
        return $this->db->delete($table);
    }

    
    /**
     * Check if column exists
     * @param string $table The table to look into
     * @param string $fieldname The field name to search
     * @return bool
     */
    public function columnExists($table, $fieldname)
    {
        $this->db->limit(1); // We only need the headers so prevent loading millions of records
        $query = $this->db->get($table);
        $existingFields = $query->list_fields();
        $query->free_result();

        return in_array($fieldname, $existingFields);
    }
// Class and file ends here.
}
