<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Role_model extends CI_Model
{

    /**
     * Get roleGet method
     *
     * @param type $parameters
     * @return boolean
     */
    public function roleGet($parameters = NULL)
    {

        if (isset($parameters->countAllRows)) {
            $this->db->select('COUNT(\'id\') AS counted');
        } else {
            $this->db->select('*');
        }
        $this->db->from(TABLE_ROLES);

        if (isset($parameters->filterVar)) {
            $this->db->where(TABLE_ROLES . '.' . $parameters->filterVar, $parameters->filterVal);
        }

        if (isset($parameters->variable)) {
            $this->db->where($parameters->variable, $this->db->escape_str($parameters->value));
        }

        if (isset($parameters->columnSearch)) {
            foreach ($parameters->columnSearch as $column_search) {
                $this->db->like($column_search['variable'], $this->db->escape_str($column_search['value']), 'both');
            }
        }

        if (!isset($parameters->countAllRows)) {
            if (isset($parameters->sort)) {
                $this->db->order_by($parameters->sort . ' ' . $parameters->order);
            } else {
                $this->db->order_by(TABLE_ROLES . '.id ASC');
            }
            if (isset($parameters->length)) {
                $this->db->limit($parameters->length, $parameters->start);
            }
        }

        $query = $this->db->get();
        $return = $query->result_array();
        $query->free_result();

        if (empty($return)) {
            return FALSE;
        } else {
            if (isset($parameters->dropdown)) {
                foreach ($return as $row) {
                    $dropdown[$row['id']] = $row['name'] . ' - ' . $row['description'] ;
                }    
                return $dropdown;
            }
            return $return;
        }
    }

    /**
     * Update role in DB
     *
     * @param array $roleData
     * @return int
     *
     */
    public function roleUpdate($roleData)
    {
        // We must know who to update
        $id = $roleData['id'];
        unset($roleData['id']);
        $this->db->where('id', $id);
        $this->db->update(TABLE_ROLES, $roleData);
        return $id;
    }

    /**
     * Insert new role in DB
     *
     * @param array $roleData
     * @return int
     *
     */
    public function roleInsert($roleData)
    {
        $this->db->insert(TABLE_ROLES, $roleData);
        return $this->db->insert_id();
    }

    
// Class and file ends here.
}
