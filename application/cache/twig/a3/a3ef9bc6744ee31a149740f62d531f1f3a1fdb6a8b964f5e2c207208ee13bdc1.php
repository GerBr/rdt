<?php

/* login/newPass.twig */
class __TwigTemplate_fe84531d4bfa5006992f8122cae1e53f82dc038191871c60b6c3f4e631a2da23 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo ((($context["message"] ?? null)) ? (($context["message"] ?? null)) : (""));
        echo " 
";
        // line 2
        echo ((($context["warning"] ?? null)) ? (($context["warning"] ?? null)) : (""));
        echo " 

";
        // line 4
        echo form_open("login/newPassSave", array("class" => "form", "id" => "form"));
        echo "
";
        // line 5
        echo form_hidden("token", ($context["token"] ?? null));
        echo "
<div class=\"form-group\">";
        // line 6
        echo lang("login_change_password_text", "password");
        echo form_input(($context["optionsPassword"] ?? null));
        echo "</div>
<div class=\"form-group\">";
        // line 7
        echo lang("login_placeholder_password_confirm", "password_confirm");
        echo form_password(($context["optionsPasswordConfirm"] ?? null));
        echo "</div>
<div class=\"form-group text-right\">
    ";
        // line 9
        echo form_submit(array("class" => "btn btn-primary btn-lg", "name" => "submit", "value" => lang("form_submit_generic")));
        echo "
</div>
";
        // line 11
        echo form_close();
    }

    public function getTemplateName()
    {
        return "login/newPass.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  56 => 11,  51 => 9,  45 => 7,  40 => 6,  36 => 5,  32 => 4,  27 => 2,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ message|raw ?: '' }} 
{{ warning|raw ?: '' }} 

{{ form_open('login/newPassSave', {'class': 'form', 'id': 'form'}) }}
{{ form_hidden('token', token) }}
<div class=\"form-group\">{{ lang('login_change_password_text', 'password') }}{{ form_input(optionsPassword) }}</div>
<div class=\"form-group\">{{ lang('login_placeholder_password_confirm', 'password_confirm') }}{{ form_password(optionsPasswordConfirm) }}</div>
<div class=\"form-group text-right\">
    {{ form_submit({'class': 'btn btn-primary btn-lg', 'name': 'submit', 'value': lang('form_submit_generic')}) }}
</div>
{{ form_close() }}", "login/newPass.twig", "C:\\MAMP\\htdocs\\localdev\\rdt\\cms\\application\\views\\login\\newPass.twig");
    }
}
