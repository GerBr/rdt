<?php

/* content/newsForm.twig */
class __TwigTemplate_03af85ee14a5f4223e0f1b2d313fd0cb166f387963d0f4636b187088012dbbc9 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo ((($context["message"] ?? null)) ? (($context["message"] ?? null)) : (""));
        echo " 
";
        // line 2
        echo ((($context["warning"] ?? null)) ? (($context["warning"] ?? null)) : (""));
        echo " 

";
        // line 4
        echo form_open_multipart("cms/News/newsSave", array("class" => "form", "id" => "form"));
        echo "
";
        // line 5
        if (($context["id"] ?? null)) {
            // line 6
            echo form_hidden("id", ($context["id"] ?? null));
            echo "
";
        }
        // line 8
        echo "<div class=\"panel panel-default\">
    <div class=\"panel-heading\">
        <h3 class=\"panel-title\">
            ";
        // line 11
        if (($context["id"] ?? null)) {
            // line 12
            echo "                ";
            echo lang("news_edit");
            echo "
            ";
        } else {
            // line 14
            echo "                ";
            echo lang("news_add");
            echo "
            ";
        }
        // line 16
        echo "        </h3>
    </div>
    <div class=\"panel-body\">
        <div class=\"row\">
            <div class=\"form-group col-md-5\">
                ";
        // line 21
        echo lang("title", "title");
        echo "
                ";
        // line 22
        echo form_input(($context["optionsTitle"] ?? null));
        echo "
            </div>
            <div class=\"form-group col-md-3\">
                ";
        // line 25
        echo lang("edition", "edition");
        echo "
                ";
        // line 26
        echo form_dropdown("edition", ($context["allEditions"] ?? null), twig_get_attribute($this->env, $this->source, ($context["news"] ?? null), "edition_id", array()), "class=\"select2\" id=\"edition\" data-width=\"100%\"");
        echo "
            </div>
            <div class=\"form-group col-md-4\">
                ";
        // line 29
        echo lang("image", "image");
        echo "
                <div class=\"input-group\">
                    ";
        // line 31
        echo form_input(($context["optionsImg"] ?? null));
        echo "
                    <span class=\"input-group-btn\">
                        <a type=\"button\" class=\"btn btn-default\" data-toggle=\"modal\" data-target=\"#filemanagerModal\">Kies</a>
                    </span>
                </div>
            </div>
        </div>
        <div class=\"form-group\">
            ";
        // line 39
        echo lang("intro", "intro");
        echo "
            ";
        // line 40
        echo form_textarea(($context["optionsIntro"] ?? null));
        echo "
        </div>
    </div>
</div>
<div class=\"container-fluid\">
    <div class=\"row\">
        <div class=\"col-md-6\"><a class=\"btn btn-default\" href=\"";
        // line 46
        echo twig_escape_filter($this->env, site_url("cms/News"), "html", null, true);
        echo "\">";
        echo lang("form_cancel_generic");
        echo " </a></div>
        <div class=\"col-md-3\">";
        // line 47
        echo form_submit(array("class" => "btn btn-primary", "name" => "submit", "value" => lang("form_submit_generic")));
        echo "</div>
        <div class=\"col-md-3\">";
        // line 48
        echo form_submit(array("class" => "btn btn-primary", "name" => "submit_done", "value" => lang("form_submit_generic_done")));
        echo "</div>
    </div>
</div>
    
    
<div class=\"modal fade modal-lg\" id=\"filemanagerModal\" tabindex=\"-1\" role=\"dialog\">
  <div class=\"modal-dialog modal-lg\" role=\"document\">
    <div class=\"modal-content\">
      <div class=\"modal-header\">
        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
      </div>
      <div class=\"modal-body\">
        <iframe width=\"765\" height=\"550\" frameborder=\"0\" src=\"";
        // line 60
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "application/libraries/filemanager/dialog.php?type=1&field_id=img\"> </iframe>
      </div>
    </div>
  </div>
</div>
";
        // line 65
        echo form_close();
    }

    public function getTemplateName()
    {
        return "content/newsForm.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  151 => 65,  143 => 60,  128 => 48,  124 => 47,  118 => 46,  109 => 40,  105 => 39,  94 => 31,  89 => 29,  83 => 26,  79 => 25,  73 => 22,  69 => 21,  62 => 16,  56 => 14,  50 => 12,  48 => 11,  43 => 8,  38 => 6,  36 => 5,  32 => 4,  27 => 2,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ message|raw ?: '' }} 
{{ warning|raw ?: '' }} 

{{ form_open_multipart('cms/News/newsSave', {'class': 'form', 'id': 'form'}) }}
{% if id %}
{{ form_hidden('id', id) }}
{% endif %}
<div class=\"panel panel-default\">
    <div class=\"panel-heading\">
        <h3 class=\"panel-title\">
            {% if id  %}
                {{ lang('news_edit') }}
            {% else %}
                {{ lang('news_add') }}
            {% endif %}
        </h3>
    </div>
    <div class=\"panel-body\">
        <div class=\"row\">
            <div class=\"form-group col-md-5\">
                {{ lang('title', 'title') }}
                {{ form_input(optionsTitle) }}
            </div>
            <div class=\"form-group col-md-3\">
                {{ lang('edition', 'edition') }}
                {{ form_dropdown('edition', allEditions, news.edition_id, 'class=\"select2\" id=\"edition\" data-width=\"100%\"') }}
            </div>
            <div class=\"form-group col-md-4\">
                {{ lang('image', 'image') }}
                <div class=\"input-group\">
                    {{ form_input(optionsImg) }}
                    <span class=\"input-group-btn\">
                        <a type=\"button\" class=\"btn btn-default\" data-toggle=\"modal\" data-target=\"#filemanagerModal\">Kies</a>
                    </span>
                </div>
            </div>
        </div>
        <div class=\"form-group\">
            {{ lang('intro', 'intro') }}
            {{ form_textarea(optionsIntro) }}
        </div>
    </div>
</div>
<div class=\"container-fluid\">
    <div class=\"row\">
        <div class=\"col-md-6\"><a class=\"btn btn-default\" href=\"{{ site_url('cms/News') }}\">{{ lang('form_cancel_generic') }} </a></div>
        <div class=\"col-md-3\">{{ form_submit({'class': 'btn btn-primary', 'name': 'submit', 'value': lang('form_submit_generic')}) }}</div>
        <div class=\"col-md-3\">{{ form_submit({'class': 'btn btn-primary', 'name': 'submit_done', 'value': lang('form_submit_generic_done')}) }}</div>
    </div>
</div>
    
    
<div class=\"modal fade modal-lg\" id=\"filemanagerModal\" tabindex=\"-1\" role=\"dialog\">
  <div class=\"modal-dialog modal-lg\" role=\"document\">
    <div class=\"modal-content\">
      <div class=\"modal-header\">
        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
      </div>
      <div class=\"modal-body\">
        <iframe width=\"765\" height=\"550\" frameborder=\"0\" src=\"{{ base_url() }}application/libraries/filemanager/dialog.php?type=1&field_id=img\"> </iframe>
      </div>
    </div>
  </div>
</div>
{{ form_close() }}", "content/newsForm.twig", "C:\\MAMP\\htdocs\\localdev\\rdt\\cms\\application\\views\\content\\newsForm.twig");
    }
}
