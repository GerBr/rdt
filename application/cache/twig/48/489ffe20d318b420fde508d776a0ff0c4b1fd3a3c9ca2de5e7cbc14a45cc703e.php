<?php

/* content/textstringForm.twig */
class __TwigTemplate_f18f69aa4757935e98d32bcdec18f69f3e5bec396a6a361509d41971769787ad extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo ((($context["message"] ?? null)) ? (($context["message"] ?? null)) : (""));
        echo " 
";
        // line 2
        echo ((($context["warning"] ?? null)) ? (($context["warning"] ?? null)) : (""));
        echo " 

";
        // line 4
        echo form_open("cms/Content/textstringSave", array("class" => "form", "id" => "form"));
        echo "
";
        // line 5
        echo form_hidden("id", ($context["id"] ?? null));
        echo "
`<div class=\"panel panel-default\">
    <div class=\"panel-heading\">
        <h3 class=\"panel-title\">
            ";
        // line 9
        if (($context["id"] ?? null)) {
            // line 10
            echo "                ";
            echo lang("textstring_edit");
            echo "
            ";
        } else {
            // line 12
            echo "                ";
            echo lang("textstring_add");
            echo "
            ";
        }
        // line 14
        echo "        </h3>
    </div>
    <div class=\"panel-body\">
        <div class=\"well\">
            <h3>";
        // line 18
        echo lang("explanation");
        echo "</h3>
            <p>";
        // line 19
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["textstring"] ?? null), "explain", array()), "html", null, true);
        echo "</p>
        </div>
        <div class=\"form-group\">
            ";
        // line 22
        echo lang("text", "text");
        echo "
            ";
        // line 23
        echo form_textarea(($context["optionsText"] ?? null));
        echo "
        </div>
    </div>
</div>
<div class=\"container-fluid\">
    <div class=\"row\">
        <div class=\"col-md-6\"><a class=\"btn btn-default\" href=\"";
        // line 29
        echo twig_escape_filter($this->env, site_url("cms/Content/textstringAsync"), "html", null, true);
        echo "\">";
        echo lang("form_cancel_generic");
        echo " </a></div>
        <div class=\"col-md-3\">";
        // line 30
        echo form_submit(array("class" => "btn btn-primary", "name" => "submit", "value" => lang("form_submit_generic")));
        echo "</div>
        <div class=\"col-md-3\">";
        // line 31
        echo form_submit(array("class" => "btn btn-primary", "name" => "submit_done", "value" => lang("form_submit_generic_done")));
        echo "</div>
    </div>
</div>

";
        // line 35
        echo form_close();
    }

    public function getTemplateName()
    {
        return "content/textstringForm.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  103 => 35,  96 => 31,  92 => 30,  86 => 29,  77 => 23,  73 => 22,  67 => 19,  63 => 18,  57 => 14,  51 => 12,  45 => 10,  43 => 9,  36 => 5,  32 => 4,  27 => 2,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ message|raw ?: '' }} 
{{ warning|raw ?: '' }} 

{{ form_open('cms/Content/textstringSave', {'class': 'form', 'id': 'form'}) }}
{{ form_hidden('id', id) }}
`<div class=\"panel panel-default\">
    <div class=\"panel-heading\">
        <h3 class=\"panel-title\">
            {% if id  %}
                {{ lang('textstring_edit') }}
            {% else %}
                {{ lang('textstring_add') }}
            {% endif %}
        </h3>
    </div>
    <div class=\"panel-body\">
        <div class=\"well\">
            <h3>{{ lang('explanation') }}</h3>
            <p>{{ textstring.explain }}</p>
        </div>
        <div class=\"form-group\">
            {{ lang('text', 'text') }}
            {{ form_textarea(optionsText) }}
        </div>
    </div>
</div>
<div class=\"container-fluid\">
    <div class=\"row\">
        <div class=\"col-md-6\"><a class=\"btn btn-default\" href=\"{{ site_url('cms/Content/textstringAsync') }}\">{{ lang('form_cancel_generic') }} </a></div>
        <div class=\"col-md-3\">{{ form_submit({'class': 'btn btn-primary', 'name': 'submit', 'value': lang('form_submit_generic')}) }}</div>
        <div class=\"col-md-3\">{{ form_submit({'class': 'btn btn-primary', 'name': 'submit_done', 'value': lang('form_submit_generic_done')}) }}</div>
    </div>
</div>

{{ form_close() }}", "content/textstringForm.twig", "C:\\MAMP\\htdocs\\localdev\\rdt\\cms\\application\\views\\content\\textstringForm.twig");
    }
}
