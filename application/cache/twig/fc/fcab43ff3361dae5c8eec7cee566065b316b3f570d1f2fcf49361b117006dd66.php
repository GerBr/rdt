<?php

/* login/loginFooter.twig */
class __TwigTemplate_52cbc5cadfce495457bf595585ee47b44916de22c0eb5e8d863bcf4d4a932a85 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
</div>

</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 2.2.3 -->
<script type=\"text/javascript\" src=\"js/jQuery/jquery-2.2.3.min.js\"></script>
<!-- Bootstrap 3.3.6 -->
<script type=\"text/javascript\" src=\"js/bootstrap.min.js\"></script>
<!-- AdminLTE App -->
<script type=\"text/javascript\" src=\"js/app.min.js\"></script>


";
        // line 17
        if (($context["form_validator"] ?? null)) {
            // line 18
            echo "    <!-- Form validator -->
    <script src=\"js/form-validator/jquery.form-validator.min.js\"></script>
    <script src=\"js/form-validator/lang/locale.en.js\"></script>
    <script type=\"text/javascript\">
        \$.validate({validateOnBlur: true,
            scrollToTopOnError: false,
            language: enErrorDialogs,
            modules: \"security\",
            onModulesLoaded: function () {
                var optionalConfig = {
                    fontSize: \"10pt\",
                    padding: \"4px\",
                    bad: \"";
            // line 30
            echo lang("form_password_bad");
            echo "\",
                    weak: \"";
            // line 31
            echo lang("form_password_weak");
            echo "\",
                    good: \"";
            // line 32
            echo lang("form_password_good");
            echo "\",
                    strong: \"";
            // line 33
            echo lang("form_password_strong");
            echo "\"
                };
                \$(\"input[name='password_confirmation']\").displayPasswordStrength(optionalConfig);
            }
        });
    </script>
";
        }
        // line 40
        echo "</body>
</html>
";
    }

    public function getTemplateName()
    {
        return "login/loginFooter.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  79 => 40,  69 => 33,  65 => 32,  61 => 31,  57 => 30,  43 => 18,  41 => 17,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("
</div>

</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 2.2.3 -->
<script type=\"text/javascript\" src=\"js/jQuery/jquery-2.2.3.min.js\"></script>
<!-- Bootstrap 3.3.6 -->
<script type=\"text/javascript\" src=\"js/bootstrap.min.js\"></script>
<!-- AdminLTE App -->
<script type=\"text/javascript\" src=\"js/app.min.js\"></script>


{% if form_validator %}
    <!-- Form validator -->
    <script src=\"js/form-validator/jquery.form-validator.min.js\"></script>
    <script src=\"js/form-validator/lang/locale.en.js\"></script>
    <script type=\"text/javascript\">
        \$.validate({validateOnBlur: true,
            scrollToTopOnError: false,
            language: enErrorDialogs,
            modules: \"security\",
            onModulesLoaded: function () {
                var optionalConfig = {
                    fontSize: \"10pt\",
                    padding: \"4px\",
                    bad: \"{{ lang('form_password_bad') }}\",
                    weak: \"{{ lang('form_password_weak') }}\",
                    good: \"{{ lang('form_password_good') }}\",
                    strong: \"{{ lang('form_password_strong') }}\"
                };
                \$(\"input[name='password_confirmation']\").displayPasswordStrength(optionalConfig);
            }
        });
    </script>
{% endif %}
</body>
</html>
", "login/loginFooter.twig", "C:\\MAMP\\htdocs\\localdev\\rdt\\cms\\application\\views\\login\\loginFooter.twig");
    }
}
