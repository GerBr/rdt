<?php

/* front/archive.twig */
class __TwigTemplate_380285f5e9507a59eddeb85c14b8b0bceaf342d2082d6374f721fde40c6f0ef7 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"content-container\">
    <h2>";
        // line 2
        echo twig_escape_filter($this->env, ($context["pageTitle"] ?? null), "html", null, true);
        echo "</h2>
    <div class=\"content\">

        <div class=\"col-md-6 col-xs-12\">
            ";
        // line 6
        echo langstring("archiveIntro");
        echo "

            <div class=\"well well-lg\">
                ";
        // line 9
        echo form_open("Archief/index", array("class" => "form panel-body", "id" => "form"));
        echo "
                <div class=\"form_group\">
                    ";
        // line 11
        echo lang("releaseyear", "year");
        echo " ";
        echo form_dropdown("year", ($context["selectYears"] ?? null), ($context["year"] ?? null), "class=\"form_control\"");
        echo "
                </div>
                ";
        // line 13
        echo form_close();
        echo "
            </div>
            <ul class=\"list-group\">
            ";
        // line 16
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["editions"] ?? null));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["row"]) {
            // line 17
            echo "                <a class=\"list-group-item\" href=\"";
            echo twig_escape_filter($this->env, base_url(), "html", null, true);
            echo "uploads/editions/";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["row"], "file", array()), "html", null, true);
            echo "\">
                    Jaargang ";
            // line 18
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["row"], "releaseYear", array()), "html", null, true);
            echo " nummer ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["row"], "number", array()), "html", null, true);
            echo " van ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["row"], "releasedate", array()), "d-m-Y"), "html", null, true);
            echo "
                </a>
            ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 21
            echo "                <p>";
            echo lang("no_data_found");
            echo "</p>   
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['row'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 23
        echo "            </ul>
        </div>
    </div>
</div>
<script>
    \$(function(){ 
        \$('select').change(function(e) {
            \$(this).parents('form').submit();
        });
    });
</script>";
    }

    public function getTemplateName()
    {
        return "front/archive.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  90 => 23,  81 => 21,  69 => 18,  62 => 17,  57 => 16,  51 => 13,  44 => 11,  39 => 9,  33 => 6,  26 => 2,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"content-container\">
    <h2>{{ pageTitle }}</h2>
    <div class=\"content\">

        <div class=\"col-md-6 col-xs-12\">
            {{ langstring('archiveIntro') }}

            <div class=\"well well-lg\">
                {{ form_open('Archief/index', {'class': 'form panel-body', 'id': 'form'}) }}
                <div class=\"form_group\">
                    {{ lang('releaseyear', 'year') }} {{ form_dropdown('year', selectYears, year, 'class=\"form_control\"') }}
                </div>
                {{ form_close() }}
            </div>
            <ul class=\"list-group\">
            {% for row in editions %}
                <a class=\"list-group-item\" href=\"{{ base_url() }}uploads/editions/{{ row.file }}\">
                    Jaargang {{ row.releaseYear }} nummer {{ row.number }} van {{ row.releasedate|date(\"d-m-Y\") }}
                </a>
            {% else %}
                <p>{{ lang('no_data_found') }}</p>   
            {% endfor %}
            </ul>
        </div>
    </div>
</div>
<script>
    \$(function(){ 
        \$('select').change(function(e) {
            \$(this).parents('form').submit();
        });
    });
</script>", "front/archive.twig", "C:\\MAMP\\htdocs\\localdev\\rdt\\cms\\application\\views\\front\\archive.twig");
    }
}
