<?php

/* content/pageForm.twig */
class __TwigTemplate_bfafc548771fdd38a9de8f073f01255c948c75a50816043c42756c20d2ee834f extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo ((($context["message"] ?? null)) ? (($context["message"] ?? null)) : (""));
        echo " 
";
        // line 2
        echo ((($context["warning"] ?? null)) ? (($context["warning"] ?? null)) : (""));
        echo " 

";
        // line 4
        echo form_open("cms/Page/pageSave", array("class" => "form", "id" => "form"));
        echo "
";
        // line 5
        if (($context["id"] ?? null)) {
            // line 6
            echo form_hidden("id", ($context["id"] ?? null));
            echo "
";
        }
        // line 8
        echo "<div class=\"panel panel-default\">
    <div class=\"panel-heading\">
        <h3 class=\"panel-title\">
            ";
        // line 11
        if (($context["id"] ?? null)) {
            // line 12
            echo "                ";
            echo lang("page_edit");
            echo "
            ";
        } else {
            // line 14
            echo "                ";
            echo lang("page_add");
            echo "
            ";
        }
        // line 16
        echo "        </h3>
    </div>
    <div class=\"panel-body\">
        <div class=\"row\">
            <div class=\"form-group col-md-4\">
                ";
        // line 21
        echo lang("page_name", "page_name");
        echo "
                ";
        // line 22
        if (($context["id"] ?? null)) {
            echo " 
                    <div class=\"input-group\">
                        <span class=\"input-group-addon\">";
            // line 24
            echo twig_escape_filter($this->env, ($context["id"] ?? null), "html", null, true);
            echo "</span>
                        <div class=\"form-group\">
                ";
        }
        // line 27
        echo "                ";
        echo form_input(($context["optionsPageName"] ?? null));
        echo "
                ";
        // line 28
        if (($context["id"] ?? null)) {
            echo " </div></div>";
        }
        // line 29
        echo "            </div>
            <div class=\"form-group col-md-4\">
                ";
        // line 31
        echo lang("parent", "parent");
        echo "
                ";
        // line 32
        echo form_dropdown("parent", ($context["allPages"] ?? null), twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "parent", array()), "class=\"select2\" id=\"parent\" data-width=\"100%\"");
        echo "
            </div>
            ";
        // line 34
        if (($context["id"] ?? null)) {
            echo " 
";
            // line 36
            echo "                <div class=\"col-md-4\"><div class=\"form-group\">";
            echo lang("updated_at", "updated_at");
            echo " ";
            echo form_input(($context["optionsUpdatedAt"] ?? null));
            echo "</div></div>
            ";
        }
        // line 38
        echo "        </div>
        <div class=\"form-group\">";
        // line 39
        echo lang("page_content", "page_content");
        echo " ";
        echo form_textarea(($context["optionsPageContent"] ?? null));
        echo "</div>
    </div>
</div>
<div class=\"container-fluid\">
    <div class=\"row\">
        <div class=\"col-md-6\"><a class=\"btn btn-default\" href=\"";
        // line 44
        echo twig_escape_filter($this->env, site_url("cms/page/index"), "html", null, true);
        echo "\">";
        echo lang("form_cancel_generic");
        echo " </a></div>
        <div class=\"col-md-3\">";
        // line 45
        echo form_submit(array("class" => "btn btn-primary", "name" => "submit", "value" => lang("form_submit_generic")));
        echo "</div>
        <div class=\"col-md-3\">";
        // line 46
        echo form_submit(array("class" => "btn btn-primary", "name" => "submit_done", "value" => lang("form_submit_generic_done")));
        echo "</div>
    </div>
</div>

";
        // line 50
        echo form_close();
    }

    public function getTemplateName()
    {
        return "content/pageForm.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  148 => 50,  141 => 46,  137 => 45,  131 => 44,  121 => 39,  118 => 38,  110 => 36,  106 => 34,  101 => 32,  97 => 31,  93 => 29,  89 => 28,  84 => 27,  78 => 24,  73 => 22,  69 => 21,  62 => 16,  56 => 14,  50 => 12,  48 => 11,  43 => 8,  38 => 6,  36 => 5,  32 => 4,  27 => 2,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ message|raw ?: '' }} 
{{ warning|raw ?: '' }} 

{{ form_open('cms/Page/pageSave', {'class': 'form', 'id': 'form'}) }}
{% if id %}
{{ form_hidden('id', id) }}
{% endif %}
<div class=\"panel panel-default\">
    <div class=\"panel-heading\">
        <h3 class=\"panel-title\">
            {% if id  %}
                {{ lang('page_edit') }}
            {% else %}
                {{ lang('page_add') }}
            {% endif %}
        </h3>
    </div>
    <div class=\"panel-body\">
        <div class=\"row\">
            <div class=\"form-group col-md-4\">
                {{ lang('page_name', 'page_name') }}
                {% if id %} 
                    <div class=\"input-group\">
                        <span class=\"input-group-addon\">{{ id }}</span>
                        <div class=\"form-group\">
                {% endif %}
                {{ form_input(optionsPageName) }}
                {% if id %} </div></div>{% endif %}
            </div>
            <div class=\"form-group col-md-4\">
                {{ lang('parent', 'parent') }}
                {{ form_dropdown('parent', allPages, page.parent, 'class=\"select2\" id=\"parent\" data-width=\"100%\"') }}
            </div>
            {% if id %} 
{#                <div class=\"col-md-3\"><div class=\"form-group\">{{ lang('created_at', 'created_at') }} {{ form_input(optionsCreatedAt) }}</div></div>#}
                <div class=\"col-md-4\"><div class=\"form-group\">{{ lang('updated_at', 'updated_at') }} {{ form_input(optionsUpdatedAt) }}</div></div>
            {% endif %}
        </div>
        <div class=\"form-group\">{{ lang('page_content', \"page_content\") }} {{ form_textarea(optionsPageContent) }}</div>
    </div>
</div>
<div class=\"container-fluid\">
    <div class=\"row\">
        <div class=\"col-md-6\"><a class=\"btn btn-default\" href=\"{{ site_url('cms/page/index') }}\">{{ lang('form_cancel_generic') }} </a></div>
        <div class=\"col-md-3\">{{ form_submit({'class': 'btn btn-primary', 'name': 'submit', 'value': lang('form_submit_generic')}) }}</div>
        <div class=\"col-md-3\">{{ form_submit({'class': 'btn btn-primary', 'name': 'submit_done', 'value': lang('form_submit_generic_done')}) }}</div>
    </div>
</div>

{{ form_close() }}", "content/pageForm.twig", "C:\\MAMP\\htdocs\\localdev\\rdt\\cms\\application\\views\\content\\pageForm.twig");
    }
}
