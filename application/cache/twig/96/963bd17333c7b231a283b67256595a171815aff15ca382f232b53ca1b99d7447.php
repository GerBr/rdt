<?php

/* generic/dataTable.twig */
class __TwigTemplate_42d82b7c49fbf43d9056bb61e4e0737d27d31114b0d7061de19e6f7b678cfdd9 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo ((($context["warning"] ?? null)) ? (($context["warning"] ?? null)) : (""));
        echo "
";
        // line 2
        echo ((($context["message"] ?? null)) ? (($context["message"] ?? null)) : (""));
        echo "

<div class=\"toolbar\"></div>

<table id=\"dg\" class=\"table table-bordered table-hover\">
    <thead>
        <tr>
            ";
        // line 9
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["columns"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["col"]) {
            // line 10
            echo "                <th field=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["col"], "name", array()), "html", null, true);
            echo "\" width=\"auto\">";
            echo ((twig_get_attribute($this->env, $this->source, $context["col"], "langname", array())) ? (lang(twig_get_attribute($this->env, $this->source, $context["col"], "langname", array()))) : (lang(twig_get_attribute($this->env, $this->source, $context["col"], "name", array()))));
            echo "</th>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['col'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 12
        echo "        </tr>
    </thead>
</table>";
    }

    public function getTemplateName()
    {
        return "generic/dataTable.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  52 => 12,  41 => 10,  37 => 9,  27 => 2,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ warning|raw ?: '' }}
{{ message|raw ?: '' }}

<div class=\"toolbar\"></div>

<table id=\"dg\" class=\"table table-bordered table-hover\">
    <thead>
        <tr>
            {% for col in columns %}
                <th field=\"{{ col.name }}\" width=\"auto\">{{ col.langname ? lang(col.langname) : lang(col.name) }}</th>
            {% endfor %}
        </tr>
    </thead>
</table>", "generic/dataTable.twig", "C:\\MAMP\\htdocs\\localdev\\rdt\\cms\\application\\views\\generic\\dataTable.twig");
    }
}
