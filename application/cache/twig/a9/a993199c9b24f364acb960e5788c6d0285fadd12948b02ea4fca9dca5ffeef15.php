<?php

/* front/news.twig */
class __TwigTemplate_968b0d8f9f0658ae9d77fedca09a228d281624f118c5a3afa84490de95c42cac extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"content-container\">
    <h2>";
        // line 2
        echo twig_escape_filter($this->env, ($context["pageTitle"] ?? null), "html", null, true);
        echo "</h2>
    ";
        // line 3
        echo twig_escape_filter($this->env, ($context["welcome"] ?? null), "html", null, true);
        echo "
    ";
        // line 4
        echo twig_escape_filter($this->env, ($context["newsIntro"] ?? null), "html", null, true);
        echo "
    <div class=\"content col-lg-9 col-md-12\">
        <ul class=\"media-list\">
            
        ";
        // line 8
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["news"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 9
            echo "            <li class=\"media\">
                <div class=\"media-body\">
                    <h3 class=\"media-heading\">";
            // line 11
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "title", array()), "html", null, true);
            echo "</h3>
                    ";
            // line 12
            echo twig_get_attribute($this->env, $this->source, $context["item"], "intro", array());
            echo "
                </div>
                <div class=\"media-right\">    
                    <a data-toggle=\"modal\" href=\"#imgModal\" class=\"newsImg\" data-orig=\"";
            // line 15
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "img", array()), "html", null, true);
            echo "\">
                        <img class=\"media-object\" src=\"";
            // line 16
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "thumb", array()), "html", null, true);
            echo "\" />
                    </a>
                </div>
                    <br>
            </li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 22
        echo "        </ul>
    </div>
</div>

<div class=\"modal fade\" id=\"imgModal\" tabindex=\"-1\" role=\"dialog\">
    <div class=\"modal-dialog modal-full\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-body\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                <img id=\"fullImg\" src=\"\" />
            </div>
        </div>
    </div>
</div>
        
<script>
    \$(function(){ 
        \$('.newsImg').click(function(e) {
            \$('#fullImg').attr('src', \$(this).attr('data-orig'));
        });
    });
</script>";
    }

    public function getTemplateName()
    {
        return "front/news.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  75 => 22,  63 => 16,  59 => 15,  53 => 12,  49 => 11,  45 => 9,  41 => 8,  34 => 4,  30 => 3,  26 => 2,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"content-container\">
    <h2>{{ pageTitle }}</h2>
    {{ welcome }}
    {{ newsIntro }}
    <div class=\"content col-lg-9 col-md-12\">
        <ul class=\"media-list\">
            
        {% for item in news %}
            <li class=\"media\">
                <div class=\"media-body\">
                    <h3 class=\"media-heading\">{{ item.title }}</h3>
                    {{ item.intro|raw }}
                </div>
                <div class=\"media-right\">    
                    <a data-toggle=\"modal\" href=\"#imgModal\" class=\"newsImg\" data-orig=\"{{ item.img }}\">
                        <img class=\"media-object\" src=\"{{ item.thumb }}\" />
                    </a>
                </div>
                    <br>
            </li>
        {% endfor %}
        </ul>
    </div>
</div>

<div class=\"modal fade\" id=\"imgModal\" tabindex=\"-1\" role=\"dialog\">
    <div class=\"modal-dialog modal-full\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-body\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                <img id=\"fullImg\" src=\"\" />
            </div>
        </div>
    </div>
</div>
        
<script>
    \$(function(){ 
        \$('.newsImg').click(function(e) {
            \$('#fullImg').attr('src', \$(this).attr('data-orig'));
        });
    });
</script>", "front/news.twig", "C:\\MAMP\\htdocs\\localdev\\rdt\\cms\\application\\views\\front\\news.twig");
    }
}
