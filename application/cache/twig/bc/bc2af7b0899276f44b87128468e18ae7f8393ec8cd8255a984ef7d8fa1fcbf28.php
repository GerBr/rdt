<?php

/* front/footer.twig */
class __TwigTemplate_ee3df561c856258aaca18432b973815141282bce4f8f483c3817ca9fbef46584 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "        </div> <!-- /.container-fluid -->
    </div> <!-- /.main-container -->
    <div style=\"height: 50px\"></div>
    <div class=\"nav navbar-inverse navbar-fixed-bottom\">
        <footer><p>&copy; Rond de Toren ";
        // line 5
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, ($context["now"] ?? null), "Y"), "html", null, true);
        echo "</p></footer>
    </div>

    <script type=\"text/javascript\" src=\"";
        // line 8
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "js/jQuery/jquery-2.2.3.min.js\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 9
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "js/bootstrap.min.js\"></script>
    <script>defer\$()</script>
</body>
</html>";
    }

    public function getTemplateName()
    {
        return "front/footer.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  39 => 9,  35 => 8,  29 => 5,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("        </div> <!-- /.container-fluid -->
    </div> <!-- /.main-container -->
    <div style=\"height: 50px\"></div>
    <div class=\"nav navbar-inverse navbar-fixed-bottom\">
        <footer><p>&copy; Rond de Toren {{ now|date(\"Y\") }}</p></footer>
    </div>

    <script type=\"text/javascript\" src=\"{{ base_url() }}js/jQuery/jquery-2.2.3.min.js\"></script>
    <script type=\"text/javascript\" src=\"{{ base_url() }}js/bootstrap.min.js\"></script>
    <script>defer\$()</script>
</body>
</html>", "front/footer.twig", "C:\\MAMP\\htdocs\\localdev\\rdt\\cms\\application\\views\\front\\footer.twig");
    }
}
