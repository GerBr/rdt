<?php

/* generic/footer.twig */
class __TwigTemplate_6f16aa4e96ed75693132239426c24e6f96e3a48737652f8d24e7dccc6d440973 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "</section>
<!-- /.content -->

";
        // line 4
        if (($context["footer_menu"] ?? null)) {
            // line 5
            echo "    <ul class=\"pager\">
        <li ";
            // line 6
            echo ((twig_get_attribute($this->env, $this->source, ($context["footer_menu"] ?? null), "prev_id", array())) ? ("") : ("class=\"disabled\""));
            echo "><a href=\"";
            echo twig_escape_filter($this->env, ((twig_get_attribute($this->env, $this->source, ($context["footer_menu"] ?? null), "prev_id", array())) ? (((twig_get_attribute($this->env, $this->source, ($context["footer_menu"] ?? null), "url", array()) . "/") . twig_get_attribute($this->env, $this->source, ($context["footer_menu"] ?? null), "prev_id", array()))) : ("javascript: void(0)")), "html", null, true);
            echo "\"><span class=\"glyphicon glyphicon-chevron-left\"></span> ";
            echo lang("form_previous");
            echo "</a></li>
        <li ";
            // line 7
            echo ((twig_get_attribute($this->env, $this->source, ($context["footer_menu"] ?? null), "next_id", array())) ? ("") : ("class=\"disabled\""));
            echo "><a href=\"";
            echo twig_escape_filter($this->env, ((twig_get_attribute($this->env, $this->source, ($context["footer_menu"] ?? null), "next_id", array())) ? (((twig_get_attribute($this->env, $this->source, ($context["footer_menu"] ?? null), "url", array()) . "/") . twig_get_attribute($this->env, $this->source, ($context["footer_menu"] ?? null), "next_id", array()))) : ("javascript: void(0)")), "html", null, true);
            echo "\">";
            echo lang("form_next");
            echo " <span class=\"glyphicon glyphicon-chevron-right\"></span></a></li>
    </ul>
";
        }
        // line 10
        echo "
</div>
<!-- /.content-wrapper -->

<!-- Main Footer -->
<footer class=\"main-footer\">
    <!-- To the right -->
    <div class=\"pull-right hidden-xs\"></div>
    <!-- Default to the left -->
    <strong>Copyright &copy; <a href=\"https://www.qualitysoftwarecompany.com\">Quality Software Company</a> ";
        // line 19
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, "now", "Y"), "html", null, true);
        echo "</strong>
</footer>

<!-- Control Sidebar -->
<aside class=\"control-sidebar control-sidebar-dark\">
    <!-- Create the tabs -->
    <ul class=\"nav nav-tabs nav-justified control-sidebar-tabs\">
        <li class=\"active\"><a href=\"#control-sidebar-home-tab\" data-toggle=\"tab\"><i class=\"fa fa-home\"></i></a></li>
        <li><a href=\"#control-sidebar-settings-tab\" data-toggle=\"tab\"><i class=\"fa fa-gears\"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class=\"tab-content\">
        <!-- Home tab content -->
        <div class=\"tab-pane active\" id=\"control-sidebar-home-tab\">
            <h3 class=\"control-sidebar-heading\">Application info</h3>
            <ul class=\"control-sidebar-menu\">
                <li><a href=\"#\">PHP version: ";
        // line 35
        echo twig_escape_filter($this->env, twig_constant("PHP_VERSION"), "html", null, true);
        echo "</a></li>
                <li><a href=\"#\">CI version: ";
        // line 36
        echo twig_escape_filter($this->env, twig_constant("CI_VERSION"), "html", null, true);
        echo "</a></li>
            </ul>
            <!-- /.control-sidebar-menu -->

        </div>
        <!-- /.tab-pane -->
        <!-- Stats tab content -->
        <div class=\"tab-pane\" id=\"control-sidebar-stats-tab\">Commit log</div>
        <!-- /.tab-pane -->
        <!-- Settings tab content -->
        <div class=\"tab-pane\" id=\"control-sidebar-settings-tab\">

            <h3 class=\"control-sidebar-heading\">Git commit logs</h3>

            <div class=\"form-group\">
                <ul>
                    ";
        // line 52
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["version_info"] ?? null), "log", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["entry"]) {
            // line 53
            echo "                        <li> ";
            echo twig_escape_filter($this->env, ellipsize($context["entry"], 100), "html", null, true);
            echo "</li>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entry'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 55
        echo "                </ul>
            </div>
        </div>
        <!-- /.tab-pane -->
    </div>
</aside>
<!-- /.control-sidebar -->
<!-- Add the sidebar's background. This div must be placed
     immediately after the control sidebar -->
<div class=\"control-sidebar-bg\"></div>
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 2.2.3 -->
<script type=\"text/javascript\" src=\"";
        // line 71
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "js/jQuery/jquery-2.2.3.min.js\"></script>
<!-- Bootstrap 3.3.6 -->
<script type=\"text/javascript\" src=\"";
        // line 73
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "js/bootstrap.min.js\"></script>
<!-- AdminLTE App -->
<script type=\"text/javascript\" src=\"";
        // line 75
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "js/app.min.js\"></script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. Slimscroll is required when using the
     fixed layout. -->

";
        // line 82
        if (($context["form_validator"] ?? null)) {
            // line 83
            echo "    <!-- Form validator -->
    <script src=\"js/form-validator/jquery.form-validator.min.js\"></script>
    <script src=\"js/form-validator/lang/nl.js\"></script>
    <script type=\"text/javascript\">
        \$.validate({validateOnBlur: true,
                scrollToTopOnError: false,
                language: 'nl'});
    </script>
";
        }
        // line 92
        echo "
";
        // line 93
        if (($context["data_table"] ?? null)) {
            // line 94
            echo "    <!-- datatables -->
    <link href=\"js/datatables/jquery.dataTables.min.css\" rel=\"stylesheet\">
    <script type=\"text/javascript\" src=\"js/datatables/jquery.dataTables.min.js\"></script>
    <script type=\"text/javascript\" src=\"js/datatables/dataTables.bootstrap.min.js\"></script>

    <!-- Yadcf extension -->
    <link href=\"js/datatables/extensions/Yadcf/css/jquery.dataTables.yadcf.css\" rel=\"stylesheet\">
    <script type=\"text/javascript\" src=\"js/datatables/extensions/Yadcf/js/jquery.dataTables.yadcf.js\"></script>

    <!-- Responsive extension -->
    <link href=\"js/datatables/extensions/Responsive/css/dataTables.responsive.css\" rel=\"stylesheet\">
    <script type=\"text/javascript\" src=\"js/datatables/extensions/Responsive/js/dataTables.responsive.min.js\"></script>

    <!-- Select extension -->
    <link href=\"js/datatables/extensions/Select/css/dataTables.select.min.css\" rel=\"stylesheet\">
    <script type=\"text/javascript\" src=\"js/datatables/extensions/Select/js/dataTables.select.min.js\"></script>

    <!-- Buttons extension -->
    <script type=\"text/javascript\" src=\"js/datatables/extensions/Buttons/js/dataTables.buttons.min.js\"></script>
    <script type=\"text/javascript\">
    // CRSF protection
    \$(function (\$) {
        \$.ajaxSetup({
            data: {
                '";
            // line 118
            echo twig_escape_filter($this->env, ($context["csrf_token_name"] ?? null), "html", null, true);
            echo "' : '";
            echo twig_escape_filter($this->env, ($context["csrf_hash"] ?? null), "html", null, true);
            echo "',
                ";
            // line 119
            echo ((($context["var1"] ?? null)) ? ((("'var1': '" . ($context["var1"] ?? null)) . "'")) : (""));
            echo "
            }
        });
    });    
    \$.fn.dataTable.ext.buttons.default = {
        className: 'btn btn-sm btn-default'
    };
    \$.fn.dataTable.ext.buttons.primary = {
        className: 'btn btn-sm btn-primary'
    };
    \$.fn.dataTable.ext.buttons.success = {
        className: 'btn btn-sm btn-success'
    };
    \$.fn.dataTable.ext.buttons.info = {
        className: 'btn btn-sm btn-info'
    };
    \$.fn.dataTable.ext.buttons.warning = {
        className: 'btn btn-sm btn-warning'
    };
    \$.fn.dataTable.ext.buttons.danger = {
        className: 'btn btn-sm btn-danger'
    };
    \$(document).ready(function () {
        \$.fn.dataTable.ext.errMode = 'throw';
        var table = \$('#dg').DataTable({
        select: true,
            processing: true,
            serverSide: true,
            stateSave: true,
            pageLength: ";
            // line 148
            echo twig_escape_filter($this->env, ((twig_get_attribute($this->env, $this->source, ($context["session"] ?? null), "table_rows", array())) ? (twig_get_attribute($this->env, $this->source, ($context["session"] ?? null), "table_rows", array())) : (25)), "html", null, true);
            echo ",
            ajax: {
                url: '";
            // line 150
            echo twig_escape_filter($this->env, ($context["ajax_url"] ?? null), "html", null, true);
            echo "',
                dataType: 'json',
                dataSrc: 'data',
                type: 'POST'
            },
            rowId: 'id',
            language: {
                url: '";
            // line 157
            echo twig_escape_filter($this->env, base_url("js/datatables/i18n/nl"), "html", null, true);
            echo ".json'
            },
            dom: '<\"top\"Bi>rt<\"bottom\"lp><\"clear\">',
            buttons: [
                ";
            // line 161
            if (($context["url_back"] ?? null)) {
                // line 162
                echo "                {
                    extend: 'default',
                    text: '";
                // line 164
                echo lang("back_to_overview");
                echo " <span class=\"glyphicon glyphicon-arrow-left\"></span>',
                    action: function () {
                        window.location.href = '";
                // line 166
                echo twig_escape_filter($this->env, ($context["url_back"] ?? null), "html", null, true);
                echo "'
                    }
                },
                ";
            }
            // line 170
            echo "                ";
            if (($context["url_view"] ?? null)) {
                // line 171
                echo "                {
                    extend: 'default',
                    text: '";
                // line 173
                echo lang("form_view_generic");
                echo " <span class=\"glyphicon glyphicon-eye-open\"></span>',
                    action: function () {
                        var row = table.rows('.selected').data();
                        if (row[0]) {
                            var id = row[0][0];
                            window.location.href = '";
                // line 178
                echo twig_escape_filter($this->env, ($context["url_view"] ?? null), "html", null, true);
                echo "/' + id + '/view'
                        } else {
                            \$(\"div.toolbar\").html('<div class=\"alert-warning\" role=\"alert\">";
                // line 180
                echo lang("no_row_selected");
                echo " <span class=\"glyphicon glyphicon-exclamation-sign\"></span></div>');
                        }
                    }
                },
                ";
            }
            // line 185
            echo "                ";
            if (($context["url_add"] ?? null)) {
                // line 186
                echo "                {
                    extend: 'success',
                    text: '";
                // line 188
                echo lang("form_add_generic");
                echo " <span class=\"glyphicon glyphicon-plus\"></span>',
                    action: function () {
                        window.location.href = '";
                // line 190
                echo twig_escape_filter($this->env, ($context["url_add"] ?? null), "html", null, true);
                echo "'
                    }
                },                
                ";
            }
            // line 194
            echo "                ";
            if (($context["url_edit"] ?? null)) {
                // line 195
                echo "                {
                    extend: 'warning',
                    text: '";
                // line 197
                echo lang("form_edit_generic");
                echo " <span class=\"glyphicon glyphicon-pencil\"></span>',
                    action: function () {
                        var row = table.rows('.selected').data();
                        if (row[0]) {
                            var id = row[0][0];
                            window.location.href = '";
                // line 202
                echo twig_escape_filter($this->env, ($context["url_edit"] ?? null), "html", null, true);
                echo "/' + id
                        } else {
                            \$(\"div.toolbar\").html('<div class=\"alert-warning\" role=\"alert\">";
                // line 204
                echo lang("no_row_selected");
                echo " <span class=\"glyphicon glyphicon-exclamation-sign\"></span></div>');
                        }
                    }
                },
                ";
            }
            // line 209
            echo "                ";
            if (($context["url_hard_delete"] ?? null)) {
                // line 210
                echo "                {
                    extend: 'danger',
                    text: '";
                // line 212
                echo lang("form_delete_generic");
                echo " <span class=\"glyphicon glyphicon-trash\"></span>',
                    action: function () {
                        var row = table.rows('.selected').data();
                        if (row[0]) {
                            var id = row[0][0];
                            window.location.href = '";
                // line 217
                echo twig_escape_filter($this->env, ($context["url_hard_delete"] ?? null), "html", null, true);
                echo "/' + id
                        } else {
                            \$(\"div.toolbar\").html('<div class=\"alert-warning\" role=\"alert\">";
                // line 219
                echo lang("no_row_selected");
                echo " <span class=\"glyphicon glyphicon-exclamation-sign\"></span></div>');
                        }
                    }
                },
                ";
            }
            // line 224
            echo "                ";
            if (($context["url_delete"] ?? null)) {
                // line 225
                echo "                {
                    extend: 'danger',
                    text: '";
                // line 227
                echo lang("form_delete_generic");
                echo " <span class=\"glyphicon glyphicon-trash\"></span>',
                    action: function () {
                        var row = table.rows('.selected').data();
                        var successmsg = '<div class=\"alert-info\" role=\"alert\">";
                // line 230
                echo lang("deleted");
                echo " <span class=\"glyphicon glyphicon-check\"></span></div>';
                        var errormsg = '<div class=\"alert-warning\" role=\"alert\">{error} <span class=\"glyphicon glyphicon-exclamation-sign\"></span></div>';
                        if (row[0]) {
                            var id = row[0][0];
                            \$.ajax({
                            url: '";
                // line 235
                echo twig_escape_filter($this->env, ($context["url_delete"] ?? null), "html", null, true);
                echo "',
                                method: 'POST',
                                data: { 'id': id },
                                dataType: 'json',
                                success: function(data, status, xhr) {
                                    if (data.success === true) {
                                        table.rows(\$('#dg tr.active')).remove().draw(false);
                                        \$(\"div.toolbar\").html(successmsg);
                                    } else {
                                        \$(\"div.toolbar\").html(errormsg.replace('{error}', '";
                // line 244
                echo lang("not_successfully_deleted");
                echo "'));
                                    }
                                },
                                error: function(xhr, status, error) {
                                    \$(\"div.toolbar\").html(errormsg.replace('{error}', '";
                // line 248
                echo lang("not_successfully_deleted");
                echo "'));
                                }
                            });
                        } else {
                            \$(\"div.toolbar\").html(errormsg.replace('{error}', '";
                // line 252
                echo lang("no_row_selected");
                echo "'));
                        }
                    }
                },
                ";
            }
            // line 257
            echo "                ";
            if (($context["url_undelete"] ?? null)) {
                // line 258
                echo "                {
                    extend: 'primary',
                    text: '";
                // line 260
                echo lang("form_undelete_generic");
                echo " <span class=\"glyphicon glyphicon-trash\"></span>',
                    action: function () {
                        var row = table.rows('.selected').data();
                        var successmsg = '<div class=\"alert-info\" role=\"alert\">";
                // line 263
                echo lang("successfully_restored");
                echo " <span class=\"glyphicon glyphicon-check\"></span></div>';
                        var errormsg = '<div class=\"alert-warning\" role=\"alert\">{error} <span class=\"glyphicon glyphicon-exclamation-sign\"></span></div>';
                        if (row[0]) {
                            var id = row[0][0];
                            \$.ajax({
                            url: '";
                // line 268
                echo twig_escape_filter($this->env, ($context["url_undelete"] ?? null), "html", null, true);
                echo "',
                            method: 'POST',
                            data: { 'id': id },
                            dataType: 'json',
                            success: function(data, status, xhr) {
                                if (data.success === true) {
                                    table.rows(\$('#dg tr.active')).remove().draw(false);
                                    \$(\"div.toolbar\").html(successmsg);
                                } else {
                                    \$(\"div.toolbar\").html(errormsg.replace('{error}', '";
                // line 277
                echo lang("not_successfully_restored");
                echo "'));
                                }
                            },
                                error: function(xhr, status, error) {
                                    \$(\"div.toolbar\").html(errormsg.replace('{error}', '";
                // line 281
                echo lang("not_successfully_restored");
                echo "'));
                                }
                            });
                        } else {
                            \$(\"div.toolbar\").html(errormsg.replace('{error}', '";
                // line 285
                echo lang("no_row_selected");
                echo "'));
                        }
                    }
                },
                ";
            }
            // line 290
            echo "                ";
            if (($context["url_hard_delete"] ?? null)) {
                // line 291
                echo "                {
                    extend: 'danger',
                    text: '";
                // line 293
                echo lang("form_hard_delete");
                echo " <span class=\"glyphicon glyphicon-trash\"></span>',
                    action: function () {
                        var row = table.rows('.selected').data();
                        if (row[0]) {
                            var id = row[0][0];
                            window.location.href = '";
                // line 298
                echo twig_escape_filter($this->env, ($context["url_hard_delete"] ?? null), "html", null, true);
                echo "/' + id
                        } else {
                            \$(\"div.toolbar\").html('<div class=\"alert-warning\" role=\"alert\">";
                // line 300
                echo lang("no_row_selected");
                echo " <span class=\"glyphicon glyphicon-exclamation-sign\"></span></div>');
                        }
                    }
                },               
                ";
            }
            // line 305
            echo "                ";
            if (($context["url_deleted"] ?? null)) {
                // line 306
                echo "                {
                    extend: 'default',
                    text: '";
                // line 308
                echo lang("form_show_deleted");
                echo " <span class=\"glyphicon glyphicon-trash\"></span>',
                    action: function () {
                        window.location.href = '";
                // line 310
                echo twig_escape_filter($this->env, ($context["url_deleted"] ?? null), "html", null, true);
                echo "'
                    }
                }
                ";
            }
            // line 314
            echo "            ],
            ";
            // line 315
            if (($context["columns"] ?? null)) {
                // line 316
                echo "            \"columns\" : [
                ";
                // line 317
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["columns"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["col"]) {
                    // line 318
                    echo "                        {\"name\":\"";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["col"], "name", array()), "html", null, true);
                    echo "\", \"orderable\": ";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["col"], "orderable", array()), "html", null, true);
                    echo " },
                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['col'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 320
                echo "            ]
            ";
            }
            // line 322
            echo "        });
    ";
            // line 323
            if (($context["url_dblclick"] ?? null)) {
                // line 324
                echo "        \$('#dg tbody').mousedown(function(e) {
            e.preventDefault();
        });
        \$('#dg tbody').on('dblclick', 'tr', function (e) {
            var id = table.row(this).data()[0];
            var base = ' ";
                // line 329
                echo twig_escape_filter($this->env, ($context["url_dblclick"] ?? null), "html", null, true);
                echo "';
            var url = base.replace('__id__', id);
            window.location.href = url;
        });
    ";
            }
            // line 334
            echo "    \$('#dg tbody').on('click', 'tr', function () {
        if (\$(this).hasClass('selected')) {
            \$(this).removeClass('selected');
        } else {
            table.\$('tr.selected').removeClass('selected');
            \$(this).addClass('selected');
        }
    });
    \$('#button').click(function () {
        table.row('.selected').remove().draw(false);
    });
    ";
            // line 345
            if (($context["columns"] ?? null)) {
                // line 346
                echo "        yadcf.init(table, [
            ";
                // line 347
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["columns"] ?? null));
                $context['loop'] = array(
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                );
                if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                    $length = count($context['_seq']);
                    $context['loop']['revindex0'] = $length - 1;
                    $context['loop']['revindex'] = $length;
                    $context['loop']['length'] = $length;
                    $context['loop']['last'] = 1 === $length;
                }
                foreach ($context['_seq'] as $context["_key"] => $context["col"]) {
                    // line 348
                    echo "                ";
                    if (twig_get_attribute($this->env, $this->source, $context["col"], "filter_type", array())) {
                        // line 349
                        echo "                     {\"column_number\": \"";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["loop"], "index0", array()), "html", null, true);
                        echo "\",  ";
                        echo ((twig_get_attribute($this->env, $this->source, $context["col"], "style_class", array())) ? ((("'style_class': '" . twig_get_attribute($this->env, $this->source, $context["col"], "style_class", array())) . "', ")) : (""));
                        echo " \"filter_type\": \"";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["col"], "filter_type", array()), "html", null, true);
                        echo "\", \"filter_delay\": 500},   
                ";
                    }
                    // line 351
                    echo "            ";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                    if (isset($context['loop']['length'])) {
                        --$context['loop']['revindex0'];
                        --$context['loop']['revindex'];
                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['col'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 352
                echo "        ]);
    ";
            }
            // line 354
            echo "
    });
    </script>
";
        }
        // line 358
        echo "
";
        // line 359
        if (($context["select2"] ?? null)) {
            // line 360
            echo "    <!-- Select2 -->
    <script type=\"text/javascript\" src=\"js/select2/select2.full.js\"></script>
    <script type=\"text/javascript\" src=\"js/select2/i18n/en.js\"></script>
    <link href=\"js/select2/select2.min.css\" rel=\"stylesheet\">
";
        }
        // line 365
        echo "
";
        // line 366
        if (($context["bootstrap_select"] ?? null)) {
            // line 367
            echo "    <!-- Select2 -->
    <script type=\"text/javascript\" src=\"js/bootstrapselect/bootstrap-select.js\"></script>
    <script type=\"text/javascript\" src=\"js/bootstrapselect/i18n/defaults-en_EN.js\"></script>
    <link href=\"css/bootstrap-select.min.css\" rel=\"stylesheet\">
";
        }
        // line 372
        echo "
";
        // line 373
        if (($context["bootstrap_switch"] ?? null)) {
            // line 374
            echo "    <!-- Bootstrap Switch -->
    <script type=\"text/javascript\" src=\"js/bootstrap-switch/bootstrap-switch.js\"></script>
    <link href=\"js/bootstrap-switch/bootstrap-switch.css\" rel=\"stylesheet\">
";
        }
        // line 378
        echo "
";
        // line 379
        if (($context["date_picker"] ?? null)) {
            // line 380
            echo "    <!-- Datepicker -->
    <script src=\"js/datepicker/bootstrap-datepicker.js\"></script>
    <script type=\"text/javascript\" src=\"js/datepicker/locales/bootstrap-datepicker.nl.js\"></script>
    <!-- Datepicker theme -->
    <link href=\"js/datepicker/datepicker3.css\" rel=\"stylesheet\">
";
        }
        // line 386
        if (($context["ckeditor"] ?? null)) {
            // line 387
            echo "    <script src=\"js/ckeditor/ckeditor.js\"></script>
";
        }
        // line 389
        echo "<script type=\"text/javascript\">
\$(document).ready(function() {
";
        // line 391
        if (($context["bootstrap_switch"] ?? null)) {
            // line 392
            echo "    \$(\".bootstrapswitch\").bootstrapSwitch(\"size\", \"mini\");
";
        }
        // line 394
        if (($context["date_picker"] ?? null)) {
            // line 395
            echo "    \$(\".datepicker\").datepicker({
        startView: 2,
        language: \"nl\",
        format: \"dd-mm-yyyy\",
        todayHighlight: true
    });
";
        }
        // line 402
        if (($context["select2"] ?? null)) {
            // line 403
            echo "    \$(\".select2\").select2();
";
        }
        // line 405
        echo "});
";
        // line 406
        if (($context["view"] ?? null)) {
            // line 407
            echo "    \$(\"#form :input\").attr(\"disabled\", true);
";
        }
        // line 409
        echo "    
    ";
        // line 410
        if (($context["ckeditor"] ?? null)) {
            // line 411
            echo "        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["ckeditor"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["element"]) {
                // line 412
                echo "            CKEDITOR.replace( '";
                echo twig_escape_filter($this->env, $context["element"], "html", null, true);
                echo "' , {
                filebrowserBrowseUrl : '";
                // line 413
                echo twig_escape_filter($this->env, base_url(), "html", null, true);
                echo "application/libraries/filemanager/dialog.php?type=2&editor=ckeditor&fldr=', 
                filebrowserUploadUrl : '";
                // line 414
                echo twig_escape_filter($this->env, base_url(), "html", null, true);
                echo "application/libraries/filemanager/dialog.php?type=2&editor=ckeditor&fldr=', 
                filebrowserImageBrowseUrl : '";
                // line 415
                echo twig_escape_filter($this->env, base_url(), "html", null, true);
                echo "application/libraries/filemanager/dialog.php?type=1&editor=ckeditor&fldr='
                ";
                // line 416
                if (($context["element"] == "intro")) {
                    echo ", height : 150 ";
                }
                echo " 
            });
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['element'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 419
            echo "    ";
        }
        // line 420
        if (($context["javascript_extra_functions"] ?? null)) {
            // line 421
            echo "    
    ";
            // line 422
            echo ($context["javascript_extra_functions"] ?? null);
            echo "
    
";
        }
        // line 425
        echo "    
</script>
<script>defer\$()</script>
</body>
</html>
";
    }

    public function getTemplateName()
    {
        return "generic/footer.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  798 => 425,  792 => 422,  789 => 421,  787 => 420,  784 => 419,  773 => 416,  769 => 415,  765 => 414,  761 => 413,  756 => 412,  751 => 411,  749 => 410,  746 => 409,  742 => 407,  740 => 406,  737 => 405,  733 => 403,  731 => 402,  722 => 395,  720 => 394,  716 => 392,  714 => 391,  710 => 389,  706 => 387,  704 => 386,  696 => 380,  694 => 379,  691 => 378,  685 => 374,  683 => 373,  680 => 372,  673 => 367,  671 => 366,  668 => 365,  661 => 360,  659 => 359,  656 => 358,  650 => 354,  646 => 352,  632 => 351,  622 => 349,  619 => 348,  602 => 347,  599 => 346,  597 => 345,  584 => 334,  576 => 329,  569 => 324,  567 => 323,  564 => 322,  560 => 320,  549 => 318,  545 => 317,  542 => 316,  540 => 315,  537 => 314,  530 => 310,  525 => 308,  521 => 306,  518 => 305,  510 => 300,  505 => 298,  497 => 293,  493 => 291,  490 => 290,  482 => 285,  475 => 281,  468 => 277,  456 => 268,  448 => 263,  442 => 260,  438 => 258,  435 => 257,  427 => 252,  420 => 248,  413 => 244,  401 => 235,  393 => 230,  387 => 227,  383 => 225,  380 => 224,  372 => 219,  367 => 217,  359 => 212,  355 => 210,  352 => 209,  344 => 204,  339 => 202,  331 => 197,  327 => 195,  324 => 194,  317 => 190,  312 => 188,  308 => 186,  305 => 185,  297 => 180,  292 => 178,  284 => 173,  280 => 171,  277 => 170,  270 => 166,  265 => 164,  261 => 162,  259 => 161,  252 => 157,  242 => 150,  237 => 148,  205 => 119,  199 => 118,  173 => 94,  171 => 93,  168 => 92,  157 => 83,  155 => 82,  145 => 75,  140 => 73,  135 => 71,  117 => 55,  108 => 53,  104 => 52,  85 => 36,  81 => 35,  62 => 19,  51 => 10,  41 => 7,  33 => 6,  30 => 5,  28 => 4,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("</section>
<!-- /.content -->

{% if footer_menu %}
    <ul class=\"pager\">
        <li {{ footer_menu.prev_id ? '' : 'class=\"disabled\"' }}><a href=\"{{ footer_menu.prev_id ? \"#{footer_menu.url}/#{footer_menu.prev_id}\" : 'javascript: void(0)' }}\"><span class=\"glyphicon glyphicon-chevron-left\"></span> {{ lang('form_previous') }}</a></li>
        <li {{ footer_menu.next_id ? '' : 'class=\"disabled\"' }}><a href=\"{{ footer_menu.next_id ? \"#{footer_menu.url}/#{footer_menu.next_id}\" : 'javascript: void(0)' }}\">{{ lang('form_next') }} <span class=\"glyphicon glyphicon-chevron-right\"></span></a></li>
    </ul>
{% endif %}

</div>
<!-- /.content-wrapper -->

<!-- Main Footer -->
<footer class=\"main-footer\">
    <!-- To the right -->
    <div class=\"pull-right hidden-xs\"></div>
    <!-- Default to the left -->
    <strong>Copyright &copy; <a href=\"https://www.qualitysoftwarecompany.com\">Quality Software Company</a> {{ \"now\"|date(\"Y\") }}</strong>
</footer>

<!-- Control Sidebar -->
<aside class=\"control-sidebar control-sidebar-dark\">
    <!-- Create the tabs -->
    <ul class=\"nav nav-tabs nav-justified control-sidebar-tabs\">
        <li class=\"active\"><a href=\"#control-sidebar-home-tab\" data-toggle=\"tab\"><i class=\"fa fa-home\"></i></a></li>
        <li><a href=\"#control-sidebar-settings-tab\" data-toggle=\"tab\"><i class=\"fa fa-gears\"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class=\"tab-content\">
        <!-- Home tab content -->
        <div class=\"tab-pane active\" id=\"control-sidebar-home-tab\">
            <h3 class=\"control-sidebar-heading\">Application info</h3>
            <ul class=\"control-sidebar-menu\">
                <li><a href=\"#\">PHP version: {{ constant('PHP_VERSION') }}</a></li>
                <li><a href=\"#\">CI version: {{ constant('CI_VERSION') }}</a></li>
            </ul>
            <!-- /.control-sidebar-menu -->

        </div>
        <!-- /.tab-pane -->
        <!-- Stats tab content -->
        <div class=\"tab-pane\" id=\"control-sidebar-stats-tab\">Commit log</div>
        <!-- /.tab-pane -->
        <!-- Settings tab content -->
        <div class=\"tab-pane\" id=\"control-sidebar-settings-tab\">

            <h3 class=\"control-sidebar-heading\">Git commit logs</h3>

            <div class=\"form-group\">
                <ul>
                    {% for entry in version_info.log %}
                        <li> {{ ellipsize(entry, 100) }}</li>
                    {% endfor %}
                </ul>
            </div>
        </div>
        <!-- /.tab-pane -->
    </div>
</aside>
<!-- /.control-sidebar -->
<!-- Add the sidebar's background. This div must be placed
     immediately after the control sidebar -->
<div class=\"control-sidebar-bg\"></div>
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 2.2.3 -->
<script type=\"text/javascript\" src=\"{{ base_url() }}js/jQuery/jquery-2.2.3.min.js\"></script>
<!-- Bootstrap 3.3.6 -->
<script type=\"text/javascript\" src=\"{{ base_url() }}js/bootstrap.min.js\"></script>
<!-- AdminLTE App -->
<script type=\"text/javascript\" src=\"{{ base_url() }}js/app.min.js\"></script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. Slimscroll is required when using the
     fixed layout. -->

{% if form_validator %}
    <!-- Form validator -->
    <script src=\"js/form-validator/jquery.form-validator.min.js\"></script>
    <script src=\"js/form-validator/lang/nl.js\"></script>
    <script type=\"text/javascript\">
        \$.validate({validateOnBlur: true,
                scrollToTopOnError: false,
                language: 'nl'});
    </script>
{% endif %}

{% if data_table %}
    <!-- datatables -->
    <link href=\"js/datatables/jquery.dataTables.min.css\" rel=\"stylesheet\">
    <script type=\"text/javascript\" src=\"js/datatables/jquery.dataTables.min.js\"></script>
    <script type=\"text/javascript\" src=\"js/datatables/dataTables.bootstrap.min.js\"></script>

    <!-- Yadcf extension -->
    <link href=\"js/datatables/extensions/Yadcf/css/jquery.dataTables.yadcf.css\" rel=\"stylesheet\">
    <script type=\"text/javascript\" src=\"js/datatables/extensions/Yadcf/js/jquery.dataTables.yadcf.js\"></script>

    <!-- Responsive extension -->
    <link href=\"js/datatables/extensions/Responsive/css/dataTables.responsive.css\" rel=\"stylesheet\">
    <script type=\"text/javascript\" src=\"js/datatables/extensions/Responsive/js/dataTables.responsive.min.js\"></script>

    <!-- Select extension -->
    <link href=\"js/datatables/extensions/Select/css/dataTables.select.min.css\" rel=\"stylesheet\">
    <script type=\"text/javascript\" src=\"js/datatables/extensions/Select/js/dataTables.select.min.js\"></script>

    <!-- Buttons extension -->
    <script type=\"text/javascript\" src=\"js/datatables/extensions/Buttons/js/dataTables.buttons.min.js\"></script>
    <script type=\"text/javascript\">
    // CRSF protection
    \$(function (\$) {
        \$.ajaxSetup({
            data: {
                '{{ csrf_token_name }}' : '{{ csrf_hash }}',
                {{ var1 ? \"'var1': '#{var1}'\"|raw }}
            }
        });
    });    
    \$.fn.dataTable.ext.buttons.default = {
        className: 'btn btn-sm btn-default'
    };
    \$.fn.dataTable.ext.buttons.primary = {
        className: 'btn btn-sm btn-primary'
    };
    \$.fn.dataTable.ext.buttons.success = {
        className: 'btn btn-sm btn-success'
    };
    \$.fn.dataTable.ext.buttons.info = {
        className: 'btn btn-sm btn-info'
    };
    \$.fn.dataTable.ext.buttons.warning = {
        className: 'btn btn-sm btn-warning'
    };
    \$.fn.dataTable.ext.buttons.danger = {
        className: 'btn btn-sm btn-danger'
    };
    \$(document).ready(function () {
        \$.fn.dataTable.ext.errMode = 'throw';
        var table = \$('#dg').DataTable({
        select: true,
            processing: true,
            serverSide: true,
            stateSave: true,
            pageLength: {{ session.table_rows ?: 25 }},
            ajax: {
                url: '{{ ajax_url }}',
                dataType: 'json',
                dataSrc: 'data',
                type: 'POST'
            },
            rowId: 'id',
            language: {
                url: '{{  base_url('js/datatables/i18n/nl')}}.json'
            },
            dom: '<\"top\"Bi>rt<\"bottom\"lp><\"clear\">',
            buttons: [
                {% if url_back %}
                {
                    extend: 'default',
                    text: '{{ lang('back_to_overview') }} <span class=\"glyphicon glyphicon-arrow-left\"></span>',
                    action: function () {
                        window.location.href = '{{ url_back }}'
                    }
                },
                {% endif %}
                {% if url_view %}
                {
                    extend: 'default',
                    text: '{{ lang('form_view_generic') }} <span class=\"glyphicon glyphicon-eye-open\"></span>',
                    action: function () {
                        var row = table.rows('.selected').data();
                        if (row[0]) {
                            var id = row[0][0];
                            window.location.href = '{{ url_view }}/' + id + '/view'
                        } else {
                            \$(\"div.toolbar\").html('<div class=\"alert-warning\" role=\"alert\">{{ lang('no_row_selected') }} <span class=\"glyphicon glyphicon-exclamation-sign\"></span></div>');
                        }
                    }
                },
                {% endif %}
                {% if url_add %}
                {
                    extend: 'success',
                    text: '{{ lang('form_add_generic') }} <span class=\"glyphicon glyphicon-plus\"></span>',
                    action: function () {
                        window.location.href = '{{ url_add }}'
                    }
                },                
                {% endif %}
                {% if url_edit %}
                {
                    extend: 'warning',
                    text: '{{ lang('form_edit_generic') }} <span class=\"glyphicon glyphicon-pencil\"></span>',
                    action: function () {
                        var row = table.rows('.selected').data();
                        if (row[0]) {
                            var id = row[0][0];
                            window.location.href = '{{ url_edit }}/' + id
                        } else {
                            \$(\"div.toolbar\").html('<div class=\"alert-warning\" role=\"alert\">{{ lang('no_row_selected') }} <span class=\"glyphicon glyphicon-exclamation-sign\"></span></div>');
                        }
                    }
                },
                {% endif %}
                {% if url_hard_delete %}
                {
                    extend: 'danger',
                    text: '{{ lang('form_delete_generic') }} <span class=\"glyphicon glyphicon-trash\"></span>',
                    action: function () {
                        var row = table.rows('.selected').data();
                        if (row[0]) {
                            var id = row[0][0];
                            window.location.href = '{{ url_hard_delete }}/' + id
                        } else {
                            \$(\"div.toolbar\").html('<div class=\"alert-warning\" role=\"alert\">{{ lang('no_row_selected') }} <span class=\"glyphicon glyphicon-exclamation-sign\"></span></div>');
                        }
                    }
                },
                {% endif %}
                {% if url_delete %}
                {
                    extend: 'danger',
                    text: '{{ lang('form_delete_generic') }} <span class=\"glyphicon glyphicon-trash\"></span>',
                    action: function () {
                        var row = table.rows('.selected').data();
                        var successmsg = '<div class=\"alert-info\" role=\"alert\">{{ lang('deleted') }} <span class=\"glyphicon glyphicon-check\"></span></div>';
                        var errormsg = '<div class=\"alert-warning\" role=\"alert\">{error} <span class=\"glyphicon glyphicon-exclamation-sign\"></span></div>';
                        if (row[0]) {
                            var id = row[0][0];
                            \$.ajax({
                            url: '{{ url_delete }}',
                                method: 'POST',
                                data: { 'id': id },
                                dataType: 'json',
                                success: function(data, status, xhr) {
                                    if (data.success === true) {
                                        table.rows(\$('#dg tr.active')).remove().draw(false);
                                        \$(\"div.toolbar\").html(successmsg);
                                    } else {
                                        \$(\"div.toolbar\").html(errormsg.replace('{error}', '{{ lang('not_successfully_deleted') }}'));
                                    }
                                },
                                error: function(xhr, status, error) {
                                    \$(\"div.toolbar\").html(errormsg.replace('{error}', '{{ lang('not_successfully_deleted') }}'));
                                }
                            });
                        } else {
                            \$(\"div.toolbar\").html(errormsg.replace('{error}', '{{ lang('no_row_selected') }}'));
                        }
                    }
                },
                {% endif %}
                {% if url_undelete %}
                {
                    extend: 'primary',
                    text: '{{ lang('form_undelete_generic') }} <span class=\"glyphicon glyphicon-trash\"></span>',
                    action: function () {
                        var row = table.rows('.selected').data();
                        var successmsg = '<div class=\"alert-info\" role=\"alert\">{{ lang('successfully_restored') }} <span class=\"glyphicon glyphicon-check\"></span></div>';
                        var errormsg = '<div class=\"alert-warning\" role=\"alert\">{error} <span class=\"glyphicon glyphicon-exclamation-sign\"></span></div>';
                        if (row[0]) {
                            var id = row[0][0];
                            \$.ajax({
                            url: '{{ url_undelete }}',
                            method: 'POST',
                            data: { 'id': id },
                            dataType: 'json',
                            success: function(data, status, xhr) {
                                if (data.success === true) {
                                    table.rows(\$('#dg tr.active')).remove().draw(false);
                                    \$(\"div.toolbar\").html(successmsg);
                                } else {
                                    \$(\"div.toolbar\").html(errormsg.replace('{error}', '{{ lang('not_successfully_restored') }}'));
                                }
                            },
                                error: function(xhr, status, error) {
                                    \$(\"div.toolbar\").html(errormsg.replace('{error}', '{{ lang('not_successfully_restored') }}'));
                                }
                            });
                        } else {
                            \$(\"div.toolbar\").html(errormsg.replace('{error}', '{{ lang('no_row_selected') }}'));
                        }
                    }
                },
                {% endif %}
                {% if url_hard_delete %}
                {
                    extend: 'danger',
                    text: '{{ lang('form_hard_delete') }} <span class=\"glyphicon glyphicon-trash\"></span>',
                    action: function () {
                        var row = table.rows('.selected').data();
                        if (row[0]) {
                            var id = row[0][0];
                            window.location.href = '{{ url_hard_delete }}/' + id
                        } else {
                            \$(\"div.toolbar\").html('<div class=\"alert-warning\" role=\"alert\">{{ lang('no_row_selected') }} <span class=\"glyphicon glyphicon-exclamation-sign\"></span></div>');
                        }
                    }
                },               
                {% endif %}
                {% if url_deleted %}
                {
                    extend: 'default',
                    text: '{{ lang('form_show_deleted') }} <span class=\"glyphicon glyphicon-trash\"></span>',
                    action: function () {
                        window.location.href = '{{ url_deleted }}'
                    }
                }
                {% endif %}
            ],
            {% if columns %}
            \"columns\" : [
                {% for col in columns %}
                        {\"name\":\"{{ col.name }}\", \"orderable\": {{ col.orderable }} },
                {% endfor %}
            ]
            {% endif %}
        });
    {% if url_dblclick %}
        \$('#dg tbody').mousedown(function(e) {
            e.preventDefault();
        });
        \$('#dg tbody').on('dblclick', 'tr', function (e) {
            var id = table.row(this).data()[0];
            var base = ' {{ url_dblclick }}';
            var url = base.replace('__id__', id);
            window.location.href = url;
        });
    {% endif %}
    \$('#dg tbody').on('click', 'tr', function () {
        if (\$(this).hasClass('selected')) {
            \$(this).removeClass('selected');
        } else {
            table.\$('tr.selected').removeClass('selected');
            \$(this).addClass('selected');
        }
    });
    \$('#button').click(function () {
        table.row('.selected').remove().draw(false);
    });
    {% if columns %}
        yadcf.init(table, [
            {% for col in columns %}
                {% if col.filter_type %}
                     {\"column_number\": \"{{ loop.index0 }}\",  {{ (col.style_class ? \"'style_class': '#{col.style_class}', \")|raw }} \"filter_type\": \"{{ col.filter_type }}\", \"filter_delay\": 500},   
                {% endif %}
            {% endfor %}
        ]);
    {% endif %}

    });
    </script>
{% endif %}

{% if select2 %}
    <!-- Select2 -->
    <script type=\"text/javascript\" src=\"js/select2/select2.full.js\"></script>
    <script type=\"text/javascript\" src=\"js/select2/i18n/en.js\"></script>
    <link href=\"js/select2/select2.min.css\" rel=\"stylesheet\">
{% endif %}

{% if bootstrap_select %}
    <!-- Select2 -->
    <script type=\"text/javascript\" src=\"js/bootstrapselect/bootstrap-select.js\"></script>
    <script type=\"text/javascript\" src=\"js/bootstrapselect/i18n/defaults-en_EN.js\"></script>
    <link href=\"css/bootstrap-select.min.css\" rel=\"stylesheet\">
{% endif %}

{% if bootstrap_switch %}
    <!-- Bootstrap Switch -->
    <script type=\"text/javascript\" src=\"js/bootstrap-switch/bootstrap-switch.js\"></script>
    <link href=\"js/bootstrap-switch/bootstrap-switch.css\" rel=\"stylesheet\">
{% endif %}

{% if date_picker %}
    <!-- Datepicker -->
    <script src=\"js/datepicker/bootstrap-datepicker.js\"></script>
    <script type=\"text/javascript\" src=\"js/datepicker/locales/bootstrap-datepicker.nl.js\"></script>
    <!-- Datepicker theme -->
    <link href=\"js/datepicker/datepicker3.css\" rel=\"stylesheet\">
{% endif %}
{% if ckeditor %}
    <script src=\"js/ckeditor/ckeditor.js\"></script>
{% endif %}
<script type=\"text/javascript\">
\$(document).ready(function() {
{% if bootstrap_switch %}
    \$(\".bootstrapswitch\").bootstrapSwitch(\"size\", \"mini\");
{% endif %}
{% if date_picker %}
    \$(\".datepicker\").datepicker({
        startView: 2,
        language: \"nl\",
        format: \"dd-mm-yyyy\",
        todayHighlight: true
    });
{% endif %}
{% if select2 %}
    \$(\".select2\").select2();
{% endif %}
});
{% if view %}
    \$(\"#form :input\").attr(\"disabled\", true);
{% endif %}
    
    {% if ckeditor %}
        {% for element in ckeditor %}
            CKEDITOR.replace( '{{ element }}' , {
                filebrowserBrowseUrl : '{{ base_url() }}application/libraries/filemanager/dialog.php?type=2&editor=ckeditor&fldr=', 
                filebrowserUploadUrl : '{{ base_url() }}application/libraries/filemanager/dialog.php?type=2&editor=ckeditor&fldr=', 
                filebrowserImageBrowseUrl : '{{ base_url() }}application/libraries/filemanager/dialog.php?type=1&editor=ckeditor&fldr='
                {% if element == 'intro' %}, height : 150 {% endif %} 
            });
        {% endfor %}
    {% endif %}
{% if javascript_extra_functions %}
    
    {{ javascript_extra_functions|raw }}
    
{% endif %}
    
</script>
<script>defer\$()</script>
</body>
</html>
", "generic/footer.twig", "C:\\MAMP\\htdocs\\localdev\\rdt\\cms\\application\\views\\generic\\footer.twig");
    }
}
