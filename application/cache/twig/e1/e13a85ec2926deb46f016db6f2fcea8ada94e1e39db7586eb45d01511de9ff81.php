<?php

/* login/loginHeader.twig */
class __TwigTemplate_a7dfb689caaab13dbce6bb260a82836c56b104389845116e4a5f92035b78d9b4 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
    <head>
        <meta charset=\"utf-8\">
        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
        <title>";
        // line 7
        echo twig_escape_filter($this->env, twig_constant("APPLICATION_NAME"), "html", null, true);
        echo " &raquo; ";
        echo lang(("main_menu_" . ($context["heading"] ?? null)));
        echo "</title>
            
        <base href=\"";
        // line 9
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "\">

        <!-- Tell the browser to be responsive to screen width -->
        <meta content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\" name=\"viewport\">
        <!-- Bootstrap 3.3.6 -->
        <link rel=\"stylesheet\" href=\"css/bootstrap.min.css\">
        <!-- Font Awesome -->
        <link rel=\"stylesheet\" href=\"css/font-awesome.min.css\">
        <!-- Ionicons -->
        <link rel=\"stylesheet\" href=\"css/ionicons.min.css\">
        <!-- Theme style -->
        <link rel=\"stylesheet\" href=\"css/AdminLTE.min.css\">
        <!-- AdminLTE Skin -->
        <link rel=\"stylesheet\" href=\"css/skins/skin-blue.min.css\">
        <!-- Custom css -->
        <link rel=\"stylesheet\" href=\"css/bootstrap-custom.css\">

        <!-- favicons -->
        <link rel=\"apple-touch-icon\" sizes=\"57x57\" href=\"";
        // line 27
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "img/icon/apple-icon-57x57.png\">
        <link rel=\"apple-touch-icon\" sizes=\"60x60\" href=\"";
        // line 28
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "img/icon/apple-icon-60x60.png\">
        <link rel=\"apple-touch-icon\" sizes=\"72x72\" href=\"";
        // line 29
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "img/icon/apple-icon-72x72.png\">
        <link rel=\"apple-touch-icon\" sizes=\"76x76\" href=\"";
        // line 30
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "img/icon/apple-icon-76x76.png\">
        <link rel=\"apple-touch-icon\" sizes=\"114x114\" href=\"";
        // line 31
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "img/icon/apple-icon-114x114.png\">
        <link rel=\"apple-touch-icon\" sizes=\"120x120\" href=\"";
        // line 32
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "img/icon/apple-icon-120x120.png\">
        <link rel=\"apple-touch-icon\" sizes=\"144x144\" href=\"";
        // line 33
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "img/icon/apple-icon-144x144.png\">
        <link rel=\"apple-touch-icon\" sizes=\"152x152\" href=\"";
        // line 34
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "img/icon/apple-icon-152x152.png\">
        <link rel=\"apple-touch-icon\" sizes=\"180x180\" href=\"";
        // line 35
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "img/icon/apple-icon-180x180.png\">
        <link rel=\"icon\" type=\"image/png\" sizes=\"192x192\" href=\"";
        // line 36
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "img/icon/android-icon-192x192.png\">
        <link rel=\"icon\" type=\"image/png\" sizes=\"32x32\" href=\"";
        // line 37
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "img/icon/favicon-32x32.png\">
        <link rel=\"icon\" type=\"image/png\" sizes=\"96x96\" href=\"";
        // line 38
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "img/icon/favicon-96x96.png\">
        <link rel=\"icon\" type=\"image/png\" sizes=\"16x16\" href=\"";
        // line 39
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "img/icon/favicon-16x16.png\">
        <link rel=\"manifest\" href=\"";
        // line 40
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "img/icon/manifest.json\">
        <script type=\"text/javascript\" src=\"js/jquery-defer.js\"></script> 
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src=\"https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js\"></script>
          <script src=\"https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js\"></script>
        <![endif]-->
    </head>
    <body class=\"hold-transition login-page\">

        <!-- Main content -->
        <div class=\"login-box\">
            <div class=\"login-logo\">
                <!-- Logo -->
                <a href=\"index.php\" class=\"logo\">
                    <!-- logo for regular state and mobile devices -->
                    <span class=\"logo-lg\">";
        // line 57
        echo twig_escape_filter($this->env, twig_constant("APPLICATION_NAME"), "html", null, true);
        echo "</span>
                </a>
            </div>
            <div class=\"login-box-body\">
                <p class=\"login-box-msg\">";
        // line 61
        echo lang("login_header");
        echo "</p>
                <!-- Your Page Content Here -->";
    }

    public function getTemplateName()
    {
        return "login/loginHeader.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 61,  131 => 57,  111 => 40,  107 => 39,  103 => 38,  99 => 37,  95 => 36,  91 => 35,  87 => 34,  83 => 33,  79 => 32,  75 => 31,  71 => 30,  67 => 29,  63 => 28,  59 => 27,  38 => 9,  31 => 7,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html lang=\"en\">
    <head>
        <meta charset=\"utf-8\">
        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
        <title>{{ constant('APPLICATION_NAME') }} &raquo; {{ lang(\"main_menu_#{heading}\") }}</title>
            
        <base href=\"{{ base_url() }}\">

        <!-- Tell the browser to be responsive to screen width -->
        <meta content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\" name=\"viewport\">
        <!-- Bootstrap 3.3.6 -->
        <link rel=\"stylesheet\" href=\"css/bootstrap.min.css\">
        <!-- Font Awesome -->
        <link rel=\"stylesheet\" href=\"css/font-awesome.min.css\">
        <!-- Ionicons -->
        <link rel=\"stylesheet\" href=\"css/ionicons.min.css\">
        <!-- Theme style -->
        <link rel=\"stylesheet\" href=\"css/AdminLTE.min.css\">
        <!-- AdminLTE Skin -->
        <link rel=\"stylesheet\" href=\"css/skins/skin-blue.min.css\">
        <!-- Custom css -->
        <link rel=\"stylesheet\" href=\"css/bootstrap-custom.css\">

        <!-- favicons -->
        <link rel=\"apple-touch-icon\" sizes=\"57x57\" href=\"{{ base_url() }}img/icon/apple-icon-57x57.png\">
        <link rel=\"apple-touch-icon\" sizes=\"60x60\" href=\"{{ base_url() }}img/icon/apple-icon-60x60.png\">
        <link rel=\"apple-touch-icon\" sizes=\"72x72\" href=\"{{ base_url() }}img/icon/apple-icon-72x72.png\">
        <link rel=\"apple-touch-icon\" sizes=\"76x76\" href=\"{{ base_url() }}img/icon/apple-icon-76x76.png\">
        <link rel=\"apple-touch-icon\" sizes=\"114x114\" href=\"{{ base_url() }}img/icon/apple-icon-114x114.png\">
        <link rel=\"apple-touch-icon\" sizes=\"120x120\" href=\"{{ base_url() }}img/icon/apple-icon-120x120.png\">
        <link rel=\"apple-touch-icon\" sizes=\"144x144\" href=\"{{ base_url() }}img/icon/apple-icon-144x144.png\">
        <link rel=\"apple-touch-icon\" sizes=\"152x152\" href=\"{{ base_url() }}img/icon/apple-icon-152x152.png\">
        <link rel=\"apple-touch-icon\" sizes=\"180x180\" href=\"{{ base_url() }}img/icon/apple-icon-180x180.png\">
        <link rel=\"icon\" type=\"image/png\" sizes=\"192x192\" href=\"{{ base_url() }}img/icon/android-icon-192x192.png\">
        <link rel=\"icon\" type=\"image/png\" sizes=\"32x32\" href=\"{{ base_url() }}img/icon/favicon-32x32.png\">
        <link rel=\"icon\" type=\"image/png\" sizes=\"96x96\" href=\"{{ base_url() }}img/icon/favicon-96x96.png\">
        <link rel=\"icon\" type=\"image/png\" sizes=\"16x16\" href=\"{{ base_url() }}img/icon/favicon-16x16.png\">
        <link rel=\"manifest\" href=\"{{ base_url() }}img/icon/manifest.json\">
        <script type=\"text/javascript\" src=\"js/jquery-defer.js\"></script> 
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src=\"https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js\"></script>
          <script src=\"https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js\"></script>
        <![endif]-->
    </head>
    <body class=\"hold-transition login-page\">

        <!-- Main content -->
        <div class=\"login-box\">
            <div class=\"login-logo\">
                <!-- Logo -->
                <a href=\"index.php\" class=\"logo\">
                    <!-- logo for regular state and mobile devices -->
                    <span class=\"logo-lg\">{{ constant('APPLICATION_NAME') }}</span>
                </a>
            </div>
            <div class=\"login-box-body\">
                <p class=\"login-box-msg\">{{ lang('login_header') }}</p>
                <!-- Your Page Content Here -->", "login/loginHeader.twig", "C:\\MAMP\\htdocs\\localdev\\rdt\\cms\\application\\views\\login\\loginHeader.twig");
    }
}
