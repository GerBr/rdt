<?php

/* dashboardHome.twig */
class __TwigTemplate_ee26883b3d2aed2c2ab02092537c3944022b3c7f8e344eb0c642ab67091234c0 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo ((($context["message"] ?? null)) ? (($context["message"] ?? null)) : (""));
        echo " to be determined";
    }

    public function getTemplateName()
    {
        return "dashboardHome.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ message|raw ?: '' }} to be determined", "dashboardHome.twig", "C:\\MAMP\\htdocs\\localdev\\rdt\\cms\\application\\views\\dashboardHome.twig");
    }
}
