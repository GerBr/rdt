<?php

/* generic/header.twig */
class __TwigTemplate_c87dde766b6085074af23965b5d07eed7de0e461523c9b8e9bd3b751d5a09f1e extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
    <head>
        <meta charset=\"utf-8\">
        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
        <title>";
        // line 7
        echo twig_escape_filter($this->env, twig_constant("APPLICATION_NAME"), "html", null, true);
        echo " &raquo; ";
        echo twig_escape_filter($this->env, ($context["pageName"] ?? null), "html", null, true);
        echo "</title>
            
        <base href=\"";
        // line 9
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "\">

        <!-- Tell the browser to be responsive to screen width -->
        <meta content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\" name=\"viewport\">
        <!-- Bootstrap 3.3.6 -->
        <link rel=\"stylesheet\" href=\"css/bootstrap.min.css\">
        <!-- Font Awesome -->
        <link rel=\"stylesheet\" href=\"css/font-awesome.min.css\">
        <!-- Ionicons -->
        <link rel=\"stylesheet\" href=\"css/ionicons.min.css\">
        <!-- Theme style -->
        <link rel=\"stylesheet\" href=\"css/AdminLTE.min.css\">
        <!-- AdminLTE Skin -->
        <link rel=\"stylesheet\" href=\"css/skins/skin-blue.min.css\">
        <!-- Custom css -->
        <link rel=\"stylesheet\" href=\"css/bootstrap-custom.css\">

        <!-- favicons -->
        <link rel=\"apple-touch-icon\" sizes=\"57x57\" href=\"";
        // line 27
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "img/icon/apple-icon-57x57.png\">
        <link rel=\"apple-touch-icon\" sizes=\"60x60\" href=\"";
        // line 28
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "img/icon/apple-icon-60x60.png\">
        <link rel=\"apple-touch-icon\" sizes=\"72x72\" href=\"";
        // line 29
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "img/icon/apple-icon-72x72.png\">
        <link rel=\"apple-touch-icon\" sizes=\"76x76\" href=\"";
        // line 30
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "img/icon/apple-icon-76x76.png\">
        <link rel=\"apple-touch-icon\" sizes=\"114x114\" href=\"";
        // line 31
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "img/icon/apple-icon-114x114.png\">
        <link rel=\"apple-touch-icon\" sizes=\"120x120\" href=\"";
        // line 32
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "img/icon/apple-icon-120x120.png\">
        <link rel=\"apple-touch-icon\" sizes=\"144x144\" href=\"";
        // line 33
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "img/icon/apple-icon-144x144.png\">
        <link rel=\"apple-touch-icon\" sizes=\"152x152\" href=\"";
        // line 34
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "img/icon/apple-icon-152x152.png\">
        <link rel=\"apple-touch-icon\" sizes=\"180x180\" href=\"";
        // line 35
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "img/icon/apple-icon-180x180.png\">
        <link rel=\"icon\" type=\"image/png\" sizes=\"192x192\" href=\"";
        // line 36
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "img/icon/android-icon-192x192.png\">
        <link rel=\"icon\" type=\"image/png\" sizes=\"32x32\" href=\"";
        // line 37
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "img/icon/favicon-32x32.png\">
        <link rel=\"icon\" type=\"image/png\" sizes=\"96x96\" href=\"";
        // line 38
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "img/icon/favicon-96x96.png\">
        <link rel=\"icon\" type=\"image/png\" sizes=\"16x16\" href=\"";
        // line 39
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "img/icon/favicon-16x16.png\">
        <link rel=\"manifest\" href=\"";
        // line 40
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "img/icon/manifest.json\">
        <script type=\"text/javascript\" src=\"";
        // line 41
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "js/jquery-defer.js\"></script> 
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src=\"https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js\"></script>
          <script src=\"https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js\"></script>
        <![endif]-->
    </head>
    <body class=\"hold-transition skin-blue sidebar-mini\">
        <div class=\"wrapper\">

            <!-- Main Header -->
            <header class=\"main-header\">

                <div class=\"logo\">
                    ";
        // line 56
        if (($context["loggedIn"] ?? null)) {
            // line 57
            echo "                        <p>";
            echo twig_escape_filter($this->env, ($context["userName"] ?? null), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, ($context["roleName"] ?? null), "html", null, true);
            echo "</p>
                    ";
        }
        // line 59
        echo "                </div>
                

                <!-- Header Navbar -->
                <nav class=\"navbar navbar-static-top\" role=\"navigation\">
                    <!-- Sidebar toggle button-->
                    <a href=\"#\" class=\"sidebar-toggle\" data-toggle=\"offcanvas\" role=\"button\">
                        <span class=\"sr-only\">Toggle navigation</span>
                    </a>
                    <h1 class=\"pull-left\">";
        // line 68
        echo twig_escape_filter($this->env, twig_constant("APPLICATION_NAME"), "html", null, true);
        echo "</h1>
                    <a href=\"";
        // line 69
        echo twig_escape_filter($this->env, site_url("cms/login/logout"), "html", null, true);
        echo "\"><h2 class=\"pull-right\"><i class=\"fa fa-sign-out\"></i>";
        echo lang("logout");
        echo "</h2></a>
                </nav>
            </header>
                            
            ";
        // line 73
        if (($context["loggedIn"] ?? null)) {
            // line 74
            echo "                <!-- Left side column. contains the logo and sidebar -->
                <aside class=\"main-sidebar\">

                    <!-- sidebar: style can be found in sidebar.less -->
                    <section class=\"sidebar\">

                        <!-- Sidebar Menu -->
                        <ul class=\"sidebar-menu tree\" data-widget=\"tree\">
                            <li class=\"header\">MENU</li>
                            ";
            // line 83
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["menu"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                // line 84
                echo "                            <li class=\"";
                if (twig_get_attribute($this->env, $this->source, $context["item"], "active", array())) {
                    echo " active";
                }
                if (twig_get_attribute($this->env, $this->source, $context["item"], "subs", array())) {
                    echo " treeview";
                }
                echo "\">
                                ";
                // line 85
                if (twig_get_attribute($this->env, $this->source, $context["item"], "subs", array())) {
                    // line 86
                    echo "                                    <a href=\"#\">
                                ";
                } else {
                    // line 88
                    echo "                                    <a href=\"";
                    echo twig_escape_filter($this->env, site_url(twig_get_attribute($this->env, $this->source, $context["item"], "menu_path", array())), "html", null, true);
                    echo "\">
                                ";
                }
                // line 90
                echo "                                <span>";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "menu_name", array()), "html", null, true);
                echo "</span>
                                ";
                // line 91
                if (twig_get_attribute($this->env, $this->source, $context["item"], "subs", array())) {
                    // line 92
                    echo "                                    <span class=\"pull-right-container\"> <i class=\"fa fa-angle-left pull-right\"></i> 
                                ";
                }
                // line 94
                echo "                                </a>
                                ";
                // line 95
                if (twig_get_attribute($this->env, $this->source, $context["item"], "subs", array())) {
                    // line 96
                    echo "                                    <ul class=\"treeview-menu\">
                                        ";
                    // line 97
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["item"], "subs", array()));
                    foreach ($context['_seq'] as $context["_key"] => $context["sub"]) {
                        // line 98
                        echo "                                            <li><a href=\"";
                        echo twig_escape_filter($this->env, site_url(twig_get_attribute($this->env, $this->source, $context["sub"], "menu_sub_path", array())), "html", null, true);
                        echo "\">";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["sub"], "menu_sub_name", array()), "html", null, true);
                        echo "</a></li>
                                        ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sub'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 100
                    echo "                                    </ul>
                                ";
                }
                // line 102
                echo "
                            </li>
                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 105
            echo "                        </ul>
                        <!-- /.sidebar-menu -->
                    </section>
                    <!-- /.sidebar -->
                </aside>
            ";
        }
        // line 111
        echo "            
            <div class=\"content-wrapper\">
                <!-- Content Header (Page header) -->
                <section class=\"content-header\">
                    <h1>
                        ";
        // line 116
        echo twig_escape_filter($this->env, ($context["pageName"] ?? null), "html", null, true);
        echo "
                    </h1>
                </section>

                <!-- Main content -->
                <section class=\"content\">

                    <!-- Your Page Content Here -->";
    }

    public function getTemplateName()
    {
        return "generic/header.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  267 => 116,  260 => 111,  252 => 105,  244 => 102,  240 => 100,  229 => 98,  225 => 97,  222 => 96,  220 => 95,  217 => 94,  213 => 92,  211 => 91,  206 => 90,  200 => 88,  196 => 86,  194 => 85,  184 => 84,  180 => 83,  169 => 74,  167 => 73,  158 => 69,  154 => 68,  143 => 59,  135 => 57,  133 => 56,  115 => 41,  111 => 40,  107 => 39,  103 => 38,  99 => 37,  95 => 36,  91 => 35,  87 => 34,  83 => 33,  79 => 32,  75 => 31,  71 => 30,  67 => 29,  63 => 28,  59 => 27,  38 => 9,  31 => 7,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html lang=\"en\">
    <head>
        <meta charset=\"utf-8\">
        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
        <title>{{ constant('APPLICATION_NAME') }} &raquo; {{ pageName }}</title>
            
        <base href=\"{{ base_url() }}\">

        <!-- Tell the browser to be responsive to screen width -->
        <meta content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\" name=\"viewport\">
        <!-- Bootstrap 3.3.6 -->
        <link rel=\"stylesheet\" href=\"css/bootstrap.min.css\">
        <!-- Font Awesome -->
        <link rel=\"stylesheet\" href=\"css/font-awesome.min.css\">
        <!-- Ionicons -->
        <link rel=\"stylesheet\" href=\"css/ionicons.min.css\">
        <!-- Theme style -->
        <link rel=\"stylesheet\" href=\"css/AdminLTE.min.css\">
        <!-- AdminLTE Skin -->
        <link rel=\"stylesheet\" href=\"css/skins/skin-blue.min.css\">
        <!-- Custom css -->
        <link rel=\"stylesheet\" href=\"css/bootstrap-custom.css\">

        <!-- favicons -->
        <link rel=\"apple-touch-icon\" sizes=\"57x57\" href=\"{{ base_url() }}img/icon/apple-icon-57x57.png\">
        <link rel=\"apple-touch-icon\" sizes=\"60x60\" href=\"{{ base_url() }}img/icon/apple-icon-60x60.png\">
        <link rel=\"apple-touch-icon\" sizes=\"72x72\" href=\"{{ base_url() }}img/icon/apple-icon-72x72.png\">
        <link rel=\"apple-touch-icon\" sizes=\"76x76\" href=\"{{ base_url() }}img/icon/apple-icon-76x76.png\">
        <link rel=\"apple-touch-icon\" sizes=\"114x114\" href=\"{{ base_url() }}img/icon/apple-icon-114x114.png\">
        <link rel=\"apple-touch-icon\" sizes=\"120x120\" href=\"{{ base_url() }}img/icon/apple-icon-120x120.png\">
        <link rel=\"apple-touch-icon\" sizes=\"144x144\" href=\"{{ base_url() }}img/icon/apple-icon-144x144.png\">
        <link rel=\"apple-touch-icon\" sizes=\"152x152\" href=\"{{ base_url() }}img/icon/apple-icon-152x152.png\">
        <link rel=\"apple-touch-icon\" sizes=\"180x180\" href=\"{{ base_url() }}img/icon/apple-icon-180x180.png\">
        <link rel=\"icon\" type=\"image/png\" sizes=\"192x192\" href=\"{{ base_url() }}img/icon/android-icon-192x192.png\">
        <link rel=\"icon\" type=\"image/png\" sizes=\"32x32\" href=\"{{ base_url() }}img/icon/favicon-32x32.png\">
        <link rel=\"icon\" type=\"image/png\" sizes=\"96x96\" href=\"{{ base_url() }}img/icon/favicon-96x96.png\">
        <link rel=\"icon\" type=\"image/png\" sizes=\"16x16\" href=\"{{ base_url() }}img/icon/favicon-16x16.png\">
        <link rel=\"manifest\" href=\"{{ base_url() }}img/icon/manifest.json\">
        <script type=\"text/javascript\" src=\"{{ base_url() }}js/jquery-defer.js\"></script> 
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src=\"https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js\"></script>
          <script src=\"https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js\"></script>
        <![endif]-->
    </head>
    <body class=\"hold-transition skin-blue sidebar-mini\">
        <div class=\"wrapper\">

            <!-- Main Header -->
            <header class=\"main-header\">

                <div class=\"logo\">
                    {% if loggedIn %}
                        <p>{{ userName }} - {{ roleName }}</p>
                    {% endif %}
                </div>
                

                <!-- Header Navbar -->
                <nav class=\"navbar navbar-static-top\" role=\"navigation\">
                    <!-- Sidebar toggle button-->
                    <a href=\"#\" class=\"sidebar-toggle\" data-toggle=\"offcanvas\" role=\"button\">
                        <span class=\"sr-only\">Toggle navigation</span>
                    </a>
                    <h1 class=\"pull-left\">{{ constant('APPLICATION_NAME') }}</h1>
                    <a href=\"{{ site_url('cms/login/logout') }}\"><h2 class=\"pull-right\"><i class=\"fa fa-sign-out\"></i>{{ lang('logout') }}</h2></a>
                </nav>
            </header>
                            
            {% if loggedIn %}
                <!-- Left side column. contains the logo and sidebar -->
                <aside class=\"main-sidebar\">

                    <!-- sidebar: style can be found in sidebar.less -->
                    <section class=\"sidebar\">

                        <!-- Sidebar Menu -->
                        <ul class=\"sidebar-menu tree\" data-widget=\"tree\">
                            <li class=\"header\">MENU</li>
                            {% for item in menu %}
                            <li class=\"{% if item.active %} active{% endif %}{% if item.subs %} treeview{% endif %}\">
                                {% if item.subs %}
                                    <a href=\"#\">
                                {% else %}
                                    <a href=\"{{ site_url(item.menu_path) }}\">
                                {% endif %}
                                <span>{{item.menu_name }}</span>
                                {% if item.subs %}
                                    <span class=\"pull-right-container\"> <i class=\"fa fa-angle-left pull-right\"></i> 
                                {% endif %}
                                </a>
                                {% if item.subs %}
                                    <ul class=\"treeview-menu\">
                                        {% for sub in item.subs %}
                                            <li><a href=\"{{ site_url(sub.menu_sub_path) }}\">{{ sub.menu_sub_name }}</a></li>
                                        {% endfor %}
                                    </ul>
                                {% endif %}

                            </li>
                            {% endfor %}
                        </ul>
                        <!-- /.sidebar-menu -->
                    </section>
                    <!-- /.sidebar -->
                </aside>
            {% endif %}
            
            <div class=\"content-wrapper\">
                <!-- Content Header (Page header) -->
                <section class=\"content-header\">
                    <h1>
                        {{ pageName }}
                    </h1>
                </section>

                <!-- Main content -->
                <section class=\"content\">

                    <!-- Your Page Content Here -->", "generic/header.twig", "C:\\MAMP\\htdocs\\localdev\\rdt\\cms\\application\\views\\generic\\header.twig");
    }
}
