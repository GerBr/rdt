<?php

/* front/page.twig */
class __TwigTemplate_c725bf3f552122da96fd2938582cac1c70c093c30caef50ab364fd7d21fda164 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"content-container\">
    <h2>";
        // line 2
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "name", array()), "html", null, true);
        echo "</h2>
    <div class=\"content\">
    ";
        // line 4
        echo twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "content", array());
        echo "
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "front/page.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 4,  26 => 2,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"content-container\">
    <h2>{{ page.name }}</h2>
    <div class=\"content\">
    {{ page.content|raw }}
    </div>
</div>", "front/page.twig", "C:\\MAMP\\htdocs\\localdev\\rdt\\cms\\application\\views\\front\\page.twig");
    }
}
