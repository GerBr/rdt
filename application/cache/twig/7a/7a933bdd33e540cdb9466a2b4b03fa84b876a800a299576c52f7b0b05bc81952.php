<?php

/* login/loginForgot.twig */
class __TwigTemplate_000f5ff9d1f3bb7493d7eca5c672194e51fef8d7fbf6af30c61a02518570e16b extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo ((($context["message"] ?? null)) ? (($context["message"] ?? null)) : (""));
        echo " 
";
        // line 2
        echo ((($context["warning"] ?? null)) ? (($context["warning"] ?? null)) : (""));
        echo " 

";
        // line 4
        echo form_open("login/forgotProcess", array("class" => "form", "id" => "form"));
        echo "
<div class=\"form-group\">";
        // line 5
        echo lang("email", "email");
        echo form_input(($context["optionsEmail"] ?? null));
        echo "</div>
<div class=\"form-group\">";
        // line 6
        echo ($context["captchaImage"] ?? null);
        echo "</div>
<div class=\"form-group\">";
        // line 7
        echo lang("login_captcha", "login_captcha");
        echo form_input(($context["optionsPassword"] ?? null));
        echo "</div>
<div class=\"form-group\">
    <a class=\"btn btn-default\" href=\"";
        // line 9
        echo twig_escape_filter($this->env, site_url("login"), "html", null, true);
        echo "\">";
        echo lang("form_cancel_generic");
        echo "</a>
    <span class=\"pull-right\">";
        // line 10
        echo form_submit(array("class" => "btn btn-primary btn-lg", "name" => "submit", "value" => lang("form_submit_generic")));
        echo "</span>
</div>
";
        // line 12
        echo form_close();
    }

    public function getTemplateName()
    {
        return "login/loginForgot.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  62 => 12,  57 => 10,  51 => 9,  45 => 7,  41 => 6,  36 => 5,  32 => 4,  27 => 2,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ message|raw ?: '' }} 
{{ warning|raw ?: '' }} 

{{ form_open('login/forgotProcess', {'class': 'form', 'id': 'form'}) }}
<div class=\"form-group\">{{ lang('email', 'email') }}{{ form_input(optionsEmail) }}</div>
<div class=\"form-group\">{{ captchaImage|raw }}</div>
<div class=\"form-group\">{{ lang('login_captcha', 'login_captcha') }}{{ form_input(optionsPassword) }}</div>
<div class=\"form-group\">
    <a class=\"btn btn-default\" href=\"{{ site_url('login') }}\">{{ lang('form_cancel_generic') }}</a>
    <span class=\"pull-right\">{{ form_submit({'class': 'btn btn-primary btn-lg', 'name': 'submit', 'value': lang('form_submit_generic')}) }}</span>
</div>
{{ form_close() }}", "login/loginForgot.twig", "C:\\MAMP\\htdocs\\localdev\\rdt\\cms\\application\\views\\login\\loginForgot.twig");
    }
}
