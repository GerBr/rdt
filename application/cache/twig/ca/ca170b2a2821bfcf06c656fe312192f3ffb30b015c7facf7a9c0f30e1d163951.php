<?php

/* front/header.twig */
class __TwigTemplate_3e759ed37933963c234c5aad47dd1f8f24ab09bf343710a900569a7453fec9ec extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"nl\">
    <head>
        <meta charset=\"utf-8\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
        <title>Rond de Toren &bull; ";
        // line 6
        echo twig_escape_filter($this->env, ($context["pageTitle"] ?? null), "html", null, true);
        echo "</title>
        <base href=\"";
        // line 7
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "\">
        <!-- Bootstrap -->
        <link href=\"";
        // line 9
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "css/bootstrap-front-min.css\" rel=\"stylesheet\">
        <link href=\"";
        // line 10
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "css/bootstrap-custom-front.css\" rel=\"stylesheet\">
        <script type=\"text/javascript\" src=\"";
        // line 11
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "js/jquery-defer.js\"></script> 
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src=\"https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js\"></script>
        <script src=\"https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js\"></script>
        <![endif]-->
    </head>
    <body id=\"front\">          
        <div id=\"main-container\">
            <header>
                <div class=\"header_image\"><a href=\"";
        // line 22
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "\"><img src=\"";
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "img/style/site_logo.png\" alt=\"Rond te Toren\" title=\"Rond de Toren\"></a></div>
                <div class=\"site_description\">
                    <h1>Rond<span class=\"r270\">de</span>Toren</h1>
                    <h2 class=\"text-success\">Loon op Zand</h2>
                </div>
                <div class=\"years text-success\"> 1967 - ";
        // line 27
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, ($context["now"] ?? null), "Y"), "html", null, true);
        echo "</div>
            </header>
            <div class=\"container-fluid\">                
                <nav class=\"navbar navbar-default\" role=\"navigation\">
                    <div class=\"navbar-header\">
                        <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#navbarCollapse\">
                            <span class=\"sr-only\">Toggle navigation</span>
                            <span class=\"icon-bar\"></span>
                            <span class=\"icon-bar\"></span>
                            <span class=\"icon-bar\"></span>
                      </button>
                    </div><!-- /.navbar-header -->
                    <div id=\"navbarCollapse\" class=\"collapse navbar-collapse\">
                        <ul class=\"nav navbar-nav\">
                            ";
        // line 41
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["menu"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 42
            echo "                                <li class=\"";
            if (twig_get_attribute($this->env, $this->source, $context["item"], "children", array())) {
                echo "dropdown";
            }
            if (twig_get_attribute($this->env, $this->source, $context["item"], "active", array())) {
                echo " active";
            }
            echo "\">
                                    <a href=\"";
            // line 43
            echo twig_escape_filter($this->env, site_url(twig_get_attribute($this->env, $this->source, $context["item"], "url", array())), "html", null, true);
            echo "\" ";
            if (twig_get_attribute($this->env, $this->source, $context["item"], "children", array())) {
                echo " class=\"dropdown-toggle\" data-toggle=\"dropdown\"";
            }
            echo ">
                                        ";
            // line 44
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "name", array()), "html", null, true);
            if (twig_get_attribute($this->env, $this->source, $context["item"], "children", array())) {
                echo " <b class=\"caret\"></b>";
            }
            // line 45
            echo "                                    </a>
                                    ";
            // line 46
            if (twig_get_attribute($this->env, $this->source, $context["item"], "children", array())) {
                // line 47
                echo "                                        <ul class=\"dropdown-menu\">
                                            ";
                // line 48
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["item"], "children", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                    // line 49
                    echo "                                                <li><a href=\"";
                    echo twig_escape_filter($this->env, site_url(twig_get_attribute($this->env, $this->source, $context["child"], "url", array())), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["child"], "name", array()), "html", null, true);
                    echo "</a>
                                            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 51
                echo "                                        </ul>
                                    ";
            }
            // line 53
            echo "                                </li>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 55
        echo "                        </ul>
                    </div>
                </nav>                
";
    }

    public function getTemplateName()
    {
        return "front/header.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  149 => 55,  142 => 53,  138 => 51,  127 => 49,  123 => 48,  120 => 47,  118 => 46,  115 => 45,  110 => 44,  102 => 43,  92 => 42,  88 => 41,  71 => 27,  61 => 22,  47 => 11,  43 => 10,  39 => 9,  34 => 7,  30 => 6,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html lang=\"nl\">
    <head>
        <meta charset=\"utf-8\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
        <title>Rond de Toren &bull; {{ pageTitle }}</title>
        <base href=\"{{ base_url() }}\">
        <!-- Bootstrap -->
        <link href=\"{{ base_url() }}css/bootstrap-front-min.css\" rel=\"stylesheet\">
        <link href=\"{{ base_url() }}css/bootstrap-custom-front.css\" rel=\"stylesheet\">
        <script type=\"text/javascript\" src=\"{{ base_url() }}js/jquery-defer.js\"></script> 
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src=\"https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js\"></script>
        <script src=\"https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js\"></script>
        <![endif]-->
    </head>
    <body id=\"front\">          
        <div id=\"main-container\">
            <header>
                <div class=\"header_image\"><a href=\"{{ base_url() }}\"><img src=\"{{ base_url() }}img/style/site_logo.png\" alt=\"Rond te Toren\" title=\"Rond de Toren\"></a></div>
                <div class=\"site_description\">
                    <h1>Rond<span class=\"r270\">de</span>Toren</h1>
                    <h2 class=\"text-success\">Loon op Zand</h2>
                </div>
                <div class=\"years text-success\"> 1967 - {{ now|date(\"Y\") }}</div>
            </header>
            <div class=\"container-fluid\">                
                <nav class=\"navbar navbar-default\" role=\"navigation\">
                    <div class=\"navbar-header\">
                        <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#navbarCollapse\">
                            <span class=\"sr-only\">Toggle navigation</span>
                            <span class=\"icon-bar\"></span>
                            <span class=\"icon-bar\"></span>
                            <span class=\"icon-bar\"></span>
                      </button>
                    </div><!-- /.navbar-header -->
                    <div id=\"navbarCollapse\" class=\"collapse navbar-collapse\">
                        <ul class=\"nav navbar-nav\">
                            {% for item in menu %}
                                <li class=\"{% if item.children %}dropdown{% endif %}{% if item.active %} active{% endif %}\">
                                    <a href=\"{{ site_url(item.url) }}\" {% if item.children %} class=\"dropdown-toggle\" data-toggle=\"dropdown\"{% endif %}>
                                        {{ item.name }}{% if item.children %} <b class=\"caret\"></b>{% endif %}
                                    </a>
                                    {% if item.children %}
                                        <ul class=\"dropdown-menu\">
                                            {% for child in item.children %}
                                                <li><a href=\"{{ site_url(child.url) }}\">{{ child.name }}</a>
                                            {% endfor %}
                                        </ul>
                                    {% endif %}
                                </li>
                            {% endfor %}
                        </ul>
                    </div>
                </nav>                
", "front/header.twig", "C:\\MAMP\\htdocs\\localdev\\rdt\\cms\\application\\views\\front\\header.twig");
    }
}
