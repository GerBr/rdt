<?php

/* login/loginIndex.twig */
class __TwigTemplate_8468dd8675bf3df297143f1f4568b2f02e7dd7df3e8b656c9e6ebbb510315e1c extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo ((($context["message"] ?? null)) ? (($context["message"] ?? null)) : (""));
        echo " 
";
        // line 2
        echo ((($context["warning"] ?? null)) ? (($context["warning"] ?? null)) : (""));
        echo " 

";
        // line 4
        echo form_open("cms/login/loginProcess", array("class" => "form", "id" => "form"));
        echo "
<div class=\"form-group\">";
        // line 5
        echo lang("email", "email");
        echo form_input(($context["optionsEmail"] ?? null));
        echo "</div>
<div class=\"form-group\">";
        // line 6
        echo lang("password", "password");
        echo form_password(($context["optionsPassword"] ?? null));
        echo "</div>
<div class=\"form-group\">
    <a class=\"btn btn-default\" href=\"";
        // line 8
        echo twig_escape_filter($this->env, site_url("cms/login/forgot"), "html", null, true);
        echo "\">";
        echo lang("login_password_forgot");
        echo "</a>
    <span class=\"pull-right\">";
        // line 9
        echo form_submit(array("class" => "btn btn-primary btn-lg", "name" => "submit", "value" => lang("form_submit_generic")));
        echo "</span>
</div>
";
        // line 11
        echo form_close();
    }

    public function getTemplateName()
    {
        return "login/loginIndex.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  58 => 11,  53 => 9,  47 => 8,  41 => 6,  36 => 5,  32 => 4,  27 => 2,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ message|raw ?: '' }} 
{{ warning|raw ?: '' }} 

{{ form_open('cms/login/loginProcess', {'class': 'form', 'id': 'form'}) }}
<div class=\"form-group\">{{ lang('email', 'email') }}{{ form_input(optionsEmail) }}</div>
<div class=\"form-group\">{{ lang('password', 'password') }}{{ form_password(optionsPassword) }}</div>
<div class=\"form-group\">
    <a class=\"btn btn-default\" href=\"{{ site_url('cms/login/forgot') }}\">{{ lang('login_password_forgot') }}</a>
    <span class=\"pull-right\">{{ form_submit({'class': 'btn btn-primary btn-lg', 'name': 'submit', 'value': lang('form_submit_generic')}) }}</span>
</div>
{{ form_close() }}", "login/loginIndex.twig", "C:\\MAMP\\htdocs\\localdev\\rdt\\cms\\application\\views\\login\\loginIndex.twig");
    }
}
