<?php
/*
 * NL language file
 */

// A
$lang['active'] = 'Actief';
$lang['action'] = 'Actie';
$lang['action_success'] = 'Actie succesvol';
$lang['action_success_not'] = 'Actie niet succesvol';
$lang['add'] = 'Toevoegen';
$lang['add_not_successfull'] = 'Niet succesvol toegevoegd';
$lang['add_successfull'] = 'Succesvol toegevoegd';
$lang['admin_log_action_overview'] = 'Overzicht actie logs';
$lang['admin_pages_overview'] = 'Overzicht pagina\'s';
$lang['admin_users'] = 'Gebruikers';
$lang['admin_users_last_login'] = 'Laatste ingelogde gebruikers';
$lang['admin_users_overview'] = 'Overzicht gebruikers';
$lang['autonumber'] = '#';
$lang['auto_complete'] = 'Auto complete';

// C
$lang['calendar_add'] = 'Agenda-item toevoegen';
$lang['calendar_edit'] = 'Agenda-item bewerken';
$lang['cancel'] = 'Annuleer';
$lang['click_select'] = 'Klik om te kiezen...';
$lang['close'] = 'Sluiten';
$lang['columns'] = 'Kolommen';
$lang['comment'] = 'Commentaar';
$lang['confirmation'] = 'Weet u het zeker?';
$lang['confirmation_title'] = 'Bevestiging';
$lang['contact_form'] = 'Contactformulier';
$lang['contact_form_sent'] = 'Contactformulier succesvol verzonden';
$lang['content'] = 'Inhoud';
$lang['created_at'] = 'Aangemaakt';

// D
$lang['date'] = 'Datum';
$lang['days'] = 'dagen';
$lang['delete'] = 'Verwijderen';
$lang['deleted_records_shown'] = 'Verwijderde records';
$lang['details'] = 'Details';

// E
$lang['edit'] = 'Bewerk';
$lang['edit_not_successfull'] = 'Niet succesvol bewerkt';
$lang['edit_rights'] = 'Bewerk rechten';
$lang['edit_successfull'] = 'Succesvol bewerkt';
$lang['edition'] = 'Editie';
$lang['edition_add'] = 'Editie toevoegen';
$lang['edition_edit'] = 'Editie bewerken';
$lang['effect'] = 'Effect';
$lang['effect_description'] = 'Effect beschrijving';
$lang['effect_docs'] = 'Effect docs';
$lang['email'] = 'E-mail';
$lang['end_date'] = 'Einddatum';
$lang['error'] = 'Fout';
$lang['errors'] = 'Fouten';
$lang['error_browser_too_old'] = 'Waarschuwing: uw huidige browser is verouderd en wordt niet ondersteund. Om deze site te kunnen gebruiken dient u gebruik te maken van een moderne browser.';
$lang['error_captcha_not_valid'] = 'Veiligheidscode niet correct - probeer het nogmaals s.v.p.';
$lang['error_cannot_delete_self'] = 'U kunt zichzelf niet verwijderen';
$lang['error_email_not_found'] = 'E-mail niet gevonden - probeer het nogmaals s.v.p.';
$lang['error_email_not_valid'] = 'E-mail niet valide - probeer het nogmaals s.v.p.';
$lang['error_missing_parameter'] = 'Parameter ontbreekt';
$lang['error_no_access'] = 'Geen toegang';
$lang['error_unknown'] = 'Onbekende fout opgetreden';
$lang['error_upload'] = 'Fout bij uploaden';
$lang['executed_by'] = 'Uitgevoerd door';
$lang['explain'] = 'Toelichting';
$lang['explanation'] = 'Toelichting';

// F
$lang['files_available'] = 'Beschikbare files';
$lang['files_not_found'] = 'Geen documenten gevonden';
$lang['file_add'] = 'Toevoegen';
$lang['file_delete'] = 'Verwijderen';
$lang['file_download'] = 'Download';
$lang['file_download_pdf'] = 'Download PDF';
$lang['file_download_zip'] = 'Download ZIP';
$lang['file_in_use'] = 'File in gebruik';
$lang['file_name'] = 'File naam';
$lang['file_path'] = 'Pad';
$lang['file_size'] = 'Grootte';
$lang['file_type'] = 'Type';
$lang['file_uploaded'] = 'Uploaded';
$lang['file_url'] = 'Url';
$lang['filter'] = 'Filter';
$lang['filter_all'] = 'Alles';
$lang['filter_reset'] = 'Reset filter';
$lang['form_add_file'] = 'Toevoegen file';
$lang['form_add_generic'] = 'Toevoegen';
$lang['form_add_image'] = 'Toevoegen image';
$lang['form_add_inline'] = 'Toevoegen inline';
$lang['form_cancel_generic'] = 'Annuleer';
$lang['form_choose_option'] = 'Kies optie';
$lang['form_continue'] = 'Ga verder';
$lang['form_copy_generic'] = 'Kopieer';
$lang['form_delete_generic'] = 'Verwijderen';
$lang['form_done'] = 'Sluiten';
$lang['form_download_generic'] = 'Download';
$lang['form_download_zip'] = 'Download ZIP';
$lang['form_edit_generic'] = 'Bewerk';
$lang['form_hard_delete_generic'] = 'Permanent verwijderen';
$lang['form_generic_date_format'] = 'yyyy-mm-dd';
$lang['form_hide_deleted'] = 'Verberg verwijderd';
$lang['form_loading'] = 'Ogenblik s.v.p...';
$lang['form_next'] = 'Volgende';
$lang['form_not_applicable'] = 'Niet van toepassing';
$lang['form_password_bad'] = 'Erg zwak';
$lang['form_password_good'] = 'Goed';
$lang['form_password_strong'] = 'Sterk';
$lang['form_password_weak'] = 'Zwak';
$lang['form_replace_file'] = 'Vervang bestand';
$lang['form_save_generic'] = 'Opslaan';
$lang['form_select_date'] = 'Kies datum';
$lang['form_select_file'] = 'Kies bestand';
$lang['form_show_deleted'] = 'Toon verwijderd';
$lang['form_soft_delete_generic'] = 'Naar prullenbak';
$lang['form_start_generic'] = 'Start';
$lang['form_submit_back'] = 'Terug';
$lang['form_submit_back_overview'] = 'Terug naar overzicht';
$lang['form_submit_change'] = 'Wijzig';
$lang['form_submit_generate'] = 'Genereer';
$lang['form_submit_generic'] = 'Verzenden';
$lang['form_submit_generic_copy'] = 'Verzenden + kopieer';
$lang['form_submit_generic_done'] = 'Verzenden + klaar';
$lang['form_submit_generic_new'] = 'Verzenden + nieuw';
$lang['form_submit_next'] = 'Volgende';
$lang['form_truncate_generic'] = 'Leeg tabel';
$lang['form_undelete_generic'] = 'Herstellen';
$lang['form_upload_file'] = 'Upload bestand';
$lang['form_user_activation_email'] = 'Wachtwoord reset email';
$lang['form_user_log'] = 'Toon log';
$lang['form_user_reset_security_cookie'] = 'Herstel security cookie';
$lang['form_validation_user_email_check'] = 'Dit emailadres bestaat al';
$lang['form_view_generic'] = 'Bekijk';

// G
$lang['gallery_add'] = 'Fotogalerij toevoegen';
$lang['gallery_edit'] = 'Fotogalerij bewerken';
$lang['general'] = 'Algemeen';

// I
$lang['id'] = 'Id';
$lang['image'] = 'Afbeelding';
$lang['images_available'] = 'Beschikbare images';
$lang['image_delete'] = 'Verwijderen';
$lang['image_download_zip'] = 'Download als zip';
$lang['image_management_notes'] = '<ul><li>Toegestane image types: jpg, png</li><li>Image size max 5Mb</li></ul>';
$lang['image_management_note_header'] = 'Opmerkingen';
$lang['image_name'] = 'Image naam';
$lang['image_size'] = 'Grootte';
$lang['image_thumbnail'] = 'Thumbnail';
$lang['image_uploaded'] = 'Datum';
$lang['image_upload_not_successfull'] = 'Image NIET succesvol geupload!';
$lang['image_upload_success'] = 'Image succesvol geupload';
$lang['image_url'] = 'Image url';
$lang['image_view'] = 'Preview';
$lang['info'] = 'Informatie';
$lang['intro'] = 'Introductie';

// K
$lang['key'] = 'Sleutel';

// L
$lang['last_login'] = 'Laatste login';
$lang['linked_ file'] = 'Gekoppeld bestand';
$lang['location_website'] = 'Website';
$lang['location_website_example'] = 'http://www.example.com';
$lang['log_details'] = 'Log details';
$lang['logged_in'] = 'Ingelogd';
$lang['login_captcha'] = 'Veiligheidscontrole';
$lang['login_captcha_retype'] = 'Type de karakters over uit het plaatje';
$lang['login_change_password_text'] = 'Kies een nieuw wachtwoord (min 8 karakters)';
$lang['login_forgot_email_reset_greeting'] = '(Deze e-mail is automatisch verzonden, reacties hierop worden niet gelezen)';
$lang['login_forgot_email_reset_intro'] = 'U heeft een wachtwoord reset aangevraagd voor Rond de Toren CMS. Klik op de link om uw wachtwoord opnieuw in te stellen. Let op: deze link is slechts 24 uur geldig.';
$lang['login_forgot_email_sent'] = 'Wachtwoordreset e-mail is verzonden. Controleer uw e-mail en volg de instructies.';
$lang['login_forgot_password_heading'] = 'Wachtwoord instellen';
$lang['login_forgot_password_text'] = 'Vul uw e-mailadres in en neem de letters van de veiligheidscontrole over om het wachtwoord-vergeten proces te starten.';
$lang['login_header'] = 'Welkom';
$lang['login_intro'] = 'Login';
$lang['login_invalid'] = 'Login niet gelukt, probeer het nogmaals s.v.p.';
$lang['login_maintenance'] = '<h3>Onderhoudsmodus</h3>Op dit moment voeren wij onderhoud uit op het systeem. Excuses voor het ongemak!<br>Inloggen tijdelijk niet mogelijk, probeer het later nogmaals.';
$lang['login_password_forgot'] = 'Wachtwoord vergeten';
$lang['login_password_reset_success'] = 'Wachtwoordreset succesvol - u kunt nu inloggen met uw e-mailadres en zojuist ingestelde wachtwoord';
$lang['login_placeholder_captcha'] = 'Veiligheidscontrole (niet hoofdletter gevoelig)';
$lang['login_placeholder_email'] = 'E-mail';
$lang['login_placeholder_password'] = 'Wachtwoord';
$lang['login_placeholder_password_confirm'] = 'Herhaal wachtwoord';
$lang['login_successfull'] = 'U bent ingelogd.';
$lang['logout'] = 'Uitloggen';
$lang['logout_success'] = 'U bent uitgelogd';
$lang['logs_overview'] = 'Overzicht logs';

// M
$lang['maintenance_mode'] = 'Onderhouds mode - Momenteel is inloggen niet mogelijk - probeer het later nogmaals svp.';
$lang['menu_hide'] = 'Verborgen';
$lang['menu_id'] = 'Menu Id';
$lang['menu_name'] = 'Menu naam';
$lang['menu_reset_success'] = 'Menu en rechten succesvol gereset';
$lang['menu_rights_reset'] = 'Menu en rechten resetten';
$lang['message'] = 'Bericht';
$lang['message_placeholder'] = 'Uw bericht';
$lang['meta_tags'] = 'Meta tags';
$lang['month_01'] = 'januari';
$lang['month_02'] = 'februari';
$lang['month_03'] = 'maart';
$lang['month_04'] = 'april';
$lang['month_05'] = 'mei';
$lang['month_06'] = 'juni';
$lang['month_07'] = 'juli';
$lang['month_08'] = 'augustus';
$lang['month_09'] = 'september';
$lang['month_10'] = 'oktober';
$lang['month_11'] = 'november';
$lang['month_12'] = 'december';
$lang['month_01_short'] = 'jan';
$lang['month_02_short'] = 'feb';
$lang['month_03_short'] = 'mrt';
$lang['month_04_short'] = 'apr';
$lang['month_05_short'] = 'mei';
$lang['month_06_short'] = 'jun';
$lang['month_07_short'] = 'jul';
$lang['month_08_short'] = 'aug';
$lang['month_09_short'] = 'sep';
$lang['month_10_short'] = 'okt';
$lang['month_11_short'] = 'nov';
$lang['month_12_short'] = 'dec';

// N
$lang['na'] = 'nvt';
$lang['name'] = 'Naam';
$lang['news_content'] = 'Nieuws content';
$lang['news_add'] = 'Toevoegen nieuws item';
$lang['news_edit'] = 'Bewerk nieuws item';
$lang['news_header'] = 'Overzicht nieuws items';
$lang['news_item'] = 'Nieuws item';
$lang['no'] = 'Nee';
$lang['no_future_editions'] = 'Nog geen verschijningsdatum gepland';
$lang['notes'] = 'Opmerkingen';
$lang['not_activated'] = 'Niet geactiveerd';
$lang['not_applicable'] = 'Niet van toepassing';
$lang['not_successfully_added'] = 'Niet succesvol toegevoegd';
$lang['not_successfully_deleted'] = 'Niet succesvol verwijderd';
$lang['not_successfully_downloaded'] = 'File niet succesvol gedownload';
$lang['not_successfully_modified'] = 'Niet succesvol opgeslagen';
$lang['not_successfully_restored'] = 'Niet succesvol hersteld';
$lang['no_data_found'] = 'Geen data gevonden';
$lang['no_row_selected'] = 'Geen rij gevonden';
$lang['no_user_found'] = 'Gebruiker niet gevonden';
$lang['number'] = 'Nummer';

// O
$lang['off'] = 'Uit';
$lang['on'] = 'Aan';

// P
$lang['page_add'] = 'Toevoegen pagina';
$lang['page_content'] = 'Pagina content';
$lang['page_edit'] = 'Bewerk pagina';
$lang['page_exists'] = 'Pagina bestaat al';
$lang['page_header'] = 'Pagina overzicht';
$lang['page_name'] = 'Pagina naam';
$lang['page_not_found'] = 'Pagina niet gevonden';
$lang['parent'] = 'Moederpagina';
$lang['password'] = 'Wachtwoord';
$lang['password_new'] = 'Nieuw wachtwoord';

// R
$lang['read_more'] = 'Lees meer';
$lang['releasedate'] = 'Uitgavedatum';
$lang['releasedate_explain'] = 'Aan de hand van de uitgavedatum wordt de jaargang, kopijdatum, etc. berekend.';
$lang['releaseyear'] = 'Jaargang';
$lang['role'] = 'Rol';
$lang['role_add'] = 'Toevoegen rol';
$lang['role_description'] = 'Beschrijving';
$lang['role_edit'] = 'Bewerk rol';
$lang['role_id'] = 'Rol id';
$lang['role_name'] = 'Rol naam';

// S
$lang['save'] = 'Opslaan';
$lang['score'] = 'Score';
$lang['search'] = 'Zoeken';
$lang['select'] = 'Selecteer';
$lang['status'] = 'Status';
$lang['store_success'] = 'Succesvol opgeslagen';
$lang['store_not_success'] = 'Niet succesvol opgeslagen';
$lang['string_name'] = 'String';
$lang['string_text'] = 'Tekst';
$lang['submit_button'] = 'Button tekst';
$lang['sub_menu_user_edit'] = 'Contactgegevens - bewerken';
$lang['sub_menu_user_index'] = 'Contactgegevens';
$lang['sub_menu_user_log'] = 'Logs';
$lang['sub_menu_user_view'] = 'Overzicht';
$lang['sub_menu_user_view_add'] = 'Bekijk - toevoegen';
$lang['subject'] = 'Onderwerp';
$lang['successfully_added'] = 'Succesvol toegevoegd';
$lang['successfully_added_email_sent'] = 'Succesvol geregistreerd - aanvraag wordt zsm in behandeling genomen.';
$lang['successfully_deleted'] = 'Succesvol verwijderd';
$lang['successfully_soft_deleted'] = 'Succesvol naar prullenbak verplaatst';
$lang['successfully_restored'] = 'Succesvol hersteld';
$lang['successfully_disabled'] = 'Succesvol uitgeschakeld';
$lang['successfully_modified'] = 'Succesvol opgeslagen';

// T
$lang['text'] = 'Tekst';
$lang['textstring_comment'] = 'Opmerking';
$lang['textstring_content'] = 'String content';
$lang['textstring_edit'] = 'Bewerk tekst string';
$lang['textstring_header'] = 'Tekst strings overzicht';
$lang['textstring_name'] = 'String naam';
$lang['title'] = 'Titel';
$lang['token'] = 'Wachtwoord reset link';
$lang['torentje_add'] = 'Torentje toevoegen';
$lang['torentje_edit'] = 'Torentje bewerken';
$lang['total'] = 'Totaal';
$lang['type_id'] = 'Type id';
$lang['type_name'] = 'Type';

// U
$lang['unknown'] = 'Onbekend';
$lang['unremovable'] = 'Niet te verwijderen';
$lang['updated_at'] = 'Wijzigingsdatum';
$lang['url'] = 'Link';
$lang['user_activation_email_sent'] = 'Activatie email is naar gebruiker verzonden';
$lang['user_activation_mail'] = 'Stuur activatie email naar gebruiker';
$lang['user_add'] = 'Toevoegen gebruiker';
$lang['user_check'] = 'E-mail controle';
$lang['user_edit'] = 'Bewerk gebruiker';
$lang['user_email'] = 'E-mail';
$lang['user_id'] = 'Gebruiker Id';
$lang['user_name'] = 'Naam';
$lang['user_pw_reset_send'] = 'Wachtwoord reset email verzonden';
$lang['user_view'] = 'Bekijk gebruiker';

// V
$lang['validation_error_does_already_exist'] = 'bestaat al';
$lang['validation_error_does_not_exist'] = 'bestaat niet';
$lang['validation_error_email_exists'] = 'e-mail bestaat al';
$lang['validation_error_in_use'] = 'Dit record kan niet worden verwijderd omdat het in gebruik is';
$lang['validation_error_not_a_date'] = 'geen valide datum';
$lang['validation_error_not_a_number'] = 'geen nummer';
$lang['validation_error_not_unique'] = 'geen unieke waarde';
$lang['validation_error_no_id'] = 'Geen id ontvangen';
$lang['validation_error_no_valid_option'] = 'geen valide optie';
$lang['validation_error_number_colums_not_correct'] = 'Aantal kolommen is niet correct';
$lang['validation_error_too_short'] = 'te kort';
$lang['views_overview'] = 'Weergaven';
$lang['view_add'] = 'Toevoegen view';
$lang['view_edit'] = 'Bewerk view';
$lang['view_name'] = 'Bekijk naam';
$lang['view_personal'] = 'Personal view';
$lang['view_reset'] = 'Reset view';
$lang['view_select_columns'] = 'Selecteer kolommen';
$lang['visitors'] = 'Aantal personen';

// W
$lang['warning_leaving_page'] = 'Waarschuwing: informatie is niet opgeslagen en kan verloren gaan!';
$lang['warning_title'] = 'Waarschuwing';

// Y
$lang['yes'] = 'Ja';
