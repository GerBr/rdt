<?php

$lang['form_validation_required'] = "Het veld %s is verplicht.";
$lang['form_validation_isset'] = "Het veld %s moet een waarde hebben.";
$lang['form_validation_valid_email'] = "Het veld %s moet een geldig e-mailadres bevatten.";
$lang['form_validation_valid_emails'] = "Het veld %s mag alleen geldige e-mailadressen bevatten.";
$lang['form_validation_valid_url'] = "Het veld %s moet een geldige URL bevatten.";
$lang['form_validation_valid_ip'] = "Het veld %s moet een geldig IP-adres bevatten.";
$lang['form_validation_min_length'] = "Het veld %s moet minimaal %s tekens lang zijn.";
$lang['form_validation_max_length'] = "Het veld %s mag maximaal %s tekens lang zijn.";
$lang['form_validation_exact_length'] = "Het veld %s moet precies %s tekens lang zijn.";
$lang['form_validation_alpha'] = "Het veld %s mag alleen alfabetische tekens bevatten.";
$lang['form_validation_alpha_numeric'] = "Het veld %s mag alleen alfabetische tekens bevatten.";
$lang['form_validation_alpha_dash'] = "Het veld %s mag alleen alfabetische tekens, underscores, and streepjes bevatten.";
$lang['form_validation_numeric'] = "Het veld %s mag alleen nummers bevatten.";
$lang['form_validation_is_numeric'] = "Het veld %s mag alleen numerieke tekens bevatten.";
$lang['form_validation_integer'] = "Het veld %s mag alleen een integer bevatten.";
$lang['form_validation_regex_match'] = "Het veld %s voldoet niet aan het juiste formaat.";
$lang['form_validation_matches'] = "Het veld %s is niet gelijk aan het veld %s.";
$lang['form_validation_is_unique'] = "%s is reeds in gebruik.";
$lang['form_validation_is_natural'] = "Het veld %s mag alleen positieve getallen bevatten.";
$lang['form_validation_is_natural_no_zero'] = "Het veld %s moet een waarde groter dan 0 bevatten.";
$lang['form_validation_decimal'] = "Het veld %s moet een decimaal getal bevatten.";
$lang['form_validation_less_than'] = "Het veld %s moet een waarde kleiner dan %s bevatten.";
$lang['form_validation_greater_than'] = "Het veld %s moet een waarde groter dan %s bevatten.";

/* End of file form_validation_lang.php */
/* Location: ./system/language/nl/form_validation_lang.php */