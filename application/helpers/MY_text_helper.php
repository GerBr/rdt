<?php

function langstring($key) 
{
    $CI = & get_instance();
    $CI->load->model('Content_model');
    $output =  '<div class="langstring">' . $CI->Content_model->langstring($key) . '</div>';
    return $output;
}