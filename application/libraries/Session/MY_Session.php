<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class MY_Session extends CI_Session
{

    /**
     * Get all flash data messages as full HTML code
     * @param $extra array specifying bootstrap alert type and content
     * @return string
     */
    public function parseFlashMessages($extra = NULL)
    {
        $message = '';
        if ($this->flashdata('message') != '') {
            $message .= $this->parseMessage($this->flashdata('message'), 'success');
        }

        if ($this->flashdata('message_error') != '') {
            $message .= $this->parseMessage($this->flashdata('message_error'), 'danger');
        }
        if (!empty($extra)) {
            foreach ($extra as $msg) {
                $message .= $this->parseMessage($msg['content'], $msg['type'], isset($msg['dismissable']) ? $msg['dismissable'] : false);
            }
        }
        return $message;
    }

    /**
     * Build a message div
     * @param string    $msg            The textual message
     * @param string    $type           Either success or danger or warning
     * @param bool      $dismissable    True for adding dismissable option
     * @return type
     */
    private function parseMessage($msg, $type, $dismissable = true)
    {
        $message = '<div class="alert alert-' . $type;
        if ($dismissable) {
            $message .= ' alert-dismissable';
        }
        $message .= '">';
        if ($dismissable) {
            $message .= '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
        }
        $message .= $msg;
        if ($type != 'warning') {
            $message .= ' <span class="glyphicon glyphicon-';
            if ($type == 'success') {
                $message .= 'ok';
            } else {
                $message .= 'remove';
            }
            $message .= '"></span>';
        }
        return $message . '</div>';
    }
}
