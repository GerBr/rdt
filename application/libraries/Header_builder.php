<?php

/**
 * Build menu
 *
 * @packagage   CodeIgniter
 * @category	Database
 * @author      GB
 *
 */
class Header_builder
{

    /**
     *
     * @var type
     */
    public $menuId = 1;
    public $section = false;
    protected $accountId = 0;
    protected $userId = 0;
    protected $roleId = 0;

    /**
     * Constructor
     * Set main CI instance, session and DB connection
     */
    public function __construct()
    {

        $this->CI = & get_instance();
        $this->userId = $this->CI->session->userdata('id');
        $this->roleId = $this->CI->session->userdata('roleId');
    }

    /**
     *
     * @param type $menuId
     * @param type $page
     */
    public function controllerSet($menuId, $section = false)
    {
        $this->menuId = $menuId;
        if ($section) {
            $this->section = $section;
        }
    }

    /**
     * Fetch vars needed for overall_header
     * @param array $extraVars
     * @return array
     */
    public function headerVarsGet($extraVars = array())
    {
        $menu = $this->menuItemsGet($this->CI->session->userdata('roleId'));
        $header = array(
            'userId' => $this->userId,
            'userName' => $this->CI->session->userdata('userName'),
            'userEmail' => $this->CI->session->userdata('userEmail'),
            'roleName' => $this->CI->session->userdata('roleName'),
            'companyName' => $this->CI->session->userdata('companyName'),
            'loggedIn' => ($this->CI->session->userdata('loggedIn') ? TRUE : FALSE),
            'menu' => $menu,
            'pageName' => $menu[$this->menuId]['menu_name'] ?? '',
            'section' => $this->section,
        );
        

        // Since these are all common vars, pass them as Global
        $headerArray = array_merge($header, $extraVars);
        foreach ($headerArray as $key => $var) {
            $this->CI->twig->addGlobal($key, $var);
        }
        return $headerArray;
    }
    
    /**
     * Get menu items for CMS
     * New menu items have to be defined below
     * @return array
     */
    public function menuItemsGet($role)
    {
        // Define menu items
        $menu = [
            0 => ['menu_name' => 'Toon website', 'menu_path' => '', 'ordering' => 10, 'auth' => 10],
            1 => ['menu_name' => 'Home', 'menu_path' => 'cms/Home', 'ordering' => 10, 'auth' => 10],
            2 => ['menu_name' => 'Beheer', 'menu_path' => 'cms/Admin', 'ordering' => 20, 'auth' => 30, 
                'subs' => [
                    ['menu_sub_name' => 'Gebruikers', 'menu_sub_path' => 'cms/Admin/userAsync', 'ordering' => 10],
                    ['menu_sub_name' => 'Logboek', 'menu_sub_path' => 'cms/Admin/logAsync', 'ordering' => 20],
                    ],
                ],
            3 => ['menu_name' => 'Vaste inhoud', 'menu_path' => 'cms/Static', 'ordering' => 20, 'auth' => 20, 
                'subs' => [
                    ['menu_sub_name' => 'Pagina\'s', 'menu_sub_path' => 'cms/Page', 'ordering' => 10],
                    ['menu_sub_name' => 'Teksten', 'menu_sub_path' => 'cms/Textstring', 'ordering' => 20],
                    ],
                ],
            4 => ['menu_name' => 'Wisselende inhoud', 'menu_path' => 'cms/Content', 'ordering' => 40, 'auth' => 10,
                'subs' => [
                    ['menu_sub_name' => 'Nieuws', 'menu_sub_path' => 'cms/News/index', 'ordering' => 10],                    
                    ['menu_sub_name' => 'Torentjes', 'menu_sub_path' => 'cms/Torentjes', 'ordering' => 20],
                    ['menu_sub_name' => 'Fotogalerij', 'menu_sub_path' => 'cms/Galleries', 'ordering' => 30],
                    ['menu_sub_name' => 'Agenda', 'menu_sub_path' => 'cms/Calendar', 'ordering' => 40],
                    ],
                ],
            5 => ['menu_name' => 'Edities', 'menu_path' => 'cms/Editions', 'ordering' => 50, 'auth' => 10, ],
        ];
        
        foreach ($menu as $menuId => $item) {
            if ($role >= $item['auth']) {
                if ($menuId == $this->menuId) {
                    $item['active'] = true;
                }
                $return[$menuId] = $item;
            }
        }
        return $return ?? '';
    }
    
    /**
     * Get front facing menu structure
     * @param string $currentMain current main menu item
     * @return array
     */
    public function frontMenuGet($currentMain = 'Home')
    {
        // Predefined menuitems
        $first = [
            ['name' => 'Nieuws', 'url' => 'Home'],  
            ['name' => 'Archief', 'url' => 'Archief', 'children' => [
                ['name' => 'Jaargangen', 'url' => 'Archief/index'],
                ['name' => 'Zoeken', 'url' => 'Archief/Zoeken'],
                ['name' => 'Fotogalerij', 'url' => 'Archief/Fotogalerij'],
                ],
            ],
            ['name' => 'Torentjes', 'url' => 'Torentjes'],  
        ];
        $last = [
            ['name' => 'Agenda', 'url' => 'Agenda'],  
        ];
        
        // Pages
        $pages = $this->CI->Page_model->pageMenuItemsGet();
        
        // Paste it together and mark current
        $menu = array_merge($first, $pages, $last);
        foreach ($menu as $key => $item){
            if ($currentMain == $item['url']) {
                $menu[$key]['active'] = true;
                break;
            }
        }
        return $menu;
    }
}

// EoF