<?php

/**
 * Make building (AJAX) data tables simpler
 * @packagage   CodeIgniter
 * @author      Ger Bruinsma
 *
 * Usually it's enough to simply call getDefaultOutput($model, $method, [$translate])
 * You can also choose to call the separte methods setParams() and getJson() if you need some extra handling
 */
class data_tables
{

    /**
     * @var array $params
     */
    public $params;

    /**
     * @var object $postdata
     */
    protected $postdata;

    /**
     * @var array $special_params
     */
    protected $special_params = ['start', 'var1', 'var2', 'var3', 'columns'];

    /**
     * @var object $CI
     */
    protected $CI;

    /**
     * Constructor
     * Set main CI instance, session and DB connection
     */
    public function __construct()
    {
        $this->CI = & get_instance();
        $this->roleId = $this->CI->session->userdata('roleId');
        $this->accountId = $this->CI->session->userdata('accountId');
        $this->CI->load->model('User_model');
        $this->setDefaultParams();
    }

    /**
     * Set parameters for this table
     * @param array $cols
     * @param object $parameters
     * @return object   Combined object holding default parameters, postdata parameters and added custom parameters
     */
    public function setParams($parameters = null)
    {

        //var_dump($parameters);

        $this->postdata = $this->CI->input->post(NULL, TRUE);
        $this->setCols();

        // Write postdata parameters first, allows for overwriting in method call
        $this->writeParams($this->postdata, TRUE);
        $this->writeParams($parameters);

        // Special postdata options
        $this->params->start = (int) $this->postdata['start'];
        $this->params->filter_search = $this->postdata['search'];

        if (isset($this->postdata['length'])) {
            $this->storeLength($this->postdata['length']);
        }
        if (isset($this->postdata['var1']) && $this->postdata['var1'] == 'filter') {
            $this->params->filter_var = $this->postdata['var2'];
            $this->params->filter_val = $this->postdata['var3'];
        }
        if (isset($this->postdata['var1']) && $this->postdata['var1'] == 'showSoftDeletes') {
            $this->params->showSoftDeletes = TRUE;
        }
        if (isset($this->postdata['order'])) {
            $this->params->sort = $this->cols[$this->postdata['order'][0]['column']];
            $this->params->order = $this->postdata['order'][0]['dir'];
        }
        if (isset($this->postdata['columns'])) {
            foreach ($this->postdata['columns'] as $array) {
                if (strlen($array['search']['value']) > 0) {
                    $this->params->columnSearch[] = array('variable' => $array['name'], 'value' => $array['search']['value'], 'regex' => $array['search']['regex']);
                }
            }
        }

        return $this->params;
    }

    /**
     * Build JSON encoded output from given rows
     * @param array $rows
     * @param int $count
     * @return string
     */
    public function getJson($rows, $count, $translate = false)
    {
        $tabledata = array();
        if ($rows) {
            if (is_array($translate)) {
                $rows = $this->translate($rows, $translate);
            }
            foreach ($rows as $row) {
                foreach ($this->cols as $index => $col) {
                    $add[$index] = $row[$col];
                }
                $tabledata[] = $add;
            }
        }

        $result = array(
            'draw' => (int) $this->CI->input->post('draw', TRUE),
            'data' => $tabledata,
            'recordsTotal' => (int) $count,
            'recordsFiltered' => (int) $count
        );

        return json_encode($result);
    }

    /**
     * Get the default full JSON output for data_tables
     * Basically a wrapper method for setParams, counting, getting the rows and parsing it as JSON
     * @param string $model     The model to load
     * @param string $method    The models' method to call
     * @param array $translate  Array of columns to translate (e.g. yes/no)
     * @return string           JSON formatted datatable output
     */
    public function getDefaultOutput($model, $method, $translate = false, $extraParams = null)
    {
        // Setup
        $this->CI->load->model($model);
        $this->setParams(['countAllRows' => true]);
        if ($extraParams) {
            foreach ($extraParams as $variable => $value) {
                $this->writeParams([$variable => $value]);
            }
        }
        // Count records
        $count = (int) $this->CI->$model->$method($this->params)[0]['counted'];
        unset($this->params->countAllRows);

        // Get rows and return JSON
        $rows = $this->CI->$model->$method($this->params);
        return $this->getJson($rows, $count, $translate);
    }

    /**
     * Translate any given columns as stated in the Tools lib
     * @param array $rows
     * @param array $translate
     * @return array
     */
    private function translate($rows, $translate)
    {
        foreach ($translate as $col => $options) {
            $prop = 'options_' . $options;
            if (isset($this->CI->tools->$prop)) {
                foreach ($this->CI->tools->$prop as $index => $value) {
                    $translateArray[$col][$index] = $this->CI->lang->line(strtolower($value));
                }
            } elseif ($options == 'lang_line') {
                $langAry[$col] = true;
            }
        }
        $return = array();
        foreach ($rows as $key => $row) {
            foreach ($row as $varname => $value) {
                if (is_null($value)) { // Basically invalid / no data
                    $return[$key][$varname] = '';
                } elseif (isset($translateArray[$varname])) { // Default translations
                    $return[$key][$varname] = $translateArray[$varname][$value];
                } elseif (isset($langAry[$varname])) { // Translated directly in language lines
                    $return[$key][$varname] = $this->CI->lang->line($varname . '_option_' . strtolower($value));
                } else { // Ordinary value
                    $return[$key][$varname] = $value;
                }
            }
        }
        return $return;
    }

    /**
     * Set columns based on received postdata
     * @return void
     */
    private function setCols()
    {
        if (isset($this->postdata['columns'])) {
            foreach ($this->postdata['columns'] as $col) {
                $this->cols[] = $col['name'];
            }
        }
    }

    /**
     * Set length in user pref
     * @param int $length
     * @return void
     */
    private function storeLength($length)
    {
//        if ($this->CI->session->userdata('tableRows') != $length) {
//            $this->CI->User_model->preferencesCreate($this->CI->session->userdata('id'), ['table_rows' => $length]);
//        }
    }

    /**
     * Set or overwrite parameter list
     * @param object $params            parameters to write
     * @param bool $exclude_specials    exclude special parameters
     * @return void
     */
    private function writeParams($params, $exclude_specials = false)
    {
        if (is_array($params) || is_object($params)) {
            foreach ($params as $key => $val) {
                if (!($exclude_specials && in_array($key, $this->special_params))) {
                    $this->params->$key = $val;
                }
            }
        }
    }

    /**
     * Define default parameters
     * @return void
     */
    private function setDefaultParams()
    {
        $this->params = new stdClass();
        $this->params->language = $this->CI->session->userdata('siteLang');
        $this->params->length = (int) $this->CI->session->userdata('tableRows');
    }
      
    /**
     * Echo AJAX response and exit 
     * @param array $parameters
     * @param int $exitcode
     */
    public function jsonSoftDelResponse($response, $msgOverride = false)
    {
        switch ($response) {
            case 'success':
                $success = true;
                $msg = $this->CI->lang->line('action_success');
                break;
            case 'inUse':
                $success = false;
                $msg = $this->CI->lang->line('validation_error_in_use');
                break;
            case 'missingParameter':
                $success = false;
                $msg = $this->CI->lang->line('error_missing_parameter');
                break;
            case 'error':
                $success = false;
                $msg = $this->CI->lang->line('error_unknown');
                break;
        }
        
        echo json_encode([
            'success' => $success,
            'error' => !$success,
            'msg' => $msgOverride ? $msgOverride : $msg,
            'reload' => $success,            
        ]);
        exit();
    }
}

// EoF