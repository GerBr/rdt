<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Tools Class
 *
 * Miscellaneous tools, not worthy of having a class of their own.
 *
 * @package	CodeIgniter
 * @subpackage	Libraries
 * @author      GB
 *
 */
Class Tools
{

    /**
     * Constructor
     *
     * Set main CI instance, session and DB connection
     */
    public function __construct()
    {
        $CI = & get_instance();
        $CI->load->library('session');
        $this->user_session = $CI->session->userdata;
        $this->db = $CI->load->database('default', TRUE);
    }

    /**
     * List of answer types yes/no
     * @var type
     */
    public $optionsYesNo = array(1 => 'yes', 2 => 'no');

    /**
     * List of answer types yes/no/na
     * @var type
     */
    public $optionsYesNoNa = array(0 => 'na', 1 => 'yes', 2 => 'no');


// Class and file ends here.
}
