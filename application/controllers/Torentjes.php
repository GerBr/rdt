<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Torentjes extends CI_Controller
{
    /**
     * Display Torentjes (mini-ads)
     * @author GB
     */
    
    public function __construct() {
        parent::__construct();
        $this->load->model('Edition_model');
        $this->load->model('Torentjes_model');
    }
    
    /**
     * Main function
     */
    public function index()
    {
        
        // Decide on editition
        $parameters = new stdClass();
        $parameters->varname = 'releasedate >';
        $parameters->value = date('Y-m-d', strtotime('- ' .INTERVAL_TORENTJES));
        $editions = $this->Edition_model->editionGetOverview($parameters);
        
        if ($editions) {
            foreach ($editions as $row) {
                $editionSelect[] = $row['id'];
            }
        }
        
        if (!$editions) {
            // Fallback to latest edition
            unset($parameters->varname, $parameters->value);
            $editions = $this->Edition_model->editionGetOverview($parameters);
        }
        $editionSelect[] = $editions[0]['id'];
        
        // Cleanup all that are not in allowed editions
        $this->Torentjes_model->torentjeCleanup($editionSelect);
        
        // Get ads for editions
        $parameters = new stdClass();
        $parameters->varname = 'edition_id';
        $parameters->value = $editionSelect;
        $parameters->sort = 'edition_id';
        $parameters->order = 'DESC';
        $torentjes = $this->Torentjes_model->torentjeGetOverview($parameters);
        
        if (!$torentjes) {
            $parameters = new stdClass();
            $parameters->sort = 'updated_at';
            $parameters->order = 'DESC';
            $parameters->lenght = 5;
            $parameters->start = 0;
            $torentjes = $this->Torentjes_model->torentjeGetOverview($parameters);
        }
        $headerVars = [
            'menu' => $this->header_builder->frontMenuGet('Torentjes'),
            'pageTitle' => 'Torentjes',
            ];
        
        $parseData = [
            'pageTitle' => 'Torentjes',
        ];
        foreach($torentjes as $key => $ad) {
            $ad['text'] = auto_link($ad['text'], 'both', true);
            if ($key % 2 == 0) {
                $parseData['col1'][] = $ad;
            } else {
                $parseData['col2'][] = $ad;
            }
        }
        
        $this->twig->display('front/header', $headerVars);
        $this->twig->display('front/torentjes', $parseData);
        $this->twig->display('front/footer');
    }
    
    
}