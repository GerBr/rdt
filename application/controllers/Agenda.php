<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Agenda extends CI_Controller
{
    /**
     * Display Calendar
     * @author GB
     */
    
    public function __construct() {
        parent::__construct();
        $this->load->model('Content_model');
    }
    
    /**
     * Main function
     */
    public function index()
    {        
        // Silent cleanup
        $this->Content_model->calendarCleanup(date('Y-m-d'));
        
        // Decide on what to show
        $year = (int) $this->input->post('year');
        if (empty($year) || ($year < date('Y'))) {
            // Default
            $year = date('Y');
            $month = date('m');
        }

        // Get items for year
        $items = $this->Content_model->calendarGetYearContent($year);
        if ($items) {
            if (!isset($month)) {
                $month = key($items);
            }
            foreach ($items as $key => $content) {
                $activeMonths[] = $key;
            }
        }
              
        // Build array of months to pick
        for($i=1;$i<13;$i++) {
            $monthnr = str_pad($i, 2, '0', STR_PAD_LEFT);
            $selectMonths[] = [
                'number' => $i, 
                'name' => $this->lang->line('month_' . $monthnr . ''), 
                'name_short' => $this->lang->line('month_' . $monthnr . '_short'), 
                'active' => in_array($i, $activeMonths),
                'content' => isset($items[$i]) ? $items[$i] : false,
                ];
        }
        
        // Display
        $headerVars = [
            'menu' => $this->header_builder->frontMenuGet(__CLASS__),
            'pageTitle' => __CLASS__,
            ];
        
        $parseData = [
            'pageTitle' => __CLASS__,
            'items' => $items,
            'activeYear' => $year,
            'activeMonth' => $month,
            'selectYears' => $this->Content_model->calendarGetYears(),
            'selectMonths' => $selectMonths,
        ];
         
        $this->twig->display('front/header', $headerVars);
        $this->twig->display('front/agenda', $parseData);
        $this->twig->display('front/footer', ['select2' => true]);
    }
    
    
}