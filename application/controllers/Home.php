<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home extends CI_Controller
{
    public function __construct() {
        parent::__construct();
        $this->load->model('Edition_model');
        $this->load->model('News_model');
    }
    
    public function index()
    {
        $headerVars = [
            'menu' => $this->header_builder->frontMenuGet(),
            'pageTitle' => 'Welkom',
            ];
        $this->twig->display('front/header', $headerVars);

        // Decide on editition
        $parameters = new stdClass();
        $parameters->withFile = true;
        $parameters->varname = 'releasedate >=';
        $parameters->value = date('Y-m-d', strtotime('- 2 week'));
        $parameters->sort = 'releasedate';
        $parameters->order = 'ASC';
        $edition = $this->Edition_model->editionGetOverview($parameters);

        if (!$edition) {
            // Fallback to latest edition
            unset($parameters->varname, $parameters->value);
            $edition = $this->Edition_model->editionGetOverview($parameters);
        }

        // Get news items for edition
        $parameters = new stdClass();
        $parameters->varname = 'edition_id';
        $parameters->value = $edition[0]['id'];
        $parameters->sort = 'updated_at';
        $parameters->order = 'DESC';
        $news = $this->News_model->newsGetOverview($parameters);
        
        if (!$news) {
            $parameters = new stdClass();
            $parameters->sort = 'updated_at';
            $parameters->order = 'DESC';
            $parameters->lenght = 5;
            $parameters->start = 0;
            $news = $this->News_model->newsGetOverview($parameters);
        }
        foreach($news as $key => $item) {
            $news[$key]['thumb'] = str_replace('/uploads/img/', '/uploads/small/', $item['img']);
        }
        $parseData = [
            'edition' => $edition[0],
            'news' => $news,
            'pageTitle' => 'Welkom bij Rond de Toren',
            ];
        
        $this->twig->display('front/news', $parseData);
        $this->twig->display('front/footer');
    }
}