<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Viewpage extends CI_Controller
{
    /**
     * Display front-end pages
     * @author GB
     */
    
    public $startDelim = '{{_';
    public $endDelim = '_}}';
    public $page;
    
    public function __construct() {
        parent::__construct();
        $this->load->helper('html');
        $this->load->model('Edition_model');
    }
    
    /**
     * Main function
     */
    public function index($url = null)
    {
        if (empty($url)) {
            redirect('Home');
        }
        $page = $this->Page_model->pageGet($url);
        $this->page = $page;
        if ($page['parent'] > 0) {
            $parent = $this->Page_model->pageGet((int) $page['parent']);
            $currentMain = $parent['url'];
        } else {
            $currentMain = $page['url'];
        }
        $headerVars = [
            'menu' => $this->header_builder->frontMenuGet($this->Page_model->pageUrlPrefix . $currentMain),
            'pageTitle' => $page['name'],
        ];
        $page['content'] = $this->replaceShortTags($page['content']);
        $parseData = [
            'message' => $this->session->parseFlashMessages(),
            'page' => $page,
        ];
        $this->twig->display('front/header', $headerVars);
        $this->twig->display('front/page', $parseData);
        $this->twig->display('front/footer');
    }
    
    
    /**
     * Replace given shorttags with proper contents
     * @param string $content
     * @return string
     */
    public function replaceShortTags($content)
    {
        preg_match_all('#' . $this->startDelim . '([a-zA-Z]+?)' . $this->endDelim . '#', $content, $matches);
        if ($matches) {
            foreach ($matches[0] as $key => $tag) {
                $method = $matches[1][$key];
                if (method_exists(__CLASS__, $method)) {
                    $replace[$tag] = $this->$method();
                }
            }            
        }
        if (isset($replace)) {
            foreach ($replace as $stored => $replacement) {
                $content = str_replace($stored, $replacement, $content);
            }
        }
        return $content;
    }

    /**
     * Return special img
     * @return string
     */
    private function adFormatExample()
    {
        return img(['src' => 'img/adformat/pic0.gif', 'id' => 'adFormatExample']);
    }
    
    /**
     * Build release schedule
     * @return string
     */
    public function releaseSchedule()
    {
        // Get future editions
        $parameters = new stdClass();
        $parameters->varname = 'releasedate >=';
        $parameters->value = date('Y-m-d');
        $editions = $this->Edition_model->editionGetOverview($parameters);
        if (!$editions) {
            return $this->lang->line('no_future_editions');
        }
        
        // Build structure for template
        $years = [];
        foreach ($editions as $row) {
            $year = date('Y', strtotime($row['releasedate']));
            if (!in_array($year, $years)){
                $years[]= $year;
            }
            $monthRelease = $this->lang->line('month_' . date('m', strtotime($row['releasedate'])));
            $monthReleaseShort = $this->lang->line('month_' . date('m', strtotime($row['releasedate'])) . '_short');

            $copyDate = strtotime($row['releasedate'] . '-' . INTERVAL_KOPIJ);
            $monthCopy = $this->lang->line('month_' . date('m', ($copyDate)));
            $monthCopyShort = $this->lang->line('month_' . date('m', ($copyDate)) . '_short');

            $edition['release'] = 'Nr. ' . $row['number'] . ' verschijnt vanaf ' . date('d', strtotime($row['releasedate'])) . ' ' . $monthRelease;
            $edition['releaseShort'] = 'Nr. ' . $row['number'] . ' - ' . date('d', strtotime($row['releasedate'])) . ' ' . $monthReleaseShort;
            $edition['copy'] = date('d', $copyDate) . ' ' . $monthCopy . ' vóór 19.00 uur';
            $edition['copyShort'] = date('d', $copyDate) . ' ' . $monthCopyShort . ' vóór 19.00';

            $table[$year][] = $edition;
        }
        foreach ($years as $i => $year) {
            $editionOverview[$i]['year'] = $year;
            $editionOverview[$i]['editions'] = $table[$year];
        }

        return $this->twig->render('front/editionOverview', ['editionOverview' => $editionOverview]);
    }
    
    
    /**
     * Build contactForm
     * @return string
     */
    private function contactForm()
    {
        // Predefined subjects
        $s = $this->input->get('s');
        if ($s) {
            $subjects = [
                'tip'       => 'Tip voor de redactie',
                'torentje'  => 'Torentje plaatsen',
                'ad'        => 'Advertentie',
                'bezorg'    => 'Bezorging',
            ];
            $subject = $subjects[$s] ?? '';
        }
        
        $optionsName = [
            'name' => 'name',
            'id' => 'name',
            'class' => 'form-control',
            'placeholder' => $this->lang->line('name'),
            'data-validation' => 'length',
            'data-validation-length' => 'min3',
        ];
        $optionsEmail = [
            'name' => 'email',
            'id' => 'email',
            'class' => 'form-control',
            'placeholder' => $this->lang->line('email'),
            'data-validation' => 'email',
        ];
        $optionsSubject = [
            'name' => 'subject',
            'id' => 'subject',
            'class' => 'form-control',
            'value' => $subject ?? '',
            'placeholder' => $this->lang->line('subject'),
            'data-validation' => 'length',
            'data-validation-length' => 'min3',
        ];
        $optionsMessage = [
            'name' => 'message',
            'id' => 'message',
            'class' => 'form-control',
            'placeholder' => $this->lang->line('message_placeholder'),
            'data-validation' => 'length',
            'data-validation-length' => 'min10',
        ];
        $parseData = [
            'optionsName' => $optionsName,
            'optionsEmail' => $optionsEmail,
            'optionsMessage' => $optionsMessage,
            'optionsSubject' => $optionsSubject,
            'page' => current_url(),
        ];
        return $this->twig->render('front/contactForm', $parseData);
    }
    
    /**
     * Send contactform
     */
    public function contactSend()
    {
        $data = $this->input->post(NULL, TRUE);

        // Check for valid email
        if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
            // Email not valid
            $this->session->set_flashdata('data', $data);
            $this->session->set_flashdata('message_error', $this->lang->line('error_email_not_valid'));
            redirect($data['page'] ?? 'Home');
        } else {
            // Build plain text email
            $mailBody = 'Datum/tijd: ' . date('Y-m-d H:i:s') . PHP_EOL;
            $mailBody .= 'Email: ' . strtolower($data['email']) . PHP_EOL;
            $mailBody .= 'Van pagina: ' . $data['page'] . PHP_EOL;
            $mailBody .= 'Naam: ' . $data['name'] . PHP_EOL . PHP_EOL;
            $mailBody .= 'Bericht:' . PHP_EOL . PHP_EOL;
            $mailBody .= $data['message'];

            $mailTo = trim(EMAIL_INFO);
            $mailHeaders = 'From:' . APPLICATION_NAME.' <'.EMAIL_FROM.'>';
            $mailSubject = 'Contact formulier website: ' . $data['subject'];
            $result = mail($mailTo, $mailSubject, $mailBody, $mailHeaders);
            $debugger = [
                'mailTo' => $mailTo,
                'mailSubject' => $mailSubject,
                'mailBody' => $mailBody,
                'result' => $result,
            ];

            // Write log
            $this->Log_model->logAdd(__CLASS__ . '/' . __FUNCTION__, array('email' => strtolower($data['email']), 'ip' => $this->input->ip_address(), 'emaildebugger' => $debugger));

            $this->session->set_flashdata('message', $this->lang->line('contact_form_sent'));
            redirect($data['page'] ?? 'Home');
        }
    }
}