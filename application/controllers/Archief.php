<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Archief extends CI_Controller
{
    public function __construct() {
        parent::__construct();
        $this->load->model('Content_model');
        $this->load->model('Edition_model');
    }
    
    /**
     * List all editions from given year
     */
    public function index()
    {
        $headerVars = [
            'menu' => $this->header_builder->frontMenuGet('Archief'),
            'pageTitle' => 'Archief',
            ];
        $this->twig->display('front/header', $headerVars);
        $year = (int) $this->input->post('year');
        if (empty($year) || ($year > date('Y')) || ($year < 1967)) {
            // Somebody is fiddling with us or simply hasn't submitted yet
            $year = date('Y');
        }
        $parameters = new stdClass();
        $parameters->year = $year;
        $parameters->withFile = true;
        $editions = $this->Edition_model->editionGetOverview($parameters);
        
        if ($editions) {
            foreach($editions as $key => $edition) {
                $editions[$key]['releaseyear'] = $year - $this->Edition_model->firstYear;
            }
        }

        // We have no archive before 2012
        for ($i=2012; $i<=date('Y'); $i++) {
            $selectYears[$i] = $i;         
        }
        
        // Put out
        $parseData = [
            'pageTitle' => 'Archief',
            'year' => $year,
            'selectYears' => $selectYears,
            'editions'  => $editions,
        ];
        $this->twig->display('front/archive', $parseData);
        $this->twig->display('front/footer', ['select2' => true]);

    }
    
    
    /**
     * Search for given phrase, text-based
     */
    public function Zoeken()
    {
        // Always show form and results
        $search = $this->input->post('search');
        if (!empty($search)) {
            $searchResult = $this->doSearch($search);
            
            if ($searchResult) {
                foreach($searchResult as $key => $edition) {
                    $searchResult[$key]['releaseyear'] = date('Y', strtotime($edition['releasedate'])) - $this->Edition_model->firstYear;
                }
            }
        }

        $optionsSearch = [
            'id' => 'search',
            'name' => 'search',
            'value' => $search,
            'placeholder' => $this->lang->line('search'),
            'class' => 'form-control'
        ];
        
        // Now output
        $headerVars = [
            'menu' => $this->header_builder->frontMenuGet(),
            'pageTitle' => 'Zoeken in archief',
            ];
        $this->twig->display('front/header', $headerVars);
        

        $parseData = [
            'search' => $search,
            'searchResult' => $searchResult ?? '',
            'optionsSearch' => $optionsSearch,
        ];
        $this->twig->display('front/searchArchive', $parseData);
        $this->twig->display('front/footer', ['select2' => true]);
    }
    
    /**
     * Perform text-based search
     * @param string $phrase
     * @return array|boolean
     */
    private function doSearch($phrase) 
    {
        $look = strtolower($phrase);
        
        $dir = './uploads/editions/txt/';
        $files = glob($dir . '*.txt');
        foreach ($files as $filename) {
            $contents = strtolower(file_get_contents($filename));
            if (stripos($contents, $look) !== false) {
                $found[] = pathinfo($filename, PATHINFO_FILENAME);
            }
        }
        
        if (is_array($found)) {
            $parameters = new stdClass();
            $parameters->withFile = true;
            $parameters->releaseDates = $found;
            return $this->Edition_model->editionGetOverview($parameters);
        }
        return false;    
    }
    
    public function Fotogalerij()
    {
        $headerVars = [
            'menu' => $this->header_builder->frontMenuGet('Archief'),
            'pageTitle' => 'Fotogalerij',
        ];
        
        $parameters = new stdClass();
        $parameters->sort = 'id';
        $parameters->order = 'DESC';
        $galleries = $this->Content_model->galleryGetOverview($parameters);
        
        $this->twig->display('front/header', $headerVars);
        $this->twig->display('front/gallery', ['galleries' => $galleries]);
        $this->twig->display('front/footer');
    }
}