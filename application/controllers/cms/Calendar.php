<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Calendar extends CI_Controller
{

    /**
     * Manage calendar/agenda items
     * 
     * @author GB
     * @copyright 2018
     */
    public $menuId = 4;
    public $section = 'Agenda';
    protected $roleId;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Content_model');
        $this->roleId = (int) $this->session->userdata('roleId');
        if ($this->roleId < 20) {
            redirect('cms/login/logout');
        }
        $this->header_builder->controllerSet($this->menuId, $this->section);
    }


    public function index($var1 = NULL)
    {
        $this->twig->display('generic/header', $this->header_builder->headerVarsGet());

        // Show message
        if ($var1 == 'showSoftDeletes') {
            $extraMessages[] = ['content' => $this->lang->line('deleted_records_shown'), 'type' => 'warning', 'dismissable' => false];
        }

        $columns = array(
            array('name' => 'id', 'orderable' => 'true'),
            array('name' => 'date', 'orderable' => 'true', 'filter_type' => 'text'),
            array('name' => 'text', 'orderable' => 'true', 'filter_type' => 'text'),
        );

        $parsedata = array('message' => $this->session->parseFlashMessages(isset($extraMessages) ? $extraMessages : NULL), 'columns' => $columns);

        // Parse content
        $this->twig->display('generic/dataTable', $parsedata);

        // Parse footer
        $footerdata = array('data_table' => TRUE, 'columns' => $columns);

        $footerdata['ajax_url'] = site_url('cms/Calendar/calendarAjax');
        $footerdata['url_add'] = site_url('cms/Calendar/itemAdd');
        $footerdata['url_edit'] = site_url('cms/Calendar/itemEdit');
        $footerdata['url_hard_delete'] = site_url('cms/Calendar/itemHardDelete');    
        
        $this->twig->display('generic/footer', $footerdata);
    }
    
    /**
     * Echo JSON encoded overview
     */
    public function calendarAjax()
    {
        echo $this->data_tables->getDefaultOutput('Content_model', 'calendarGetOverview');
    }
    
    /**
     * Add form
     */
    public function itemAdd()
    {
        $this->twig->display('generic/header', $this->header_builder->headerVarsGet());
        
        $optionsDate = [
            'name' => 'date',
            'id' => 'date',
            'placeholder' => $this->lang->line('date'),
            'data-validation' => 'required',
            'class' => 'form-control datepicker'
        ];
        
        $optionsText = [
            'name' => 'text',
            'id' => 'text',
            'placeholder' => $this->lang->line('text'),
            'data-validation' => 'required',
            'class' => 'form-control'
        ];
        
        $parseData = [
            'message' => $this->session->parseFlashMessages(),
            'optionsDate' => $optionsDate,
            'optionsText' => $optionsText,
        ];
        $this->twig->display('content/calendarForm', $parseData);

        $footerdata = ['form_validator' => true, 'date_picker' => true];
        $this->twig->display('generic/footer', $footerdata);
    }
    
    /**
     * Edit form
     * @param int $id
     */
    public function itemEdit($id = 0)
    {
        $data = $this->Content_model->calendarItemGet(intval($id));
        if (empty($data)) {
            $this->session->set_flashdata('message_error', $this->lang->line('no_data_found'));
            redirect('cms/' . __CLASS__);
        }
        $this->twig->display('generic/header', $this->header_builder->headerVarsGet());

        $optionsDate = [
            'name' => 'date',
            'id' => 'date',
            'value' => date('Y-m-d', strtotime($data['date'])),
            'placeholder' => $this->lang->line('date'),
            'data-validation' => 'required',
            'class' => 'form-control datepicker'
        ];
        
        $optionsText = [
            'name' => 'text',
            'id' => 'text',
            'value' => preg_replace('/\<br(\s*)?\/?\>/i', '', $data['text']),
            'placeholder' => $this->lang->line('text'),
            'data-validation' => 'required',
            'class' => 'form-control'
        ];

        $parseData = [
            'message' => $this->session->parseFlashMessages(),
            'id' => $id,
            'torentje' => $data,
            'optionsDate' => $optionsDate,
            'optionsText' => $optionsText,
        ];
        $this->twig->display('content/calendarForm', $parseData);
        
        $footerdata = ['form_validator' => true, 'date_picker' => true];
        $this->twig->display('generic/footer', $footerdata);
    }

    /**
     * Store torentje
     */
    public function calendarItemSave() {
        $data = $this->input->post(NULL, TRUE);
        
        // Set validation rules
        if (isset($data['id'])) {
            $this->form_validation->set_rules('id', 'id', 'trim|required');
        }
        $this->form_validation->set_rules('date', 'date', 'trim|required');
        $this->form_validation->set_rules('text', 'text', 'trim|required');

        // Validate data
        if ($this->form_validation->run() == FALSE) {
            // Return to previous edition
            if (isset($data['id'])) {
                $this->session->set_flashdata('message_error', $this->lang->line('edit_not_successfull') . validation_errors());
                redirect('cms/Calendar/itemEdit');
            } else {
                $this->session->set_flashdata('message_error', $this->lang->line('add_not_successfull') . validation_errors());
                redirect('cms/Calendar/itemAdd');
            }
        } 
        $store = [
            'id' => isset($data['id']) ? $data['id'] : null,
            'date' => date('Y-m-d', strtotime($data['date'])),
            'text' => nl2br($data['text']),
        ];
        $id = $this->Content_model->calendarItemStore($store);
        if ($id) {
            $this->Log_model->logAdd(__CLASS__ . '/' . __FUNCTION__, $store);
            $this->session->set_flashdata('message', $this->lang->line('store_success'));
        } else {
            $this->session->set_flashdata('message_error', $this->lang->line('store_not_success'));            
        }

        if (isset($data['submit_done'])) {
            redirect('cms/Calendar');
        } else {
            redirect('cms/Calendar/itemEdit/' . $id);
        }
       
    }
    
    public function import()
    {
        die('disabled');
//        $file = fopen(UPLOAD_DIR . 'tmp/kalender.csv', 'r');
//        $i = 1;
//        while( ($row = fgetcsv($file, 1000, ';', '"')) != false ) {    
//            if ($i > 1) {
//                $date = $row[1] . '-' . $row[2] . '-'. $row[3];
//                $store = [
//                    'id' =>  null,
//                    'date' => date('Y-m-d', strtotime($date)),
//                    'text' => nl2br($row[4]),
//                ];
//                $this->Content_model->calendarItemStore($store);
//            }
//            $i++;
//        }
//        var_dump($i);
    }


    /**
     * Hard delete calendar item
     * @return JSON response
     */
    public function itemHardDelete()
    {
        $id = $this->input->post('id');
        if (empty($id)) {
            return $this->data_tables->jsonSoftDelResponse('missingParameter');
        }
        
        $result = $this->Generic_model->hardDelete(TABLE_CALENDAR, $id);
        
        if ($result) {
            $this->Log_model->logAdd(__CLASS__ . '/' . __FUNCTION__, $id);
            return $this->data_tables->jsonSoftDelResponse('success');
        }
        return $this->data_tables->jsonSoftDelResponse('error');
    }
// Class and file ends here.
}
