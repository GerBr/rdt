<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin extends CI_Controller
{

    /**
     *
     * Admin actions: log and user management
     * @author GB
     * @copyright 2018
     */

    public $menuId = 2;
    protected $userId;
    protected $roleId;

    public function __construct()
    {

        parent::__construct();
        $this->header_builder->controllerSet($this->menuId);
        $this->userId = (int) $this->session->userdata('id');
        $this->roleId = (int) $this->session->userdata('roleId');

        // Check if user is autorised to be here
        if ($this->roleId < 30) {
            redirect('cms/login/logout');
        }
        
        $this->load->model('Role_model');
    }

    /**
     * Admin home
     */
    public function index()
    {
        redirect('cms/Home');

    }

    public function userAsync($var1 = NULL)
    {
        $this->twig->display('generic/header', $this->header_builder->headerVarsGet());

        // Show message
        if ($var1 == 'showSoftDeletes') {
            $extraMessages[] = ['content' => $this->lang->line('deleted_records_shown'), 'type' => 'warning', 'dismissable' => false];
        }

        $columns = array(
            array('name' => 'id', 'orderable' => 'true'),
            array('name' => 'name', 'orderable' => 'true', 'filter_type' => 'text'),
            array('name' => 'email', 'orderable' => 'true'),
            array('name' => 'role', 'orderable' => 'true'),
            array('name' => 'last_login', 'orderable' => 'true'),
            );

        $parsedata = array('message' => $this->session->parseFlashMessages(isset($extraMessages) ? $extraMessages : NULL), 'columns' => $columns);

        // Parse content
        $this->twig->display('generic/dataTable', $parsedata);

        // Parse footer
        $footerdata = array('data_table' => TRUE, 'columns' => $columns);

        $footerdata['ajax_url'] = site_url('cms/Admin/userAjax');

        
        if ($var1 == 'showSoftDeletes') {
            $footerdata['var1'] = 'showSoftDeletes';
            $footerdata['url_back'] = site_url('cms/Admin/userAsync');            
            $footerdata['url_undelete'] = site_url('cms/Admin/userSoftRestore');
        } elseif ($this->roleId >= 30) {
            $footerdata['url_add'] = site_url('cms/Admin/userAdd');
            $footerdata['url_edit'] = site_url('cms/Admin/userEdit');
            $footerdata['url_delete'] = site_url('cms/Admin/userSoftDelete');
            $footerdata['url_deleted'] = site_url('cms/Admin/userAsync/showSoftDeletes');
        }
        $this->twig->display('generic/footer', $footerdata);
    }
    
    public function userAjax()
    {
        echo $this->data_tables->getDefaultOutput('User_model', 'userGetOverview');
    }
    
    /**
     * Add form
     */
    public function userAdd()
    {
        $this->twig->display('generic/header', $this->header_builder->headerVarsGet());
        
        $optionsEmail = [
            'name' => 'email',
            'id' => 'email',
            'placeholder' => $this->lang->line('email'),
            'data-validation' => 'required|email',
            'class' => 'form-control'
        ];
        
        $optionsName = [
            'name' => 'name',
            'id' => 'name',
            'placeholder' => $this->lang->line('name'),
            'data-validation' => 'required',
            'class' => 'form-control'
        ];
        
        $parameters = new stdClass();
        $parameters->dropdown = true;
        $allRoles = $this->Role_model->roleGet($parameters);
        
        $parseData = [
            'message' => $this->session->parseFlashMessages(),
            'optionsEmail' => $optionsEmail,
            'optionsName' => $optionsName,
            'allRoles' => $allRoles,
        ];
        $this->twig->display('admin/userForm', $parseData);

        $footerdata = ['form_validator' => true, 'select2' => true,];
        $this->twig->display('generic/footer', $footerdata);
    }
    
    /**
     * Edit form
     * @param int $id
     */
    public function userEdit($id = 0)
    {
        $parameters = new stdClass();
        $parameters->variable = 'id';
        $parameters->value = intval($id);
        $data = $this->User_model->userGet($parameters);
        if (empty($data)) {
            $this->session->set_flashdata('message_error', $this->lang->line('no_data_found'));
            redirect('cms/' . __CLASS__);
        }
        $this->twig->display('generic/header', $this->header_builder->headerVarsGet());

       $optionsEmail = [
            'name' => 'email',
            'id' => 'email',
            'placeholder' => $this->lang->line('email'),
            'data-validation' => 'required|email',
            'class' => 'form-control',
            'value' => $data['email'],
        ];
        
        $optionsName = [
            'name' => 'name',
            'id' => 'name',
            'placeholder' => $this->lang->line('name'),
            'data-validation' => 'required',
            'class' => 'form-control',
            'value' => $data['name'],
        ];
        
        $parameters = new stdClass();
        $parameters->dropdown = true;
        $allRoles = $this->Role_model->roleGet($parameters);
        
        $parseData = [
            'message' => $this->session->parseFlashMessages(),
            'optionsEmail' => $optionsEmail,
            'optionsName' => $optionsName,
            'allRoles' => $allRoles,
            'user' => $data,
            'id' => $id,
        ];
        $this->twig->display('admin/userForm', $parseData);

        $footerdata = ['form_validator' => true, 'select2' => true,];
        $this->twig->display('generic/footer', $footerdata);
    }

    /**
     * Store item
     */
    public function userSave() {
        $data = $this->input->post(NULL, TRUE);
        
        // Set validation rules
        if (isset($data['id'])) {
            $this->form_validation->set_rules('id', 'id', 'trim|required');
        }
        $this->form_validation->set_rules('name', 'name', 'trim|required');
        $this->form_validation->set_rules('role_id', 'role', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules(
            'email', 'email', array('trim', 'required', 'valid_email', array('is_unique',
                function($str) use ($data) {
                    return $this->User_model->emailUnique($str, (isset($data['id']) ? $data['id'] : 0));
                })
            )
        );

        // Validate data
        if ($this->form_validation->run() == FALSE) {
            // Return to previous edition
            if (isset($data['id'])) {
                $this->session->set_flashdata('message_error', $this->lang->line('edit_not_successfull') . validation_errors());
                redirect('cms/Admin/userEdit');
            } else {
                $this->session->set_flashdata('message_error', $this->lang->line('add_not_successfull') . validation_errors());
                redirect('cms/Admin/userAdd');
            }
        } 
        $store = [
            'id' => isset($data['id']) ? $data['id'] : null,
            'email' => $data['email'],
            'name' => $data['name'],
            'role_id' => $data['role_id'],
        ];
        
        $id = $this->User_model->userStore($store);
        if ($id) {
            $this->Log_model->logAdd(__CLASS__ . '/' . __FUNCTION__, $store);
            $this->session->set_flashdata('message', $this->lang->line('store_success'));
        } else {
            $this->session->set_flashdata('message_error', $this->lang->line('store_not_success'));            
        }

        if (isset($data['submit_done'])) {
            redirect('cms/Admin/userAsync');
        } else {
            redirect('cms/Admin/userEdit/' . $id);
        }
    }
  
    /**
     * Soft delete user
     * @return JSON response
     */
    public function userSoftDelete()
    {
        $id = $this->input->post('id');
        if (empty($id)) {
            return $this->data_tables->jsonSoftDelResponse('missingParameter');
        }
        if ((int) $id == (int) $this->userId) {
            return $this->data_tables->jsonSoftDelResponse('error', $this->lang->line('error_cannot_delete_self'));
        }
        
        $result = $this->Generic_model->setSoftDeleteStatus(TABLE_USERS, $id, 1);
        
        if ($result) {
            $this->Log_model->logAdd(__CLASS__ . '/' . __FUNCTION__, $id);
            return $this->data_tables->jsonSoftDelResponse('success');
        }
        return $this->data_tables->jsonSoftDelResponse('error');
    }
    
    /**
     * Soft restore user
     * @return JSON response
     */
    public function userSoftRestore()
    {
        $id = $this->input->post('id');
        if (empty($id)) {
            return $this->data_tables->jsonSoftDelResponse('missingParameter');
        }
        
        $result = $this->Generic_model->setSoftDeleteStatus(TABLE_USERS, $id, 0);
        
        if ($result) {
            $this->Log_model->logAdd(__CLASS__ . '/' . __FUNCTION__, $id);
            return $this->data_tables->jsonSoftDelResponse('success');
        }
        return $this->data_tables->jsonSoftDelResponse('error');
    }
    
    public function logAsync()
{
        $this->twig->display('generic/header', $this->header_builder->headerVarsGet());

        $columns = array(
            array('name' => 'id', 'orderable' => 'true'),
            array('name' => 'name', 'orderable' => 'true', 'filter_type' => 'text'),
            array('name' => 'action', 'orderable' => 'true', 'filter_type' => 'text'),
            array('name' => 'created_at', 'orderable' => 'true', 'filter_type' => 'text'),
        );

        $parsedata = array('message' => $this->session->parseFlashMessages(isset($extraMessages) ? $extraMessages : NULL), 'columns' => $columns);

        // Parse content
        $this->twig->display('generic/dataTable', $parsedata);

        // Parse footer
        $footerdata = array('data_table' => TRUE, 'columns' => $columns);

        $footerdata['ajax_url'] = site_url('cms/Admin/logAjax');
        $footerdata['url_view'] = site_url('cms/Admin/logView');
        $this->twig->display('generic/footer', $footerdata);
    }
    
    /**
     * Echo JSON encoded overview
     */
    public function logAjax()
    {
        echo $this->data_tables->getDefaultOutput('Log_model', 'logGetOverview');
    }
    
    public function logView($id)
    {        
        $data = $this->Log_model->logSingleGet($id);
        if (empty($data)) {
            $this->session->set_flashdata('message_error', $this->lang->line('no_data_found'));
            redirect('cms/Admin/LogAsync');
        }
        $this->twig->display('generic/header', $this->header_builder->headerVarsGet());
        
        // Expand info        
        $details = json_decode($data['json'], true);
        
        $parseData = [
            'message' => $this->session->parseFlashMessages(),
            'log' => $data,
            'details' => var_export($details, true),
        ];
        $this->twig->display('admin/logDetails', $parseData);
        $this->twig->display('generic/footer');
    }

// Class and file ends here.
}
