<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Torentjes extends CI_Controller
{

    /**
     * Manage main content
     * 
     * @author GB
     * @copyright 2018
     */
    public $menuId = 4;
    public $section = 'Torentjes';
    protected $userId;
    protected $roleId;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Content_model');
        $this->load->model('Edition_model');
        $this->load->model('Torentjes_model');
        $this->userId = (int) $this->session->userdata('id');
        $this->roleId = (int) $this->session->userdata('roleId');
        if ($this->roleId < 20) {
            redirect('cms/login/logout');
        }
        $this->header_builder->controllerSet($this->menuId, $this->section);
    }


    public function index($var1 = NULL)
    {
        $this->twig->display('generic/header', $this->header_builder->headerVarsGet());

        // Show message
        if ($var1 == 'showSoftDeletes') {
            $extraMessages[] = ['content' => $this->lang->line('deleted_records_shown'), 'type' => 'warning', 'dismissable' => false];
        }

        $columns = array(
            array('name' => 'id', 'orderable' => 'true'),
            array('name' => 'title', 'orderable' => 'true', 'filter_type' => 'text'),
            array('name' => 'edition', 'orderable' => 'true', 'filter_type' => 'text'),
            array('name' => 'end_date', 'orderable' => 'true', 'filter_type' => 'text'),
        );

        $parsedata = array('message' => $this->session->parseFlashMessages(isset($extraMessages) ? $extraMessages : NULL), 'columns' => $columns);

        // Parse content
        $this->twig->display('generic/dataTable', $parsedata);

        // Parse footer
        $footerdata = array('data_table' => TRUE, 'columns' => $columns);

        $footerdata['ajax_url'] = site_url('cms/Torentjes/torentjesAjax');
        $footerdata['url_add'] = site_url('cms/Torentjes/torentjeAdd');
        $footerdata['url_edit'] = site_url('cms/Torentjes/torentjeEdit');
        $footerdata['url_hard_delete'] = site_url('cms/Torentjes/torentjeHardDelete');            
        
        $this->twig->display('generic/footer', $footerdata);
    }
    
    /**
     * Echo JSON encoded overview
     */
    public function torentjesAjax()
    {
        // Count records
        $this->data_tables->setParams(['countAllRows' => true, 'calcEdition' => true]);
        $count = (int) $this->Torentjes_model->torentjeGetOverview($this->data_tables->params)[0]['counted'];
        unset($this->data_tables->params->countAllRows);

        // Get rows and return JSON
        $rows = $this->Torentjes_model->torentjeGetOverview($this->data_tables->params);
        echo $this->data_tables->getJson($rows, $count);
    }
    
    /**
     * Add form
     */
    public function torentjeAdd()
    {
        $this->twig->display('generic/header', $this->header_builder->headerVarsGet());
        
        $optionsTitle = [
            'name' => 'title',
            'id' => 'title',
            'placeholder' => $this->lang->line('title'),
            'data-validation' => 'required',
            'class' => 'form-control'
        ];
        
        $optionsText = [
            'name' => 'text',
            'id' => 'text',
            'placeholder' => $this->lang->line('text'),
            'data-validation' => 'required',
            'class' => 'form-control'
        ];
        
        $parameters = new stdClass();
        $parameters->varname = 'releasedate >';
        $parameters->value= date('Y-m-d', strtotime('-2 month'));
        $parameters->dropdown = true;
        $allEditions = $this->Edition_model->editionGetOverview($parameters);
        
        $parseData = [
            'message' => $this->session->parseFlashMessages(),
            'optionsTitle' => $optionsTitle,
            'optionsText' => $optionsText,
            'allEditions' => $allEditions,
        ];
        $this->twig->display('content/torentjeForm', $parseData);

        $footerdata = ['form_validator' => true, 'select2' => true, 'date_picker' => true];
        $this->twig->display('generic/footer', $footerdata);
    }
    
    /**
     * Edit form
     * @param int $id
     */
    public function torentjeEdit($id = 0)
    {
        $data = $this->Torentjes_model->torentjeGet(intval($id));
        if (empty($data)) {
            $this->session->set_flashdata('message_error', $this->lang->line('no_data_found'));
            redirect('cms/' . __CLASS__);
        }
        $this->twig->display('generic/header', $this->header_builder->headerVarsGet());

        $optionsTitle = [
            'name' => 'title',
            'id' => 'title',
            'value' => $data['title'],
            'placeholder' => $this->lang->line('title'),
            'data-validation' => 'required',
            'class' => 'form-control'
        ];
        
        $optionsText = [
            'name' => 'text',
            'id' => 'text',
            'value' => preg_replace('/\<br(\s*)?\/?\>/i', '', $data['text']),
            'placeholder' => $this->lang->line('text'),
            'data-validation' => 'required',
            'class' => 'form-control'
        ];
        
        $parameters = new stdClass();
        $parameters->varname = 'releasedate >';
        $parameters->value= date('Y-m-d', strtotime('-2 month'));
        $parameters->dropdown = true;
        $allEditions = $this->Edition_model->editionGetOverview($parameters);
        
        $parseData = [
            'message' => $this->session->parseFlashMessages(),
            'id' => $id,
            'torentje' => $data,
            'optionsTitle' => $optionsTitle,
            'optionsText' => $optionsText,
            'allEditions' => $allEditions,
        ];
        $this->twig->display('content/torentjeForm', $parseData);
        
        $footerdata = ['form_validator' => true, 'select2' => true, 'date_picker' => true];
        $this->twig->display('generic/footer', $footerdata);
    }

    /**
     * Store torentje
     */
    public function torentjeSave() {
        $data = $this->input->post(NULL, TRUE);
        
        // Set validation rules
        if (isset($data['id'])) {
            $this->form_validation->set_rules('id', 'id', 'trim|required');
        }
        $this->form_validation->set_rules('title', 'title', 'trim|required');
        $this->form_validation->set_rules('text', 'text', 'trim|required');
        $this->form_validation->set_rules('edition', 'edition', 'trim|numeric|required');

        // Validate data
        if ($this->form_validation->run() == FALSE) {
            // Return to previous edition
            if (isset($data['id'])) {
                $this->session->set_flashdata('message_error', $this->lang->line('edit_not_successfull') . validation_errors());
                redirect('cms/Torentjes/torentjeEdit');
            } else {
                $this->session->set_flashdata('message_error', $this->lang->line('add_not_successfull') . validation_errors());
                redirect('cms/Torentjes/torentjeAdd');
            }
        } 
        $store = [
            'id' => isset($data['id']) ? $data['id'] : null,
            'edition_id' => $data['edition'],
            'title' => $data['title'],
            'text' => nl2br($data['text']),
        ];
        $id = $this->Torentjes_model->torentjeStore($store);
        if ($id) {
            $this->Log_model->logAdd(__CLASS__ . '/' . __FUNCTION__, $store);
            $this->session->set_flashdata('message', $this->lang->line('store_success'));
        } else {
            $this->session->set_flashdata('message_error', $this->lang->line('store_not_success'));            
        }

        if (isset($data['submit_done'])) {
            redirect('cms/Torentjes');
        } else {
            redirect('cms/Torentjes/torentjeEdit/' . $id);
        }
       
    }
    
    /**
     * Hard delete a mini ad
     * @return JSON response
     */
    public function torentjeHardDelete()
    {
        $id = $this->input->post('id');
        if (empty($id)) {
            return $this->data_tables->jsonSoftDelResponse('missingParameter');
        }
        
        $result = $this->Generic_model->hardDelete(TABLE_TORENTJES, $id);
        
        if ($result) {
            $this->Log_model->logAdd(__CLASS__ . '/' . __FUNCTION__, $id);
            return $this->data_tables->jsonSoftDelResponse('success');
        }
        return $this->data_tables->jsonSoftDelResponse('error');
    }
    
// Class and file ends here.
}
