<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login extends CI_Controller
{

    /**
     * Standard login class
     *
     * @author GB
     * @copyright 2018
     *
     */
    public $page = 'login';

    public function __construct()
    {
        parent::__construct();
        // Set default lang if not set in session
        if ($this->session->userdata('siteLang') == NULL) {
            // Store user data in session
            $this->session->set_userdata('siteLang', 'en');
        }
        
        $this->load->model('Generic_model');
        $this->header_builder->controllerSet(1, $this->page);

        $this->load->model('Role_model');
        $this->load->helper('captcha');
        $this->load->library('email');
        $this->load->helper('cookie');
        $this->load->library('form_validation');
    }

    /**
     * Show login form
     */
    public function index($logout_success = FALSE)
    {
        /**
         * Do lost pass cleanup here
         * Since this is the default page, any user indicated as inactive due
         * to a lost pass request is now cleared for login when token is expired
         * No cron needed
         */
        $this->User_model->tokensDelete();

        // Get flash data
        $email = $this->session->flashdata('email');
        $extraMessages = array();
        if ($logout_success) {
            $extraMessages[] = ['content' => $this->lang->line('logout_success'), 'type' => 'info'];
        }
        // Build header
        $header = [
            'heading' => 'Inloggen',
            'formTarget' => 'cms/login/loginProcess',
            'message' => $this->session->parseFlashMessages($extraMessages),
            'warning' => MAINTENANCE_MODE ? '<div class="alert alert-warning">' . $this->lang->line('login_maintenance') . '</div>' : NULL,
            ];

        $this->twig->display('login/loginHeader', $this->header_builder->headerVarsGet($header));

        $optionsEmail = array('name' => 'email',
            'id' => 'email',
            'class' => 'form-control',
            'placeholder' => $this->lang->line('login_placeholder_email'),
            'data-validation' => 'email',
            'value' => (empty($email) ? '' : $email));

        $optionsPassword = array('name' => 'password',
            'id' => 'password',
            'class' => 'form-control',
            'placeholder' => $this->lang->line('login_placeholder_password'),
            'data-validation' => 'length',
            'data-validation-length' => 'min4',
            'value' => '');

        $formData = array(
            'optionsEmail' => $optionsEmail,
            'optionsPassword' => $optionsPassword,
        );

        $this->twig->display('login/loginIndex', $formData);
        $footerdata = array('form_validator' => TRUE);
        $this->twig->display('login/loginFooter', $footerdata);
    }

    /**
     * Process login data
     */
    public function loginProcess()
    {
        // Get the POST data with xss filter
        $email = $this->input->post('email', TRUE);
        $password = $this->input->post('password', TRUE);

        // Get user 
        $user = $this->User_model->userLoginGet($email, hash('sha256', PASSWORD_SALT . $password));
        //var_dump($user);
        if ($user) {
            // Update user last_login
            $data = array(
                'id' => $user['id'],
                'last_login' => date('Y-m-d H:i:s'),
                );

            $this->User_model->userUpdate($data);

            // Get role name
            $parameters = new stdClass();
            $parameters->variable = 'id';
            $parameters->value = $user['role_id'];
            $role = $this->Role_model->roleGet($parameters);
            $role_name = ucfirst($role[0]['name']);
          
            // Store user data in session
            $userData = array(
                'id' => (int) $user['id'],
                'userName' => trim($user['name']),
                'userEmail' => $user['email'],
                'loggedIn' => TRUE,
                'roleId' => (int) $user['role_id'],
                'roleName' => $role_name,
            );

            //var_dump($userData);
            // Maintenance mode?
            if (MAINTENANCE_MODE && $user['role_id'] < 30) {
                $this->Log_model->logAdd('maintenance_mode', $userData);
                $this->session->set_flashdata('message_error', $this->lang->line('maintenance_mode'));
                redirect('cms/login/index');
            } else {
                $this->session->set_userdata($userData);
                $this->Log_model->logAdd('valid_login', array('user_id' => $user['id'], 'ip' => $this->session->userdata('ip_address')), $user['id']);
                $this->session->set_flashdata('message', $this->lang->line('login_successfull'));
                redirect('cms/Home/index');
            }
        } else {
            // Wrong login, write log
            $this->Log_model->logAdd(__CLASS__ . '/' . __FUNCTION__, array('email' => $email, 'ip' => $this->input->ip_address(), 'emaildebugger' => $this->email->print_debugger()));
            $this->session->set_flashdata('message_error', $this->lang->line('login_invalid'));
            $this->session->set_flashdata('email', $email);
            redirect('cms/login/index');
        }
    }

    /**
     * Logout
     * Destroy session
     */
    public function logout()
    {
        $this->session->sess_destroy();
        redirect('cms/login/index/logout_success');
    }

    /**
     * Request new password
     */
    public function forgot()
    {
        // If this is not the case, create a new dir instead:
        $sCaptchaDir = FCPATH . "captcha/";

        if (!is_dir($sCaptchaDir)) {
            if (!mkdir($sCaptchaDir, 0755, TRUE)) {
                $sErrorMessage = __CLASS__ . "  - Directory couldn't be created";
                // Write output to log file:
                log_message('error', $sErrorMessage);
            };
        }
        // Create captcha to prevent bot requests
        $vals = array(
            'img_path' => './captcha/',
            'img_url' => base_url('captcha') . '/',
            'font_path' => './system/fonts/texb.ttf',
            'img_width' => 300,
            'img_height' => 70,
            'font_size' => 24,
            'expiration' => 7200,
            'pool' => '23456789abcdefkmnpqrstuxyzABCDEFGHKMNPRSTUVWXYZ',
        );

        $cap = create_captcha($vals);

        $capdata = array(
            'captcha_time' => $cap['time'],
            'ip_address' => $this->input->ip_address(),
            'word' => $cap['word']
        );

        // Write captcha
        $this->User_model->captchaCreate($capdata);

        $data = $this->session->flashdata('postdata');

        // Build page
        $header = [
            'message' => $this->session->parseFlashMessages(),
            'warning' => MAINTENANCE_MODE ? '<div class="alert alert-warning">' . $this->lang->line('login_maintenance') . '</div>' : NULL,
            'heading' => 'Wachtwoord vergeten',
            'formTarget' => 'cms/login/forgotProcess',
            ];
        $this->twig->display('login/loginHeader', $this->header_builder->headerVarsGet($header));

        $optionsEmail = array('name' => 'email',
            'id' => 'email',
            'class' => 'form-control',
            'placeholder' => $this->lang->line('login_placeholder_email'),
            'data-validation' => 'email',
            'value' => (empty($data['email']) ? '' : strtolower($data['email'])));

        $optionsPassword = array('name' => 'captcha',
            'id' => 'captcha',
            'class' => 'form-control',
            'placeholder' => $this->lang->line('login_placeholder_captcha'),
            'data-validation' => 'length',
            'data-validation-length' => 'min4',
            'value' => '');

        $formData = array(
            'captchaImage' => $this->lang->line('login_captcha') . ' ' . $cap['image'] . ' <a href="' . site_url('cms/login/forgot') . '"><span class="fas fa-redo"></span></a>',
            'optionsEmail' => $optionsEmail,
            'optionsPassword' => $optionsPassword,
        );

        $this->twig->display('login/loginForgot', $formData);
        $footerdata = array('form_validator' => TRUE);
        $this->twig->display('login/loginFooter', $footerdata);
    }

    /**
     * Handle new password request
     */
    public function forgotProcess()
    {
        $form_data = $this->input->post(NULL, TRUE);

        $parameters = new stdClass();
        $parameters->variable = 'email';
        $parameters->value = trim(strtolower($form_data['email']));
        $user = $this->User_model->userGet($parameters);

        // Check for valid email
        if (!filter_var($form_data['email'], FILTER_VALIDATE_EMAIL)) {
            // Email not valid
            $this->session->set_flashdata('postdata', $form_data);
            $this->session->set_flashdata('message_error', $this->lang->line('error_email_not_valid'));
            redirect('cms/login/forgot');
        }

        // Login name may exist, but also check some other stuff
        if (($user) && (trim(strtolower($form_data['email'])) == trim(strtolower($user['email'])))) {

            // Check if captcha is valid
            // First, delete old captchas
            $expiration = time() - 86400; // 24 hour limit
            $this->User_model->captchaDelete($expiration);

            $data = new stdClass();
            $data->word = $form_data['captcha'];
            $data->ipAddress = $this->input->ip_address();
            $data->captchaTime = $expiration;

            $captcha = $this->User_model->captchaCheck($data);

            // Then see if a captcha exists:
            if ($captcha['0']['count'] == 0) {

                // Captcha not valid
                // Set postdata
                $this->session->set_flashdata('postdata', $form_data);
                $this->session->set_flashdata('message_error', $this->lang->line('error_captcha_not_valid'));
                redirect('cms/login/forgot');
            } else {
                $token = hash('sha256', $user['id'] . trim($user['email']) . time());

                // Update user record in DB
                $update_data = array(
                    'token' => $token,
                    'token_end' => date('Y-m-d H:i:s', time() + TOKEN_EXPIRATION_TIME),
                    'id' => $user['id'],
                );

                $this->User_model->userUpdate($update_data);
                // Build plain text email
                $mailTo = trim($user['email']);
                $mailHeaders = 'From:' . APPLICATION_NAME.' <'.EMAIL_FROM.'>';
                $mailSubject = 'Rond de Toren CMS Wachtwoord vergeten';
                $mailBody = $this->lang->line('login_forgot_email_reset_intro') . PHP_EOL . PHP_EOL;
                $mailBody .= site_url('cms/login/newPass?t=' . $token) . PHP_EOL . PHP_EOL;
                $mailBody .= $this->lang->line('login_forgot_email_reset_greeting');  
                $result = mail($mailTo, $mailSubject, $mailBody, $mailHeaders);
                $debugger = [
                    '$mailTo' => $mailTo,
                    '$mailSubject' => $mailSubject,
                    '$mailBody' => $mailBody,
                    '$result' => $result,
                ];

                // Write log
                $this->Log_model->logAdd(__CLASS__ . '/' . __FUNCTION__, array('user_id' => $user['id'], 'ip' => $this->session->userdata('ip_address'), 'emaildebugger' => $debugger), $user['id']);
                $this->session->set_flashdata('message', $this->lang->line('login_forgot_email_sent'));
                redirect('cms/login/index');
            }
        } else {

            // Email not found
            // Set postdata
            $this->session->set_flashdata('postdata', $form_data);
            $this->session->set_flashdata('message_error', $this->lang->line('error_email_not_found'));
            redirect('cms/login');
        }
    }

    /**
     * Let user enter a new password
     * when he has forgotten the old one
     */
    public function newPass()
    {
        // We need a token
        $token = $this->input->get('t');
        if (!$token) {
            $this->session->set_flashdata('message_error', $this->lang->line('error_no_access'));
            redirect('cms/login');
        }

        $parameters = new stdClass();
        $parameters->variable = 'token';
        $parameters->value = $token;
        $userData = $this->User_model->userGet($parameters);

        // Build header
        $header = [
            'heading' => 'Nieuw wachtwoord',
            'formTarget' => 'cms/login/newPassSave',
            'message' => $this->session->parseFlashMessages(),
            'warning' => MAINTENANCE_MODE ? '<div class="alert alert-warning">' . $this->lang->line('login_maintenance') . '</div>' : NULL,
        ];
        $this->twig->display('login/loginHeader', $this->header_builder->headerVarsGet($header));

        // is it a valid token?
        if (($userData) && (strtotime($userData['token_end']) > time() )) {
            $optionsPassword = array('name' => 'password_confirmation',
                'id' => 'password',
                'class' => 'form-control',
                'placeholder' => $this->lang->line('login_placeholder_password'),
                'data-validation' => 'strength',
                'data-validation-strength' => '2',
                'value' => '');

            $optionsPasswordConfirm = array('name' => 'password',
                'id' => 'password_confirm',
                'class' => 'form-control',
                'placeholder' => $this->lang->line('login_placeholder_password_confirm'),
                'data-validation' => 'confirmation',
                'value' => '');

            $formData = array(
                'message' => $this->session->parseFlashMessages(),
                'warning' => MAINTENANCE_MODE ? '<div class="alert alert-warning">' . $this->lang->line('login_maintenance') . '</div>' : NULL,
                'token' => $token,
                'optionsPassword' => $optionsPassword,
                'optionsPasswordConfirm' => $optionsPasswordConfirm,
            );

            $this->twig->display('login/newPass', $formData);
            $footerdata = array('form_validator' => TRUE);
            $this->twig->display('login/loginFooter', $footerdata);
        } else {
            $this->session->set_flashdata('message_error', $this->lang->line('login_link_invalid'));
            redirect('cms/login');
        }
    }

    /**
     * Save new password
     */
    public function newPassSave()
    {

        $post_data = $this->input->post(NULL, TRUE);

        $parameters = new stdClass();
        $parameters->variable = 'token';
        $parameters->value = $post_data['token'];
        $user = $this->User_model->userGet($parameters);

        if ($user) {
            // update user
            $update_data = array(
                'password' => hash('sha256', PASSWORD_SALT . $post_data['password']),
                'token_end' => NULL,
                'token' => NULL,
                'id' => $user['id']
                );

            $this->User_model->userUpdate($update_data);

            // log
            unset($update_data['token_end'], $update_data['token']);
            $update_data['ip'] = $this->session->userdata('ip_address');
            $this->Log_model->logAdd(__CLASS__ . '/' . __FUNCTION__, $update_data, $user['id']);

            // Succes, display message and return
            $this->session->set_flashdata('message', $this->lang->line('login_password_reset_success'));
            redirect('cms/login');
        } else {
            $this->session->set_flashdata('message_error', $this->lang->line('error_no_access'));
            redirect('cms/login');
        }
    }
// Class and file ends here.
}
