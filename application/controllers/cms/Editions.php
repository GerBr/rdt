<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Editions extends CI_Controller
{

    /**
     * Manage paper editions
     * 
     * @author GB
     * @copyright 2018
     */
    public $menuId = 5;
    protected $userId;
    protected $roleId;

    public function __construct()
    {
        parent::__construct();
        $this->lang->load('upload', 'nl');
        $this->load->library('pdfparser');
        $this->load->model('Edition_model');
        $this->load->model('News_model');
        $this->load->model('Torentjes_model');
        $this->userId = (int) $this->session->userdata('id');
        $this->roleId = (int) $this->session->userdata('roleId');
        if ($this->roleId < 10) {
            redirect('cms/login/logout');
        }
        $this->header_builder->controllerSet($this->menuId);
    }

    /**
     * Display Async table of editions
     */
    public function index($var1 = NULL)
    {
        $this->twig->display('generic/header', $this->header_builder->headerVarsGet());

        // Show message
        if ($var1 == 'showSoftDeletes') {
            $extraMessages[] = ['content' => $this->lang->line('deleted_records_shown'), 'type' => 'warning', 'dismissable' => false];
        }

        $columns = array(
            array('name' => 'id', 'orderable' => 'true'),
            array('name' => 'number', 'orderable' => 'true', 'filter_type' => 'text'),
            array('name' => 'releasedate', 'orderable' => 'true', 'filter_type' => 'text'),
        );

        $parsedata = array('message' => $this->session->parseFlashMessages(isset($extraMessages) ? $extraMessages : NULL), 'columns' => $columns);

        // Parse content
        $this->twig->display('generic/dataTable', $parsedata);

        // Parse footer
        $footerdata = array('data_table' => TRUE, 'columns' => $columns);

        $footerdata['ajax_url'] = site_url('cms/Editions/editionAjax');
        if ($var1 == 'showSoftDeletes') {
            $footerdata['var1'] = 'showSoftDeletes';
            $footerdata['url_back'] = site_url('cms/Editions');
            $footerdata['url_undelete'] = site_url('cms/Editions/EditionSoftRestore');
            $footerdata['url_hard_delete'] = site_url('cms/Editions/EditionHardDelete');
        } else {
            $footerdata['url_add'] = site_url('cms/Editions/editionAdd');
            $footerdata['url_edit'] = site_url('cms/Editions/editionEdit');
            $footerdata['url_delete'] = site_url('cms/Editions/editionSoftDelete');
            $footerdata['url_deleted'] = site_url('cms/Editions/index/showSoftDeletes');
        }
        
        $this->twig->display('generic/footer', $footerdata);
    }
    
    /**
     * Echo JSON encoded overview
     */
    public function editionAjax()
    {
        echo $this->data_tables->getDefaultOutput('Edition_model', 'editionGetOverview');
    }
    
    /**
     * Add form
     */
    public function editionAdd()
    {
        $this->twig->display('generic/header', $this->header_builder->headerVarsGet());
        
        $optionsNumber = [
            'name' => 'number',
            'id' => 'number',
            'type' => 'number',
            'placeholder' => $this->lang->line('number'),
            'data-validation' => 'number',
            'class' => 'form-control'
        ];

        $optionsReleaseDate = [
            'name' => 'releasedate',
            'id' => 'releasedate',
            'placeholder' => $this->lang->line('releasedate'),
            'data-validation' => 'required',
            'class' => 'form-control datepicker'
        ];
        
        $optionsFile = [
            'name' => 'userfile',
            'id' => 'userfile',
            'placeholder' => $this->lang->line('form_select_file'),
            'class' => 'custom-file-input'
        ];
        
        $parseData = [
            'message' => $this->session->parseFlashMessages(),
            'optionsNumber' => $optionsNumber,
            'optionsReleaseDate' => $optionsReleaseDate,
            'optionsFile' => $optionsFile,
        ];
        $this->twig->display('content/editionForm', $parseData);

        $footerdata = ['form_validator' => true, 'select2' => true, 'date_picker' => true, 'fileupload' => 'userfile'];
        $this->twig->display('generic/footer', $footerdata);
    }
    /**
     * Edit form
     * @param int $id
     */
    public function editionEdit($id = 0)
    {
        $data = $this->Edition_model->editionGet(intval($id));
        if (empty($data)) {
            $this->session->set_flashdata('message_error', $this->lang->line('no_data_found'));
            redirect('cms/' . __CLASS__);
        }
        $this->twig->display('generic/header', $this->header_builder->headerVarsGet());
        
        $optionsNumber = [
            'name' => 'number',
            'id' => 'number',
            'value' => $data['number'],
            'type' => 'number',
            'placeholder' => $this->lang->line('number'),
            'data-validation' => 'number',
            'class' => 'form-control'
        ];

        $optionsReleaseDate = [
            'name' => 'releasedate',
            'id' => 'releasedate',
            'value' => date('d-m-Y', strtotime($data['releasedate'])),
            'placeholder' => $this->lang->line('releasedate'),
            'data-validation' => 'required',
            'class' => 'form-control datepicker'
        ];

        $optionsFile = [
            'name' => 'userfile',
            'id' => 'userfile',
            'placeholder' => $this->lang->line('form_select_file'),
            'class' => 'custom-file-input'
        ];
        
        if (!empty($data['file'])) {
            $file = anchor('./uploads/editions/' .  $data['file'], $data['file']);
        }
        
        $parseData = [
            'message' => $this->session->parseFlashMessages(),
            'id' => $id,
            'file' => $file ?? false,
            'edition' => $data,
            'optionsNumber' => $optionsNumber,
            'optionsReleaseDate' => $optionsReleaseDate,
            'optionsFile' => $optionsFile,
        ];
        
        $this->twig->display('content/editionForm', $parseData);

        $footerdata = ['form_validator' => true, 'date_picker' => true, 'fileupload' => 'userfile'];
        $this->twig->display('generic/footer', $footerdata);
    }

    /**
     * Store editions
     */
    public function editionSave() {
        $data = $this->input->post(NULL, TRUE);

        // Set validation rules
        if (isset($data['id'])) {
            $this->form_validation->set_rules('id', 'id', 'trim|required');
        }
        $this->form_validation->set_rules('releasedate', 'releasedate', 'trim|required');
        $this->form_validation->set_rules('number', 'number', 'trim|numeric|required');

        // Validate data
        if ($this->form_validation->run() == FALSE) {
            // Return to previous edition
            if (isset($data['id'])) {
                $this->session->set_flashdata('message_error', $this->lang->line('edit_not_successfull') . validation_errors());
                redirect('cms/Editions/editionEdit');
            } else {
                $this->session->set_flashdata('message_error', $this->lang->line('add_not_successfull') . validation_errors());
                redirect('cms/Editions/editionAdd');
            }
        } 
        $store = [
            'id' => $data['id'] ?? null,
            'number' => $data['number'],
            'releasedate' => date('Y-m-d', strtotime($data['releasedate'])),
        ];

        // File uploaded?
        if (!empty($_FILES['userfile']['name'])) {
            $upload = $this->uploadFile($store['releasedate'], $store['number']);
            if (isset($upload['error'])) {
                $this->session->set_flashdata('message_error', $this->lang->line('store_not_success') . $upload['error']);
            } else {
                $store['file'] = $upload['upload_data']['file_name'];
            }
        }
        
        $id = $this->Edition_model->editionStore($store);

        if ($id) {
            $this->Log_model->logAdd(__CLASS__ . '/' . __FUNCTION__, $store);
            $this->session->set_flashdata('message', $this->lang->line('store_success'));
        } else {
            $this->session->set_flashdata('message_error', $this->lang->line('store_not_success'));            
        }        

        if (isset($data['submit_done'])) {
            redirect('cms/' . __CLASS__);
        } else {
            redirect('cms/Editions/editionEdit/' . $id);
        }
       
    }
    
    
    public function uploadFile($releasedate, $number)
    {
        $filename = 'rdt-jg' . $this->Edition_model->calcYear($releasedate);
        $filename.= '-nr' . str_pad($number, 2, '0', STR_PAD_LEFT) . '-' . str_replace('-', '', $releasedate) . '.pdf';
        
        $config['upload_path'] = './uploads/editions/';
        $config['file_name'] = $filename;
        $config['file_ext_tolower'] = true;
        $config['overwrite'] = true;
        $config['allowed_types'] = 'pdf';
        $config['max_size'] = 0;
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('userfile')) {
            return array('error' => $this->upload->display_errors());
        } else {
            $txtname = date('Y-m-d', strtotime($releasedate)) . '.txt';
            $txt = $this->pdfparser->parseFile($config['upload_path'] . $filename);
            file_put_contents($config['upload_path'] . '/txt/' . $txtname, $txt);
            return array('upload_data' => $this->upload->data());
        }
    }
    
   
    /**
     * Soft delete edition
     * @return JSON response
     */
    public function editionSoftDelete()
    {
        $id = $this->input->post('id');
        if (empty($id)) {
            return $this->data_tables->jsonSoftDelResponse('missingParameter');
        }
        
        // Test if edition has news or torentjes linked to it
        $parameters = new stdClass();
        $parameters->varname = 'edition_id';
        $parameters->value = $id;
        $news = $this->News_model->newsGetOverview($parameters);
        $torentjes = $this->Torentjes_model->torentjeGetOverview($parameters);
        if ($news || $torentjes) {
            return $this->data_tables->jsonSoftDelResponse('inUse');
        }
        
        $result = $this->Generic_model->setSoftDeleteStatus(TABLE_EDITIONS, $id, 1);
        
        if ($result) {
            $this->Log_model->logAdd(__CLASS__ . '/' . __FUNCTION__, $id);
            return $this->data_tables->jsonSoftDelResponse('success');
        }
        return $this->data_tables->jsonSoftDelResponse('error');
    }
    
    /**
     * Soft restore edition
     * @return JSON response
     */
    public function editionSoftRestore()
    {
        $id = $this->input->post('id');
        if (empty($id)) {
            return $this->data_tables->jsonSoftDelResponse('missingParameter');
        }
        
        $result = $this->Generic_model->setSoftDeleteStatus(TABLE_EDITIONS, $id, 0);
        
        if ($result) {
            $this->Log_model->logAdd(__CLASS__ . '/' . __FUNCTION__, $id);
            return $this->data_tables->jsonSoftDelResponse('success');
        }
        return $this->data_tables->jsonSoftDelResponse('error');
    }
       
    /**
     * Hard delete edition
     * @return JSON response
     */
    public function editionHardDelete()
    {
        $id = $this->input->post('id');
        if (empty($id)) {
            return $this->data_tables->jsonSoftDelResponse('missingParameter');
        }
        
        // Check if we have a file
        $edition = $this->Edition_model->editionGet($id);
        if (!empty($edition['file'])) {
            unlink('./uploads/editions/' . $edition['file']);
        } 
        
        $result = $this->Generic_model->hardDelete(TABLE_EDITIONS, $id);
        
        if ($result) {
            $this->Log_model->logAdd(__CLASS__ . '/' . __FUNCTION__, $id);
            return $this->data_tables->jsonSoftDelResponse('success');
        }
        return $this->data_tables->jsonSoftDelResponse('error');
    }

    /**
     * Once in a lifetime... Import from Fred
     */
    public function import()
    {
        show_error('deze functie is uitgeschakeld');
//        $sourceDir = './uploads/editions/archive/';
//        $targetDir = './uploads/editions/';
//
//        $files = glob($sourceDir . '*.pdf');
//        foreach ($files as $filename) {
//            if (!is_array($filename)) {
//                // Buy us some time
//                set_time_limit(30);
//                $file = pathinfo($filename,  PATHINFO_BASENAME);
//                // Copy file
//                copy($sourceDir . $file, $targetDir . $file);
//
//                // Extract data for DB
//                preg_match('/rdt-jg([0-9]+)-nr([0-9]+)-([0-9]+)/i', $file, $matches);
//                $edition = [
//                    'id' => null,
//                    'number' => $matches[2],
//                    'releasedate' => date('Y-m-d', strtotime($matches[3])),
//                    'file' => $file,
//                ];
//                $this->Edition_model->editionStore($edition);
//
//                // Add txt version
//                $txtname = $edition['releasedate'] . '.txt';
//                $txt = $this->pdfparser->parseFile($filename);
//                file_put_contents($targetDir . 'txt/' . $txtname, $txt);
//                echo $file . ' handled';
//            }
//        }
        
    }
    
// Class and file ends here.
}
