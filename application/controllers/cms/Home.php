<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home extends CI_Controller
{

    /**
     * CMS home
     * @author GB
     * @copyright 2018
     */
    public $page = 'home';
    public $menuId = 1;
    protected $userId;
    protected $roleId;

    public function __construct()
    {
        parent::__construct();

        $this->userId = (int) $this->session->userdata('id');
        $this->roleId = (int) $this->session->userdata('roleId');

        $this->header_builder->controllerSet($this->menuId);
        if ($this->roleId < 10) {
            redirect('cms/login/logout');
        }
        $this->load->helper('text');
        $this->load->model('Role_model');
    }

    /**
     * Main home
     */
    public function index()
    {
        $this->twig->display('generic/header', $this->header_builder->headerVarsGet());

        if ($this->roleId >= 30) {
            ob_start();
            phpinfo();
            $phpInfoFull = ob_get_clean();
            $phpInfo = preg_replace('%^.*<body>(.*)</body>.*$%ms', '$1', $phpInfoFull);
        }
        
        // Parse content and footer
        $parseData = array(
            'message' => $this->session->parseFlashMessages(),
            'roles' => $this->Role_model->roleGet(),
            'phpInfo' => $phpInfo ?? '',
        );

        $this->twig->display('dashboardHome', $parseData);
        $this->twig->display('generic/footer');
    }
    

// Class and file ends here.
}
