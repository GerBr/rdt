<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Galleries extends CI_Controller
{

    /**
     * Manage main content
     * 
     * @author GB
     * @copyright 2018
     */
    public $menuId = 4;
    public $section = 'Fotogalerij';
    protected $roleId;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Content_model');
        $this->roleId = (int) $this->session->userdata('roleId');
        if ($this->roleId < 20) {
            redirect('cms/login/logout');
        }
        $this->header_builder->controllerSet($this->menuId, $this->section);
    }


    public function index($var1 = NULL)
    {
        $this->twig->display('generic/header', $this->header_builder->headerVarsGet());

        // Show message
        if ($var1 == 'showSoftDeletes') {
            $extraMessages[] = ['content' => $this->lang->line('deleted_records_shown'), 'type' => 'warning', 'dismissable' => false];
        }

        $columns = array(
            array('name' => 'id', 'orderable' => 'true'),
            array('name' => 'title', 'orderable' => 'true', 'filter_type' => 'text'),
            array('name' => 'url', 'orderable' => 'true', 'filter_type' => 'text'),
        );

        $parsedata = array('message' => $this->session->parseFlashMessages(isset($extraMessages) ? $extraMessages : NULL), 'columns' => $columns);

        // Parse content
        $this->twig->display('generic/dataTable', $parsedata);

        // Parse footer
        $footerdata = array('data_table' => TRUE, 'columns' => $columns);

        $footerdata['ajax_url'] = site_url('cms/Galleries/galleryAjax');
        $footerdata['url_add'] = site_url('cms/Galleries/galleryAdd');
        $footerdata['url_edit'] = site_url('cms/Galleries/galleryEdit');
        $footerdata['url_hard_delete'] = site_url('cms/Galleries/galleryHardDelete');            
        
        $this->twig->display('generic/footer', $footerdata);
    }
    
    /**
     * Echo JSON encoded overview
     */
    public function galleryAjax()
    {
        // Count records
        $this->data_tables->setParams(['countAllRows' => true, 'calcEdition' => true]);
        $count = (int) $this->Content_model->galleryGetOverview($this->data_tables->params)[0]['counted'];
        unset($this->data_tables->params->countAllRows);

        // Get rows and return JSON
        $rows = $this->Content_model->galleryGetOverview($this->data_tables->params);
        echo $this->data_tables->getJson($rows, $count);
    }
    
    /**
     * Add form
     */
    public function galleryAdd()
    {
        $this->twig->display('generic/header', $this->header_builder->headerVarsGet());
        
        $optionsTitle = [
            'name' => 'title',
            'id' => 'title',
            'placeholder' => $this->lang->line('title'),
            'data-validation' => 'required',
            'class' => 'form-control'
        ];
        
        $optionsUrl = [
            'name' => 'url',
            'id' => 'url',
            'placeholder' => $this->lang->line('url'),
            'data-validation' => 'required',
            'class' => 'form-control'
        ];
        
        $parseData = [
            'message' => $this->session->parseFlashMessages(),
            'optionsTitle' => $optionsTitle,
            'optionsUrl' => $optionsUrl,
        ];
        $this->twig->display('content/galleryForm', $parseData);

        $footerdata = ['form_validator' => true];
        $this->twig->display('generic/footer', $footerdata);
    }
    
    /**
     * Edit form
     * @param int $id
     */
    public function galleryEdit($id = 0)
    {
        $data = $this->Content_model->galleryGet(intval($id));
        if (empty($data)) {
            $this->session->set_flashdata('message_error', $this->lang->line('no_data_found'));
            redirect('cms/' . __CLASS__);
        }
        $this->twig->display('generic/header', $this->header_builder->headerVarsGet());

        $optionsTitle = [
            'name' => 'title',
            'id' => 'title',
            'value' => $data['title'],
            'placeholder' => $this->lang->line('title'),
            'data-validation' => 'required',
            'class' => 'form-control'
        ];
        
        $optionsUrl = [
            'name' => 'url',
            'id' => 'url',
            'value' =>  $data['url'],
            'placeholder' => $this->lang->line('url'),
            'data-validation' => 'required',
            'class' => 'form-control'
        ];
        
        $parseData = [
            'message' => $this->session->parseFlashMessages(),
            'id' => $id,
            'gallery' => $data,
            'optionsTitle' => $optionsTitle,
            'optionsUrl' => $optionsUrl,
        ];
        $this->twig->display('content/galleryForm', $parseData);
        
        $footerdata = ['form_validator' => true];
        $this->twig->display('generic/footer', $footerdata);
    }

    /**
     * Store item
     */
    public function gallerySave() {
        $data = $this->input->post(NULL, TRUE);
        
        // Set validation rules
        if (isset($data['id'])) {
            $this->form_validation->set_rules('id', 'id', 'trim|required');
        }
        $this->form_validation->set_rules('title', 'title', 'trim|required');
        $this->form_validation->set_rules('url', 'url', 'trim|required');

        // Validate data
        if ($this->form_validation->run() == FALSE) {
            // Return to previous edition
            if (isset($data['id'])) {
                $this->session->set_flashdata('message_error', $this->lang->line('edit_not_successfull') . validation_errors());
                redirect('cms/Galleries/galleryEdit');
            } else {
                $this->session->set_flashdata('message_error', $this->lang->line('add_not_successfull') . validation_errors());
                redirect('cms/Galleries/galleryAdd');
            }
        } 
        $store = [
            'id' => isset($data['id']) ? $data['id'] : null,
            'title' => $data['title'],
            'url' => $data['url'],
        ];
        
        $id = $this->Content_model->galleryStore($store);
        if ($id) {
            $this->Log_model->logAdd(__CLASS__ . '/' . __FUNCTION__, $store);
            $this->session->set_flashdata('message', $this->lang->line('store_success'));
        } else {
            $this->session->set_flashdata('message_error', $this->lang->line('store_not_success'));            
        }

        if (isset($data['submit_done'])) {
            redirect('cms/Galleries');
        } else {
            redirect('cms/Galleries/galleryEdit/' . $id);
        }
       
    }
    
    /**
     * Hard delete gallery
     * @return JSON response
     */
    public function galleryHardDelete()
    {
        $id = $this->input->post('id');
        if (empty($id)) {
            return $this->data_tables->jsonSoftDelResponse('missingParameter');
        }
        
        $result = $this->Generic_model->hardDelete(TABLE_GALLERIES, $id);
        
        if ($result) {
            $this->Log_model->logAdd(__CLASS__ . '/' . __FUNCTION__, $id);
            return $this->data_tables->jsonSoftDelResponse('success');
        }
        return $this->data_tables->jsonSoftDelResponse('error');
    }
// Class and file ends here.
}
