<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Page extends CI_Controller
{

    /**
     * Manage pages
     * 
     * @author GB
     * @copyright 2018
     */
    public $menuId = 3;
    public $section = 'Pagina\'s';
    protected $userId;
    protected $roleId;

    public function __construct()
    {
        parent::__construct();
        $this->userId = (int) $this->session->userdata('id');
        $this->roleId = (int) $this->session->userdata('roleId');
        if ($this->roleId < 20) {
            redirect('cms/login/logout');
        }
        $this->header_builder->controllerSet($this->menuId, $this->section);
    }

    /**
     * Display Async table of pages
     */
    public function index($var1 = NULL)
    {
        $this->twig->display('generic/header', $this->header_builder->headerVarsGet());

        // Show message
        if ($var1 == 'showSoftDeletes') {
            $extraMessages[] = ['content' => $this->lang->line('deleted_records_shown'), 'type' => 'warning', 'dismissable' => false];
        }

        $columns = array(
            array('name' => 'id', 'orderable' => 'true'),
            array('name' => 'name', 'orderable' => 'true', 'filter_type' => 'text'),
            array('name' => 'updated_at', 'orderable' => 'true'),
            );

        $parsedata = array('message' => $this->session->parseFlashMessages(isset($extraMessages) ? $extraMessages : NULL), 'columns' => $columns);

        // Parse content
        $this->twig->display('generic/dataTable', $parsedata);

        // Parse footer
        $footerdata = array('data_table' => TRUE, 'columns' => $columns);

        $footerdata['ajax_url'] = site_url('cms/Page/pageAjax');

        if ($var1 == 'showSoftDeletes') {
            $footerdata['var1'] = 'showSoftDeletes';
            $footerdata['url_back'] = site_url('cms/page');
            $footerdata['url_undelete'] = site_url('cms/Page/pageSoftRestore');
            $footerdata['url_hard_delete'] = site_url('cms/Page/pageHardDelete');
        } else {
            $footerdata['url_add'] = site_url('cms/page/pageAdd');
            $footerdata['url_edit'] = site_url('cms/page/pageEdit');
            $footerdata['url_delete'] = site_url('cms/Page/pageSoftDelete');
            $footerdata['url_deleted'] = site_url('cms/Page/index/showSoftDeletes');
        }
        
        $this->twig->display('generic/footer', $footerdata);
    }
    
    /**
     * Echo JSON encoded overview
     */
    public function pageAjax()
    {
        echo $this->data_tables->getDefaultOutput('Page_model', 'pageGetOverview');
    }
    
    /**
     * Add form
     */
    public function pageAdd()
    {
        $this->twig->display('generic/header', $this->header_builder->headerVarsGet());
        
        $allPages[0] = '-';
        $parameters = new stdClass();
        $parameters->sort = 'name';
        $parameters->order = 'ASC';
        $parameters->dropdown = ['key' => 'id', 'name' => 'name'];
        $allPages = $allPages + $this->Page_model->pageGetOverview($parameters);
        
        $optionsPageName = [
            'name' => 'name',
            'id' => 'name',
            'placeholder' => $this->lang->line('name'),
            'data-validation' => 'length',
            'data-validation-length' => 'min3',
            'class' => 'form-control'
        ];

        $optionsPageContent = [
            'name' => 'content',
            'id' => 'content',
            'placeholder' => $this->lang->line('content'),
            'data-validation' => 'length',
            'data-validation-length' => 'min10',
            'class' => 'form-control ckeditor'
        ];
        
        $optionsUpdatedAt = [
            'name' => 'updated_at',
            'id' => 'updated_at',
            'class' => 'form-control',
            'readonly' => 'readonly'
        ];
        
        $parseData = [
            'message' => $this->session->parseFlashMessages(),
            'allPages' => $allPages,
            'optionsPageName' => $optionsPageName,
            'optionsPageContent' => $optionsPageContent,
            'optionsUpdatedAt' => $optionsUpdatedAt,
        ];
        $this->twig->display('content/pageForm', $parseData);

        $footerdata = ['form_validator' => true, 'select2' => true, 'ckeditor' => ['content']];
        $this->twig->display('generic/footer', $footerdata);
    }
    /**
     * Edit form
     * @param int $id
     */
    public function pageEdit($id = 0)
    {
        $data = $this->Page_model->pageGet(intval($id));
        if (empty($data)) {
            $this->session->set_flashdata('message_error', $this->lang->line('no_data_found'));
            redirect('cms/' . __CLASS__);
        }
        $this->twig->display('generic/header', $this->header_builder->headerVarsGet());
        
        $allPages[0] = '-';
        $parameters = new stdClass();
        $parameters->sort = 'name';
        $parameters->order = 'ASC';
        $parameters->dropdown = ['key' => 'id', 'name' => 'name'];
        $allPages = $allPages + $this->Page_model->pageGetOverview($parameters);
        unset($allPages[$id]);

        $optionsPageName = [
            'name' => 'name',
            'id' => 'name',
            'value' => $data['name'],
            'placeholder' => $this->lang->line('name'),
            'data-validation' => 'length',
            'data-validation-length' => 'min3',
            'class' => 'form-control'
        ];

        $optionsPageContent = [
            'name' => 'content',
            'id' => 'content',
            'value' => $data['content'],
            'placeholder' => $this->lang->line('content'),
            'data-validation' => 'length',
            'data-validation-length' => 'min10',
            'class' => 'form-control ckeditor'
        ];
        
        $optionsUpdatedAt = [
            'name' => 'updated_at',
            'id' => 'updated_at',
            'value' => $data['updated_at'],
            'class' => 'form-control',
            'readonly' => 'readonly'
        ];
        
        $parseData = [
            'message' => $this->session->parseFlashMessages(),
            'id' => $id,
            'page' => $data,
            'allPages' => $allPages,
            'optionsPageName' => $optionsPageName,
            'optionsPageContent' => $optionsPageContent,
            'optionsUpdatedAt' => $optionsUpdatedAt,
        ];
        $this->twig->display('content/pageForm', $parseData);

        $footerdata = ['form_validator' => true, 'select2' => true, 'ckeditor' => ['content']];
        $this->twig->display('generic/footer', $footerdata);
    }

    /**
     * Store pages
     */
    public function pageSave() {
        $data = $this->input->post(NULL, TRUE);
        
        // Set validation rules
        if (isset($data['id'])) {
            $this->form_validation->set_rules('id', 'id', 'trim|required');
        }
        $this->form_validation->set_rules('name', 'name', 'trim|required');
        $this->form_validation->set_rules('parent', 'parent', 'trim|numeric');

        // Validate data
        if ($this->form_validation->run() == FALSE) {
            // Return to previous page
            if (isset($data['id'])) {
                $this->session->set_flashdata('message_error', $this->lang->line('edit_not_successfull') . validation_errors());
                redirect('cms/Page/pageEdit');
            } else {
                $this->session->set_flashdata('message_error', $this->lang->line('add_not_successfull') . validation_errors());
                redirect('cms/Page/pageAdd');
            }
        } 
        $store = [
            'id' => $data['id'] ?: null,
            'name' => $data['name'],
            'parent' => intval($data['parent']),
            'content' => $data['content'],
        ];
        $id = $this->Page_model->pageStore($store);
        if ($id) {
            $this->Log_model->logAdd(__CLASS__ . '/' . __FUNCTION__, $store);
            $this->session->set_flashdata('message', $this->lang->line('store_success'));
        } else {
            $this->session->set_flashdata('message_error', $this->lang->line('store_not_success'));            
        }

        if (isset($data['submit_done'])) {
            redirect('cms/' . __CLASS__);
        } else {
            redirect('cms/page/pageEdit/' . $id);
        }
    }
    
    /**
     * Soft delete page
     * @return JSON response
     */
    public function pageSoftDelete()
    {
        $id = $this->input->post('id');
        if (empty($id)) {
            return $this->data_tables->jsonSoftDelResponse('missingParameter');
        }
        // Test if page is parent
        $children = $this->Page_model->pageMenuItemsGet($id);
        if ($children) {
            return $this->data_tables->jsonSoftDelResponse('inUse');
        }
        
        $result = $this->Generic_model->setSoftDeleteStatus(TABLE_PAGES, $id, 1);
        
        if ($result) {
            $this->Log_model->logAdd(__CLASS__ . '/' . __FUNCTION__, $id);
            return $this->data_tables->jsonSoftDelResponse('success');
        }
        return $this->data_tables->jsonSoftDelResponse('error');
    }
    
    /**
     * Soft restore page
     * @return JSON response
     */
    public function pageSoftRestore()
    {
        $id = $this->input->post('id');
        if (empty($id)) {
            return $this->data_tables->jsonSoftDelResponse('missingParameter');
        }
        
        $result = $this->Generic_model->setSoftDeleteStatus(TABLE_PAGES, $id, 0);
        
        if ($result) {
            $this->Log_model->logAdd(__CLASS__ . '/' . __FUNCTION__, $id);
            return $this->data_tables->jsonSoftDelResponse('success');
        }
        return $this->data_tables->jsonSoftDelResponse('error');
    }
       
    /**
     * Hard delete page
     * @return JSON response
     */
    public function pageHardDelete()
    {
        $id = $this->input->post('id');
        if (empty($id)) {
            return $this->data_tables->jsonSoftDelResponse('missingParameter');
        }
        
        $result = $this->Generic_model->hardDelete(TABLE_PAGES, $id);
        
        if ($result) {
            $this->Log_model->logAdd(__CLASS__ . '/' . __FUNCTION__, $id);
            return $this->data_tables->jsonSoftDelResponse('success');
        }
        return $this->data_tables->jsonSoftDelResponse('error');
    }
// Class and file ends here.
}
