<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class News extends CI_Controller
{

    /**
     * Manage main content
     * 
     * @author GB
     * @copyright 2018
     */
    public $menuId = 4;
    public $section = 'Nieuws';
    protected $userId;
    protected $roleId;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Edition_model');
        $this->load->model('News_model');
        $this->userId = (int) $this->session->userdata('id');
        $this->roleId = (int) $this->session->userdata('roleId');
        if ($this->roleId < 20) {
            redirect('cms/login/logout');
        }
        $this->header_builder->controllerSet($this->menuId, $this->section);
    }


    /**
     * Async overview for news items
     */
    public function index($var1 = NULL)
    {
        $this->twig->display('generic/header', $this->header_builder->headerVarsGet());

        // Show message
        if ($var1 == 'showSoftDeletes') {
            $extraMessages[] = ['content' => $this->lang->line('deleted_records_shown'), 'type' => 'warning', 'dismissable' => false];
        }

        $columns = array(
            array('name' => 'id', 'orderable' => 'true'),
            array('name' => 'title', 'orderable' => 'true', 'filter_type' => 'text'),
            array('name' => 'edition', 'orderable' => 'false', 'filter_type' => 'text'),
            array('name' => 'updated_at', 'orderable' => 'true', 'filter_type' => 'text'),
        );

        $parsedata = array('message' => $this->session->parseFlashMessages(isset($extraMessages) ? $extraMessages : NULL), 'columns' => $columns);

        // Parse content
        $this->twig->display('generic/dataTable', $parsedata);

        // Parse footer
        $footerdata = array('data_table' => TRUE, 'columns' => $columns);

        $footerdata['ajax_url'] = site_url('cms/News/newsAjax');
        if ($var1 == 'showSoftDeletes') {
            $footerdata['var1'] = 'showSoftDeletes';
            $footerdata['url_back'] = site_url('cms/News');
            $footerdata['url_undelete'] = site_url('cms/News/newsSoftRestore');
            $footerdata['url_hard_delete'] = site_url('cms/News/newsHardDelete');

        } else {
            $footerdata['url_add'] = site_url('cms/News/newsAdd');
            $footerdata['url_edit'] = site_url('cms/News/newsEdit');
            $footerdata['url_delete'] = site_url('cms/News/newsSoftDelete');
            $footerdata['url_deleted'] = site_url('cms/News/index/showSoftDeletes');
        }

        $this->twig->display('generic/footer', $footerdata);
    }
    
    /**
     * Echo JSON encoded overview
     */
    public function newsAjax()
    {
        // Count records
        $this->data_tables->setParams(['countAllRows' => true, 'calcEdition' => true]);
        $count = (int) $this->News_model->newsGetOverview($this->data_tables->params)[0]['counted'];
        unset($this->data_tables->params->countAllRows);

        // Get rows and return JSON
        $rows = $this->News_model->newsGetOverview($this->data_tables->params);
        echo $this->data_tables->getJson($rows, $count);
    }
    
    /**
     * Add form
     */
    public function newsAdd()
    {
        $this->twig->display('generic/header', $this->header_builder->headerVarsGet());
        
        $optionsTitle = [
            'name' => 'title',
            'id' => 'title',
            'placeholder' => $this->lang->line('title'),
            'data-validation' => 'required',
            'class' => 'form-control'
        ];
        
        $optionsIntro = [
            'name' => 'intro',
            'id' => 'intro',
            'placeholder' => $this->lang->line('content'),
            'data-validation' => 'length',
            'data-validation-length' => 'min10',
            'class' => 'form-control ckeditor'
        ];

        $optionsImg = [
            'name' => 'img',
            'id' => 'img',
            'placeholder' => $this->lang->line('image'),
            'class' => 'form-control'
        ];
        
        $parameters = new stdClass();
        $parameters->varname = 'releasedate >';
        $parameters->value= date('Y-m-d', strtotime('-3 month'));
        $parameters->dropdown = true;
        $allEditions = $this->Edition_model->editionGetOverview($parameters);
        
        $parseData = [
            'message' => $this->session->parseFlashMessages(),
            'optionsTitle' => $optionsTitle,
            'optionsIntro' => $optionsIntro,
            'optionsImg' => $optionsImg,
            'allEditions' => $allEditions,
        ];
        $this->twig->display('content/newsForm', $parseData);

        $footerdata = ['form_validator' => true, 'select2' => true, 'ckeditor' => ['intro']];
        $this->twig->display('generic/footer', $footerdata);
    }
    
    /**
     * Edit form
     * @param int $id
     */
    public function newsEdit($id = 0)
    {
        $data = $this->News_model->newsGet(intval($id));
        if (empty($data)) {
            $this->session->set_flashdata('message_error', $this->lang->line('no_data_found'));
            redirect('cms/' . __CLASS__);
        }
     
        $this->twig->display('generic/header', $this->header_builder->headerVarsGet());
        
        $optionsTitle = [
            'name' => 'title',
            'id' => 'title',
            'value' => $data['title'],
            'placeholder' => $this->lang->line('title'),
            'data-validation' => 'required',
            'class' => 'form-control'
        ];
        
        $optionsIntro = [
            'name' => 'intro',
            'id' => 'intro',
            'value' => $data['intro'],
            'placeholder' => $this->lang->line('content'),
            'data-validation' => 'length',
            'data-validation-length' => 'min10',
            'class' => 'form-control ckeditor'
        ];

        $optionsImg = [
            'name' => 'img',
            'id' => 'img',
            'value' => $data['img'],
            'placeholder' => $this->lang->line('image'),
            'class' => 'form-control'
        ];
        
        $parameters = new stdClass();
        $parameters->varname = 'releasedate >';
        $parameters->value= date('Y-m-d', strtotime('-3 month'));
        $parameters->dropdown = true;
        $allEditions = $this->Edition_model->editionGetOverview($parameters);
        
        $parseData = [
            'message' => $this->session->parseFlashMessages(),
            'id' => $id,
            'news' => $data,
            'optionsTitle' => $optionsTitle,
            'optionsIntro' => $optionsIntro,
            'optionsImg' => $optionsImg,
            'allEditions' => $allEditions,
        ];
        $this->twig->display('content/newsForm', $parseData);

        $footerdata = ['form_validator' => true, 'select2' => true, 'ckeditor' => ['intro']];
        $this->twig->display('generic/footer', $footerdata);
    }

    /**
     * Store torentje
     */
    public function newsSave() {
        $data = $this->input->post(NULL, TRUE);
        
        // Set validation rules
        if (isset($data['id'])) {
            $this->form_validation->set_rules('id', 'id', 'trim|required');
        }
        $this->form_validation->set_rules('title', 'title', 'trim|required');
        $this->form_validation->set_rules('intro', 'intro', 'trim|required');
        $this->form_validation->set_rules('edition', 'edition', 'trim|numeric|required');

        // Validate data
        if ($this->form_validation->run() == FALSE) {
            // Return to previous edition
            if (isset($data['id'])) {
                $this->session->set_flashdata('message_error', $this->lang->line('edit_not_successfull') . validation_errors());
                redirect('cms/News/newsEdit');
            } else {
                $this->session->set_flashdata('message_error', $this->lang->line('add_not_successfull') . validation_errors());
                redirect('cms/News/newsAdd');
            }
        } 
        $store = [
            'id' => $data['id'] ?: null,
            'edition_id' => $data['edition'],
            'title' => $data['title'],
            'intro' => $data['intro'],
            'img' => $data['img'],
        ];
        $id = $this->News_model->newsStore($store);
        if ($id) {
            $this->Log_model->logAdd(__CLASS__ . '/' . __FUNCTION__, $store);
            $this->session->set_flashdata('message', $this->lang->line('store_success'));
        } else {
            $this->session->set_flashdata('message_error', $this->lang->line('store_not_success'));            
        }

        if (isset($data['submit_done'])) {
            redirect('cms/' . __CLASS__);
        } else {
            redirect('cms/News/newsEdit/' . $id);
        }  
    }
    
    /**
     * Soft delete news item
     * @return JSON response
     */
    public function newsSoftDelete()
    {
        $id = $this->input->post('id');
        if (empty($id)) {
            return $this->data_tables->jsonSoftDelResponse('missingParameter');
        }
        
        $result = $this->Generic_model->setSoftDeleteStatus(TABLE_NEWS, $id, 1);
        
        if ($result) {
            $this->Log_model->logAdd(__CLASS__ . '/' . __FUNCTION__, $id);
            return $this->data_tables->jsonSoftDelResponse('success');
        }
        return $this->data_tables->jsonSoftDelResponse('error');
    }
    
    /**
     * Soft restore news item
     * @return JSON response
     */
    public function newsSoftRestore()
    {
        $id = $this->input->post('id');
        if (empty($id)) {
            return $this->data_tables->jsonSoftDelResponse('missingParameter');
        }
        
        $result = $this->Generic_model->setSoftDeleteStatus(TABLE_NEWS, $id, 0);
        
        if ($result) {
            $this->Log_model->logAdd(__CLASS__ . '/' . __FUNCTION__, $id);
            return $this->data_tables->jsonSoftDelResponse('success');
        }
        return $this->data_tables->jsonSoftDelResponse('error');
    }
       
    /**
     * Hard delete news item
     * @return JSON response
     */
    public function newsHardDelete()
    {
        $id = $this->input->post('id');
        if (empty($id)) {
            return $this->data_tables->jsonSoftDelResponse('missingParameter');
        }
        
        $result = $this->Generic_model->hardDelete(TABLE_NEWS, $id);
        
        if ($result) {
            $this->Log_model->logAdd(__CLASS__ . '/' . __FUNCTION__, $id);
            return $this->data_tables->jsonSoftDelResponse('success');
        }
        return $this->data_tables->jsonSoftDelResponse('error');
    }
// Class and file ends here.
}
