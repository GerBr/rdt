<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Textstring extends CI_Controller
{

    /**
     * Manage static textstrings
     * 
     * @author GB
     * @copyright 2018
     */
    public $menuId = 3;
    public $section = 'Teksten';
    protected $roleId;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Content_model');
        $this->roleId = (int) $this->session->userdata('roleId');
        if ($this->roleId < 20) {
            redirect('cms/login/logout');
        }
        $this->header_builder->controllerSet($this->menuId, $this->section);
    }


    public function index($var1 = NULL)
    {
        $this->twig->display('generic/header', $this->header_builder->headerVarsGet());

        // Show message
        if ($var1 == 'showSoftDeletes') {
            $extraMessages[] = ['content' => $this->lang->line('deleted_records_shown'), 'type' => 'warning', 'dismissable' => false];
        }

        $columns = array(
            array('name' => 'id', 'orderable' => 'true'),
            array('name' => 'key', 'orderable' => 'true', 'filter_type' => 'text'),
            array('name' => 'explain', 'orderable' => 'true', 'filter_type' => 'text'),
        );

        $parsedata = array('message' => $this->session->parseFlashMessages(isset($extraMessages) ? $extraMessages : NULL), 'columns' => $columns);

        // Parse content
        $this->twig->display('generic/dataTable', $parsedata);

        // Parse footer
        $footerdata = array('data_table' => TRUE, 'columns' => $columns);

        $footerdata['ajax_url'] = site_url('cms/Textstring/textstringAjax');
        $footerdata['url_edit'] = site_url('cms/Textstring/textstringEdit');        
        $this->twig->display('generic/footer', $footerdata);
    }
    
    /**
     * Echo JSON encoded overview
     */
    public function textstringAjax()
    {
        echo $this->data_tables->getDefaultOutput('Content_model', 'textstringGetOverview');
    }
    
    /**
     * Edit form
     * @param int $id
     */
    public function textstringEdit($id = 0)
    {
        $data = $this->Content_model->textstringGet(intval($id));
        if (empty($data)) {
            $this->session->set_flashdata('message_error', $this->lang->line('no_data_found'));
            redirect('cms/' . __CLASS__);
        }
        $this->twig->display('generic/header', $this->header_builder->headerVarsGet());

        $optionsText = [
            'name' => 'text',
            'id' => 'text',
            'value' => $data['text'],
            'placeholder' => $this->lang->line('text'),
            'data-validation' => 'required',
            'class' => 'form-control ckeditor'
        ];
        
        $parseData = [
            'message' => $this->session->parseFlashMessages(),
            'id' => $id,
            'textstring' => $data,
            'optionsText' => $optionsText,
        ];
        $this->twig->display('content/textstringForm', $parseData);
        
        $footerdata = ['form_validator' => true, 'ckeditor' => ['text']];
        $this->twig->display('generic/footer', $footerdata);
    }

    /**
     * Store textstring
     */
    public function textstringSave() {
        $data = $this->input->post(NULL, TRUE);
        
        // Set validation rules
        $this->form_validation->set_rules('id', 'id', 'trim|required');
        $this->form_validation->set_rules('text', 'text', 'trim|required');

        // Validate data
        if ($this->form_validation->run() == FALSE) {
            // Return to previous edition
            $this->session->set_flashdata('message_error', $this->lang->line('edit_not_successfull') . validation_errors());
            redirect('cms/Content/textstringEdit');
        } 
        $result = $this->Content_model->textstringStore($data['id'], $data['text']);
        if ($result) {
            $this->Log_model->logAdd(__CLASS__ . '/' . __FUNCTION__, $data);
            $this->session->set_flashdata('message', $this->lang->line('store_success'));
        } else {
            $this->session->set_flashdata('message_error', $this->lang->line('store_not_success'));            
        }

        if (isset($data['submit_done'])) {
            redirect('cms/Textstring/');
        } else {
            redirect('cms/Textstring/textstringEdit/' . $data['id']);
        }
       
    }
// Class and file ends here.
}
